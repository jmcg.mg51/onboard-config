import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

import { MiddleMongoService } from './../services/http/middle-mongo.service';
import { CognitoService } from 'app/services/aws/cognito.service';
import { Clientes } from 'app/model/Clientes';
import { isEmpty } from 'app/model/UtilValidator';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class HomeComponent implements OnInit {
  displayedColumns: string[] = ['nombre', 'correo', 'ApiToken'];
  dataSource: any;
  expandedElement: Clientes | null;
  clientes: any;
  errorGenerico: string;
  nombreOferta: string;
  userRole;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private middleMongo: MiddleMongoService, private cognito: CognitoService, public router: Router,
              private spinner: NgxSpinnerService) {
}

async ngOnInit() {
  await this.spinner.show();
  sessionStorage.removeItem('tipoDeBusqueda');
  sessionStorage.removeItem('resultSearch');
  sessionStorage.removeItem('exportButton');
  sessionStorage.removeItem('tipoBusqueda');
  sessionStorage.removeItem('correoSearch');
  sessionStorage.removeItem('trackIDSearch');
  sessionStorage.removeItem('date1Search');
  sessionStorage.removeItem('date2Search');
  localStorage.clear();
  this.clientes = [];
  this.paginator._intl.itemsPerPageLabel = 'Resultados por pagina';
  this.userRole = this.cognito.getRole();
  this.dataSource = new MatTableDataSource<Clientes>(this.clientes);
  await this.readWSClient();

}

async readWSClient() {
  // this.cognito.getCredentialsExample();
  // await this.cognito.creaLasCredenciales();
  this.clientes = await this.middleMongo.getCustomers();
  // console.log(this.clientes);
  if (this.clientes && !isEmpty(this.clientes) ) {
    this.dataSource = new MatTableDataSource<Clientes>(this.clientes);
    this.dataSource.paginator = this.paginator;
  }
  this.spinner.hide();
}

applyFilter(filterValue: string) {
  this.dataSource.filter = filterValue.trim().toLowerCase();
}

async editClient(id: string, user: string, pwd: string, nombre: string) {
  // await this.spinner.show();
  console.log("el nombre de la oferta", nombre);
  this.nombreOferta = nombre;
  localStorage.setItem("nombreComercio", this.nombreOferta);
  // sessionStorage.setItem('tokenEdit', await this.tokenService.generaToken(user, pwd));
  // this.spinner.hide();
  this.router.navigate(['/dashboard/edit' + `/${id}`]);

}

}
