import { SecuredTouchComponent } from './../../administrador/secured-touch/secured-touch.component';
import { BlackListComponent } from './../../administrador/black-list/black-list.component';
import { Routes } from '@angular/router';

import { LoginComponent } from './../../user/login/login.component';
import { HomeComponent } from '../../home/home.component';
import { UserComponent } from '../../user/profile/user.component';
import { CreateClienteComponent } from 'app/administrador/create-cliente/create-cliente.component';
import { ReportsComponent } from '../../administrador/reports/reports.component';
import { DetailComponent } from '../../administrador/reports/detail/detail.component';
import { NubariumComponent } from '../../administrador/nubarium/nubarium.component';
import { RulesComponent } from 'app/administrador/rules/rules.component';


import { H2hConfigComponent } from 'app/administrador/h2h-config/h2h-config.component';
import { FraudComponent } from '../../administrador/fraud/fraud.component';
import { StatisticsFraudComponent } from '../../administrador/statistics/statistics-fraud/statistics-fraud.component';
import { StatisticsGeneralComponent } from '../../administrador/statistics/statistics-general/statistics-general.component';
import { AnalyticsSpecificComponent } from '../../administrador/analytics/analytics-specific/analytics-specific.component';
import { AnalyticsKpisComponent } from '../../administrador/analytics/analytics-kpis/analytics-kpis.component';
import { StatusComponent } from '../../administrador/reports/status/status.component';
import { LoginGuard } from './../../services/guards/login.guard';
import { NotificationsComponent } from '../../administrador/notifications/notifications.component';
import { FicoScoreComponent } from '../../administrador/fico-score/fico-score.component';
import { PerfilesComponent } from '../../administrador/perfiles/perfiles.component';
import { CirculoCreditoComponent } from '../../administrador/circulo-credito/circulo-credito.component';


export const AdminLayoutRoutes: Routes = [
    { path: '',               component: HomeComponent },
    { path: 'user',           component: UserComponent },
    // { path: 'login',          component: LoginComponent },
    { path: 'edit',           component: CreateClienteComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit'
    }},
    { path: 'edit/:id',       component: CreateClienteComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit, Director, Gerente, EjecutivoMIT'
    }},
    { path: 'st',       component: SecuredTouchComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit'
    }},
    { path: 'reports', component: ReportsComponent},
    { path: 'status',       component: StatusComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit, Director, Gerente, EjecutivoMIT'
    }},
    { path: 'bl',       component: BlackListComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit'
    }},
    { path: 'nb',       component: NubariumComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit'
    }},
    { path: 'reports/detail/:trackID',       component: DetailComponent},
    { path: 'rules',       component: RulesComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit'
    }},
    { path: 'h2h',       component: H2hConfigComponent, 
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit'
    }},
    { path: 'fraud',       component: FraudComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit, Director, Gerente, EjecutivoMIT'
    }},
    { path: 'statistics/fraud',       component: StatisticsFraudComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit, Director, Gerente, EjecutivoMIT'
    }},
    { path: 'statistics/general',       component: StatisticsGeneralComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit, Director, Gerente, EjecutivoMIT'
    }},
    { path: 'analytics/analytics-specific',       component: AnalyticsSpecificComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit, Director, Gerente, EjecutivoMIT'
    }},
    { path: 'notifications',       component: NotificationsComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit, Director, Gerente, EjecutivoMIT'
    }},
    { path: 'analytics/analytics-kpis',       component: AnalyticsKpisComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit, Director, Gerente, EjecutivoMIT'
    }},
    { path: 'ficoScore',       component: FicoScoreComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit'
    }},
    { path: 'perfiles',       component: PerfilesComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit'
    }},
    { path: 'auditoria',       component: CirculoCreditoComponent,
    canActivate: [LoginGuard],
    data: {
      role: 'AdministradorMit'
    }},
];
