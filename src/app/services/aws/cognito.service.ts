import { Injectable } from '@angular/core';
import Auth from '@aws-amplify/auth';
import * as AWS from 'aws-sdk/global';
import * as AWSCognito from 'aws-sdk';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { urlMongo } from 'app/model/util/LigasUtil';
import { environment } from 'environments/environment';
import { CookieService } from '../other/cookie.service';



@Injectable({
  providedIn: 'root'
})
export class CognitoService {


  public cognitoCreds: AWS.CognitoIdentityCredentials;
  public accessKeyId: string;
  public secretAccessKey: string;
  public sessionToken: string;
  public role: string;
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor( private cookieService: CookieService, private http: HttpClient,) { }
  
  parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}

  async isLogguedIn() {
    let tok = this.cookieService.getCookie('jwtToken');
    console.log('el token es: ', tok);
    if(!tok || tok == null) {
      return false;
    }
    tok = this.parseJwt(tok);
    this.role = tok['cognito:groups']? tok['cognito:groups'][0]:'AdministradorMit';
    console.log('el token2 es: ', tok['cognito:groups'][0]);
    console.log('rol : ' , this.role);
    return true;
    // return await Auth.currentAuthenticatedUser()
    //   .then((success) => {
    //     console.log(success);
    //     this.role = success["signInUserSession"].accessToken.payload['cognito:groups'][0];
    //     if(this.role === "Ejecutivo"){
    //       this.role = "viewer"
    //     }
    //     console.log(this.role);
    //     return true;
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //     return false;
    //   });
  }

  async signOut() {
    try {
      sessionStorage.clear();
      localStorage.clear();
      this.cookieService.deleteAllCookies();
      await Auth.signOut();
    } catch (error) {
      console.log('error signing out: ', error);
    }
  }

  async signIn(username: string, password: string) {
    let resultSignIn = '';
    console.log('voy hacer el login');
    // if(this.isLogguedIn() ) {
    //   console.log('ya esta logueado');
    //   await Auth.signOut();
    // }
    console.log('no esta logueado');
    await Auth.signIn(username, password)
      .then(
        (success) => {
          console.log(success);
          if (success.signInUserSession) {
            this.cookieService.setCookie('jwtToken',success.signInUserSession.accessToken.jwtToken);
            //sessionStorage.setItem('jwtToken', success.signInUserSession.accessToken.jwtToken);
            this.getUserInfoFromAdmin(username);
          }
          if (success.challengeName === 'NEW_PASSWORD_REQUIRED') {
            console.log('segun es nuevo');
            resultSignIn = 'NEW';
          } else {
            if (!success.attributes.email_verified) {
              console.log('no voy a ir al else ');
              resultSignIn = 'NOT';
            } else {
            }
          }
        }
      )
      .catch(err => {
        console.log('error', err);
        resultSignIn = 'ERROR';
      });
    return resultSignIn;
  }

  async creaLasCredenciales() {
    const currentSession = await Auth.currentCredentials();
    this.accessKeyId = currentSession.accessKeyId;
    this.secretAccessKey = currentSession.secretAccessKey;
    this.sessionToken = currentSession.sessionToken;

    AWSCognito.config.credentials = AWS.config.credentials;

    AWS.config.update({
      region: environment.region,
      accessKeyId: this.accessKeyId,
      secretAccessKey: this.secretAccessKey,
      sessionToken: this.sessionToken
    });
    console.log("getCredentiales");
  }

  async creaClient(nombreDelCliente: string) {
    await this.creaLasCredenciales();
    const client = nombreDelCliente;
    const cognitoidentityserviceprovider = new AWSCognito.CognitoIdentityServiceProvider();
    const params = {
      ClientName: client, /* required */
      UserPoolId: environment.userPoolId, /* required */
      GenerateSecret: true,
      AllowedOAuthFlows: ['client_credentials'],
      AllowedOAuthScopes: ['MIT_Onboarding_RS/read', 'MIT_Onboarding_RS/write'],
      ExplicitAuthFlows: ['ALLOW_ADMIN_USER_PASSWORD_AUTH', 'ALLOW_REFRESH_TOKEN_AUTH', 'ALLOW_USER_SRP_AUTH', 'ALLOW_CUSTOM_AUTH'],
      RefreshTokenValidity: 3650,
      AllowedOAuthFlowsUserPoolClient: true,
    };
    const cred = await cognitoidentityserviceprovider.createUserPoolClient(params).promise();
    console.log(cred);
    return cred;
  }

  async changePass(username: string, oldPassword: string, newPassword: string) {
    let resultSignIn = 'OK';

    await Auth.signIn(username, oldPassword)
      .then(async (user) => {
        if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
          const { requiredAttributes } = user.challengeParam; // the array of required attributes, e.g ['email', 'phone_number']
          await Auth.completeNewPassword(
            user,               // the Cognito User Object
            newPassword,       // the new password
            // OPTIONAL, the required attributes
            {
             // email: user.challengeParam.userAttributes.email
            }
          ).then((userN: any) => {
            // at this time the user is logged in if no MFA required
            console.log(userN);
            if (userN.signInUserSession) {
              //sessionStorage.setItem('jwtToken', userN.signInUserSession.accessToken.jwtToken);
              document.cookie = "jwt=" + userN.signInUserSession.accessToken.jwtToken;
              this.getUserInfoFromAdmin(username);
            }
          }).catch(e => {
            console.log(e);
            resultSignIn = 'ERROR';
          });
        }
      }).catch(e => {
        console.log(e);
      });
    console.log('result sign in: ', resultSignIn);
    return resultSignIn;
  }

  // crear el usuario  correo-nombreComercio-codigoComercio (idMongo)
  async createUserFromAdmin(username: string, nombreComercio: string, codigoComercio: string, perfil: string) {
    let response = 'ERROR';
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    try {
      let responseService = await this.http.post(urlMongo + `cognito/usuario`, 
      {
        email: username,nombreComercio, codigoComercio, perfil,
        action : 'POST'}, { headers: this.headers })
        .toPromise();
        console.log('tengo los datos del servicio: ' , responseService);
        if(responseService && responseService['message'] && responseService['message'] == 'OK') {
          response = 'OK';
        }
    } catch (error) {
      console.log('ocurrio un error: ' , error);
    }

    return response;

    // await this.creaLasCredenciales();
    // const cognitoidentityserviceprovider = new AWSCognito.CognitoIdentityServiceProvider();
    // try {
    //   const params = {
    //     UserPoolId: environment.userPoolId, /* required */
    //     Username: username, /* required */
    //     DesiredDeliveryMediums: [
    //       'EMAIL'
    //     ],
    //     ForceAliasCreation: false,
    //     // MessageAction: 'SUPPRESS',
    //     // TemporaryPa: 'Prueba123$',
    //     UserAttributes: [
    //       {
    //         Name: 'email_verified',
    //         Value: "true"
    //       },
    //       {
    //         Name: 'email',
    //         Value: username
    //       },
    //       {
    //         Name: 'custom:nombre_comercio',
    //         Value: nombreComercio
    //       },
    //       {
    //         Name: 'custom:codigo_comercio',
    //         Value: codigoComercio
    //       },
    //     ]
    //   };
    //   const user = await cognitoidentityserviceprovider.adminCreateUser(params).promise();
    //   await this.ingresaValoresAGrupos(username, perfil);
    // } catch (error) {
    //   console.log('error signing up:', error.message);
    //     response = error.message;

      
    // }
  }

  async deleteUserFromAdmin(username: string) {
    let response = 'ERROR';
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    try {
      let responseService = await this.http.post(urlMongo + `cognito/usuario`, {id: username, action : 'DEL'}, { headers: this.headers })
        .toPromise();
        console.log('tengo los datos del servicio: ' , responseService);
        if(responseService && responseService['message'] && responseService['message'] == 'OK') {
          response = 'OK';
        }
    } catch (error) {
      console.log('ocurrio un error: ' , error);
    }
    return response;
    // let response = 'OK';
    // await this.creaLasCredenciales();
    // const cognitoidentityserviceprovider = new AWSCognito.CognitoIdentityServiceProvider();
    // const params = { Username: username, UserPoolId: environment.userPoolId };
    // try {
    //   const result = await cognitoidentityserviceprovider.adminDeleteUser(params).promise();
    //   // console.log(result);
    // } catch (e) {
    //   console.log('error al borrar el usuario: ', e);
    //   response = 'ERROR';
    // }
    // return response;
  }

  // async ingresaValoresAGrupos(username: string, perfil: string) {
  //   await this.creaLasCredenciales();
  //   const cognitoidentityserviceprovider = new AWSCognito.CognitoIdentityServiceProvider({ apiVersion: '2016-04-18' });
  //   const params = {
  //     GroupName: perfil,
  //     UserPoolId: environment.userPoolId,
  //     Username: username
  //   };
  //   const result = await cognitoidentityserviceprovider.adminAddUserToGroup(params)
  //     .promise();
  // }

  // Consulta usuarios que dio de alta el admin
  async getAllUsers(codigoComercio: string) {
    console.log('a ver si pinto este metodo al fin');
    let response;
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    try {
      response = await this.http.post(urlMongo + `cognito/usuario`, { action : 'GET',
      'codigoComercio': codigoComercio}, { headers: this.headers })
        .toPromise();
        console.log('tengo los datos del servicio: ' , response);
    } catch (error) {
      console.log('ocurrio un error: ' , error);
      return [];
    }
    return response;
  }

  // Consulta usuarios que dio de alta el admin
  // async getAllUsersUserMIT() {
  //   let result = {};
  //   await this.creaLasCredenciales();
  //   const cognitoidentityserviceprovider = new AWSCognito.CognitoIdentityServiceProvider({ apiVersion: '2016-04-18' });
  //   const params = {
  //     UserPoolId: environment.userPoolId
  //   };
  //   try {
  //     let usuarios = [];
  //     let PaginationToken;
  //     do {
  //       let response = await cognitoidentityserviceprovider.listUsers(params).promise();
  //       if(response.Users) {
  //         usuarios = usuarios.concat(response.Users);
  //       }
  //       PaginationToken = response.PaginationToken;
  //       console.log('respuesta de cognito' , response);
  //     }while(PaginationToken !== undefined);
  //     console.log(usuarios);

  //     //const filter = {Name: 'custom:codigo_comercio'};
  //     const nameFilter = 'custom:codigo_comercio';

  //     const filtrados = usuarios.filter((item) => {
        
  //       return !JSON.stringify(item.Attributes).includes(nameFilter);
  //      // return ( JSON.stringify( item.Attributes).indexOf (JSON.stringify(!filter)) >=  0);
  //   });
  //   result = filtrados;
  //   //result = usuarios['Users'];
  //   } catch (e) {
  //     console.log('error al obtener los uaurios: ', e);
  //   }
  //   return result;
  // }


  async forgotPass(username: string) {
    let result = 'OK';
    await Auth.forgotPassword(username)
      .then(
        (success) => {
          console.log(success);
        }
      )
      .catch(err => {
        console.log('error', err);
        result = 'ERROR';
      });
    return result;
  }

  async recoveryPass(username: string, code: string, newPass: string) {
    let result = 'OK';
    console.log(Auth);
    await Auth.forgotPasswordSubmit(username, code, newPass)
      .then(
        (success) => {
          console.log(success);
        }
      )
      .catch(err => {
        console.log('error', err);
        result = 'ERROR';
      });
    return result;
  }

  async getUserInfoFromAdmin(user: string) {
    await this.creaLasCredenciales();
    const cognitoidentityserviceprovider = new AWSCognito.CognitoIdentityServiceProvider();
    try {
      const params = {
        UserPoolId: environment.userPoolId, /* required */
        Username: user, /* required */
      }
      const usuario = await cognitoidentityserviceprovider.adminGetUser(params)
      .promise();
      if (usuario.UserAttributes) {
        const filtrados = usuario.UserAttributes.filter((item) => {
          if (item.Name === 'custom:codigo_comercio' && item.Value !== "0") {
            console.log('el codigo del comercio sera: ', item.Value);
            sessionStorage.setItem('codigo_comercio', item.Value);
          }

          if (item.Name === 'custom:nombre_comercio' && item.Value !== "EjecutivoMIT") {
            console.log('el nombre del comercio sera: ', item.Value);
            sessionStorage.setItem('nombre_comercio', item.Value);
          }

          
      });
      }
      console.log(usuario);
    } catch(e) {
      console.log('error', e);
    }
  }

  public getRole(){
    return this.role; 
  }
}
