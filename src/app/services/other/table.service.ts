import { Injectable } from '@angular/core';


@Injectable()
export class TableService {

  constructor() { }

  public createTableGeneral(idTable:string, user: any){
    var table = $(idTable).DataTable();
    table.clear()
    table.destroy();
    console.log("Estoy creando la tabla con", user);
    $.each(user, function (i, item) {
      var index = parseInt(i.toString()) + 1;
      $('<tr>').append(
      $('<td>').text(index.toString()),
      $('<td>').text(item['files'][0].fechaReporte),
      $('<td>').text(item.correo),
      $('<td>').text(item.bank),
      $('<td>').text(item.place),
      $('<td>').text(item.age),
      $('<td>').text("No disponible"),
      $('<td>').text(item.genre),
      $('<td>').text(item.identificationType),
      $('<td>').text(item.domain),
      $('<td>').text(item.autorizationType),
      $('<td>').text(item.lote),
      $('<td>').text(item.resultado)).appendTo('#dtResultReport tbody');
  
  });

  this.paginateTable(idTable)
  $('.dataTables_length').addClass('bs-select');
  $('#dtResultReport').addClass('table-responsive');

  }


  public createTableVariable(idTable:string, result: any){
    var table = $(idTable).DataTable();
    table.clear()
    table.destroy();
    console.log("Estoy creando la tabla con", result);
    $.each(result, function (i, item) {
      var index = parseInt(i.toString()) + 1;
      $('<tr>').append(
      $('<td>').text(item.name),
      $('<td>').text(item.total),
      $('<td>').text(item.percentageFraude),
      $('<td>').text(item.percentageTotal)).appendTo(idTable + ' tbody');
  
  });

  this.paginateTable(idTable)
  
  $('.dataTables_length').addClass('bs-select');

  }

  public createTableStatus(idTable:string, result: any){
    var table = $(idTable).DataTable();
    table.clear()
    table.destroy();
    console.log("Estoy creando la tabla con", result);
    $.each(result, function (i, item) {
      var index = parseInt(i.toString()) + 1;
      $('<tr>').append(
      $('<td>').text(index),
      $('<td>').text(item.trackId),
      $('<td>').text(item.correo),
      $('<td>').text(item.cliente),
      $('<td>').text(item.oferta),
      $('<td>').text(item.creacionDate),
      $('<td>').text(item.dateCambiada),
      $('<td>').text(item.previousStatus),
      $('<td>').text(item.currentStatus),
      $('<td>').text(item.cause)).appendTo(idTable + ' tbody');
  
  });

  this.paginateTable(idTable)
  
  $('.dataTables_length').addClass('bs-select');
  $('.table').addClass('table-responsive');
  }


  public createTableStatisticsFraud(idTable:string, result: any){
    console.log("LA tabla es", idTable)
    var table = $(idTable).DataTable();
    table.clear()
    table.destroy();
    console.log("Estoy creando la tabla con", result);
    $.each(result, function (i, item) {
      var index = parseInt(i.toString()) + 1;
      $('<tr>').append(
      $('<td>').text(index.toString()),
      $('<td>').text(item['files'][0].fechaReporte),
      $('<td>').text(item.correo),
      $('<td>').text(item.cliente),
      $('<td>').text(item.lote),
      $('<td>').text(item['files'][0].importeTransaccional),
      $('<td>').text(item['files'][0].importeQuebranto),
      $('<td>').text(item['files'][0].importeVencido),
      $('<td>').text(item['files'][0].resultado)).appendTo(idTable + ' tbody');
  
  });

  this.paginateTable(idTable)
  
  $('.dataTables_length').addClass('bs-select');
  $('.table').addClass('table-responsive');
  }

  public createTableFiles(idTable:string, result: any){
    console.log("LA tabla es", idTable)
    var table = $(idTable).DataTable();
    table.clear()
    table.destroy();
    console.log("Estoy creando la tabla con", result);
    $.each(result, function (i, item) {
      console.log("item", item.Name);
      var index = parseInt(i.toString()) + 1;
      $('<tr>').append(
      $('<td>').text(index.toString()),
      $('<td>').append(`<button [click]="${console.log('e item es: ' )}" >${item.Name}</button>`),
      $('<td>').text(item.Size)).appendTo(idTable + ' tbody');
  
  });

  this.paginateTable(idTable)
  
  $('.dataTables_length').addClass('bs-select');
  $('.table').addClass('table-responsive');
  }

  public paginateTable(idTable: string){
    $(idTable).DataTable({
        "pagingType": "full_numbers", // "simple" option for 'Previous' and 'Next' buttons only
        "language": {
          "lengthMenu": "Mostrar _MENU_ resultados por página",
          "zeroRecords": "No hay resultados",
          "info": "Resultados: _START_ de _END_ de _TOTAL_ de trámites",
          "infoEmpty": "No hay resultados disponibles",
          "search":         "Buscar:",
          "infoFiltered": "(filtered from _MAX_ total records)",
          "paginate": {
            "first":      "Primero",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
        },
        "drawCallback": () => {
          $( ".ButtonViewDetail" ).click((e) => {
            //var id = e.target.id
            //console.log("EL id seleccionado es", e.target.id);
            //this.viewDetail(id);
          });
        },
      
      });
      
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {

  }

}