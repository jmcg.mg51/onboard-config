import { Injectable } from '@angular/core';


@Injectable()
export class CookieService {

    constructor() { }

    //    public setCookie(name: string, value: string, expireDays: number, path: string = '') {
    public setCookie(name: string, value: string) {
        // let d: Date = new Date();
        // d.setTime(d.getTime() + expireDays * 24 * 60 * 60 * 1000);
        // let expires: string = `expires=${d.toUTCString()}`;
        // let cpath: string = path ? `; path=${path}` : '';
        //        document.cookie = `${name}=${value}; ${expires}${cpath}`;
        document.cookie = `${name}=${value};expires=60 * 1000`;
    }

    public getCookie(name: string) {
        let ca: Array<string> = document.cookie.split(';');
        let caLen: number = ca.length;
        let cookieName = `${name}=`;
        let c: string;

        for (let i: number = 0; i < caLen; i += 1) {
            c = ca[i].replace(/^\s+/g, '');
            if (c.indexOf(cookieName) == 0) {
                return c.substring(cookieName.length, c.length);
            }
        }
        return '';
    }

    public deleteAllCookies() {
        const cookies = document.cookie.split(";");
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i];
            const eqPos = cookie.indexOf("=");
            const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    }
}