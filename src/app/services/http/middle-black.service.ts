import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { CognitoService } from 'app/services/aws/cognito.service';
import { MiddleOfertaMongoService } from './middle-oferta-mongo.service';
import { urlMongo } from 'app/model/util/LigasUtil';
import { CookieService } from '../other/cookie.service';

@Injectable({
  providedIn: 'root'
})
export class MiddleBlackService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');
  mensajeSession = 'Sesión finalizada, favor de volver a ingresar';

  constructor(private http: HttpClient, public cognitoService: CognitoService, public router: Router, private cookieService: CookieService) { }

  async getBlackListReport() {
    let response = [];
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    await this.http.get(urlMongo + `catalogo/blacklist`, { headers: this.headers })
      // await this.http.get('https://dev-api.mitidentity.com/' + `cliente`)
      .toPromise().then(async (res) => {
        if (!res['errorMessage']) {
          if (Array.isArray(res)) {
            response = res;
          }
        }
      }).catch((err) => {
        console.log(err);
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = [];
        }
      });
    return response;
  }

  async createValueBlack(request: any) {
    let response = 'OK';
    request = {request: request};
    console.log('esto es lo que mandare');
    console.log(request);
    console.log(JSON.stringify(request));
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    console.log(urlMongo);
    await this.http.post(urlMongo + `catalogo/blacklist`, request, { headers: this.headers })
      // await this.http.get('https://dev-api.mitidentity.com/' + `cliente`)
      .toPromise().then(async (res) => {
        console.log(res);
        if (res['errorType']) {
          response = 'NOT';
        }
      }).catch((err) => {
        response = 'NOT';
        console.log(err);
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
      });
      console.log(response);
    return response;
  }

  async removeValueBlack(request: any) {
    let response = 'OK';
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    console.log(urlMongo);
    await this.http.put(urlMongo + `catalogo/blacklist`, request, { headers: this.headers })
      // await this.http.get('https://dev-api.mitidentity.com/' + `cliente`)
      .toPromise().then(async (res) => {
        console.log(res);
        if (res['errorType']) {
          response = 'NOT';
        }
      }).catch((err) => {
        response = 'NOT';
        console.log(err);
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
      });
      console.log(response);
    return response;
  }


  validaError(err: string) {
    if (err['error'] && err['error']['message'] && err['error']['message'].includes('expired')) {
      this.cognitoService.signOut();
      this.router.navigate(['/login']);
      return 'sessionExpired';
    }
    return '';
  }
}
