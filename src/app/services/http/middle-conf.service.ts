import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { MiddleOfertaMongoService } from './middle-oferta-mongo.service';
import { CognitoService } from '../aws/cognito.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { urlMongo } from 'app/model/util/LigasUtil';
import { CookieService } from '../other/cookie.service';

@Injectable({
  providedIn: 'root'
})
export class MiddleConfService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');
  mensajeSession = 'Sesión finalizada, favor de volver a ingresar';

  constructor(private http: HttpClient, private ofertaService: MiddleOfertaMongoService,
    public cognitoService: CognitoService, public router: Router, private spinner: NgxSpinnerService,
    private cookieService: CookieService) { }


/** Obtener estatus de Nuvarium */
async getStatusNuvarium() {
  let response = {};
  const token = this.cookieService.getCookie('jwtToken');
  this.headers = this.headers.set('Authorization', token);
  await this.http.get(urlMongo + `catalogo/nubarium`, { headers: this.headers })
    .toPromise().then(async (res) => {
      if (res['errorType']) {
        response = { error: 'Error, favor de volver a intentar' };
      } else {
        response = res;
      }
    }).catch((err) => {
      response = { error: 'Error, favor de volver a intentar' };
      const validaResponse = this.validaError(err);
      if(validaResponse !== '') {
        response = this.mensajeSession;
      }
    });
  return response;
}

async updateStatusNubarium(data: any) {
  const url = urlMongo + `catalogo/nubarium`;
  let response = 'OK';
  this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
  await this.http.put(url, data, { headers: this.headers })
    .toPromise().then(async (res) => {
      if (res['errorType']) {
        response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(res['errorType'])}`;
      }
    }).catch((err) => {
      console.log(err);
      response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(err)}`;
      const validaResponse = this.validaError(err);
      if(validaResponse !== '') {
        response = this.mensajeSession;
      }
    });
  return response;
}

/** END functions Nubarium */

/** Obtener estatus de rules */
async getStatusRules() {
  let response = {};
  const token = this.cookieService.getCookie('jwtToken');
  this.headers = this.headers.set('Authorization', token);
  await this.http.get(urlMongo + `catalogo/rules`, { headers: this.headers })
    .toPromise().then(async (res) => {
      if (res['errorType']) {
        response = { error: 'Error, favor de volver a intentar' };
      } else {
        response = res;
      }
    }).catch((err) => {
      response = { error: 'Error, favor de volver a intentar' };
      const validaResponse = this.validaError(err);
      if(validaResponse !== '') {
        response = this.mensajeSession;
      }
    });
  return response;
}

async updateStatusRules(data: any) {
  const url = urlMongo + `catalogo/rules`;
  let response = 'OK';
  this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
  await this.http.put(url, data, { headers: this.headers })
    .toPromise().then(async (res) => {
      if (res['errorType']) {
        response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(res['errorType'])}`;
      }
    }).catch((err) => {
      console.log(err);
      response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(err)}`;
      const validaResponse = this.validaError(err);
      if(validaResponse !== '') {
        response = this.mensajeSession;
      }
    });
  return response;
}

/** END functions Rules */

/** Obtener estatus de h2h */
async getFieldH2H(retries: boolean) {
  let response = {};
  const token = this.cookieService.getCookie('jwtToken');
  this.headers = this.headers.set('Authorization', token);
  let url = urlMongo + `catalogo/h2h`;
  if(retries && retries === true) {
    url += `?retries=true`
  }
  await this.http.get(url, { headers: this.headers })
    .toPromise().then(async (res) => {
      if (res['errorType']) {
        response = { error: 'Error, favor de volver a intentar' };
      } else {
        response = res;
      }
    }).catch((err) => {
      response = { error: 'Error, favor de volver a intentar' };
      const validaResponse = this.validaError(err);
      if(validaResponse !== '') {
        response = this.mensajeSession;
      }
    });
  return response;
}

async updateFieldH2H(data: any) {
  const url = urlMongo + `catalogo/h2h`;
  let response = 'OK';
  this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
  await this.http.put(url, data, { headers: this.headers })
    .toPromise().then(async (res) => {
      if (res['errorType']) {
        response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(res['errorType'])}`;
      }
    }).catch((err) => {
      console.log(err);
      response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(err)}`;
      const validaResponse = this.validaError(err);
      if(validaResponse !== '') {
        response = this.mensajeSession;
      }
    });
  return response;
}
/** END functions h2h */
validaError(err: string) {
  if (err['error'] && err['error']['message'] && err['error']['message'].includes('expired')) {
    this.cognitoService.signOut();
    this.router.navigate(['/login']);
    return 'sessionExpired';
  }
  return '';
}

/** INIT Servicios para configurar FICO SCORE */

async getLevelFicoScore() {
  let response = {};
  const token = this.cookieService.getCookie('jwtToken');
  this.headers = this.headers.set('Authorization', token);
  await this.http.get(urlMongo + `catalogo/ficoScore`, { headers: this.headers })
    .toPromise().then( (res) => {
      console.log("respuest", res);
      if (res['errorType']) {
        response = { error: 'Error, favor de volver a intentar' };
      } else {
        response = res;
      }
    }).catch((err) => {
      response = { error: 'Error, favor de volver a intentar' };
      const validaResponse = this.validaError(err);
      if(validaResponse !== '') {
        response = this.mensajeSession;
      }
    });
  return response;
}

async updateLevelFicoScore(data: any) {
  const url = urlMongo + `catalogo/ficoScore`;
  let response = 'OK';
  this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
  await this.http.put(url, data, { headers: this.headers })
    .toPromise().then(async (res) => {
      console.log(res);
      if (res && res['errorType']) {
        response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(res['errorType'])}`;
      }
    }).catch((err) => {
      console.log(err);
      response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(err)}`;
      const validaResponse = this.validaError(err);
      if(validaResponse !== '') {
        response = this.mensajeSession;
      }
    });
  return response;
}

/** FIN Servicios para configurar FICO SCORE */

}
