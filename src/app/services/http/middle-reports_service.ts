import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { CognitoService } from 'app/services/aws/cognito.service';
import { MiddleOfertaMongoService } from './middle-oferta-mongo.service';
import { urlMongo } from 'app/model/util/LigasUtil';
import { CookieService } from '../other/cookie.service';

@Injectable({
  providedIn: 'root'
})
export class MiddleReportsService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');
  mensajeSession = 'Sesión finalizada, favor de volver a ingresar';

  constructor(private http: HttpClient, public cognitoService: CognitoService, public router: Router, private cookieService: CookieService) { }


  async createReportOfUser(request: any){
    let response;
    console.log(request);
    console.log(JSON.stringify(request));
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    console.log(urlMongo);
    await this.http.post(urlMongo + `reporte/fileupfraud`, request, { headers: this.headers })
      .toPromise().then(async (res) => {
        response = res; 
      }).catch((err) => {
        response = err; 
      });
    console.log(response);
    return response;

  }

  async createReportOfFiles(typeReport:string, request: any){
    let response;
    console.log("Los valores del request son:", request.dateInit, request.dateEnd, request.cliente, request.resultado );
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    console.log(urlMongo);
    await this.http.get(urlMongo + `reporte/fileupfraud/filesreport?reportType=${typeReport}&dateInit=${request.dateInit}&dateEnd=${request.dateEnd}&cliente=${request.cliente}&resultado=${request.resultado}`, { headers: this.headers })
      .toPromise().then(async (res) => {
        response = res; 
      }).catch((err) => {
        response = err; 
      });
    console.log(response);
    return response;

  }


  async createReportOfStatus(request: any){
    let response;
    console.log("Los valores del request son:", request.dateInit, request.dateEnd, request.cliente, request.status );
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    console.log(urlMongo);
    await this.http.get(urlMongo + `usuario/status?cliente=${request.cliente}&status=${request.status}&dateInit=${request.dateInit}&dateEnd=${request.dateEnd}`, { headers: this.headers })
      .toPromise().then(async (res) => {
        response = res; 
      }).catch((err) => {
        response = err; 
      });
    console.log(response);
    return response;

  }

  async createReportOfAnalytics(idAnalytics: string){
    let response;
    console.log(idAnalytics);
    console.log(JSON.stringify(idAnalytics));
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    console.log(urlMongo);
    await this.http.get(urlMongo + `reporte/analitica/${idAnalytics}`, { headers: this.headers })
      .toPromise().then(async (res) => {
        response = res; 
      }).catch((err) => {
        response = err; 
      });
    console.log(response);
    return response;

  }

  async createReportDetailAnalytics(request){
    let response;
    console.log("el request es:", request);
    //console.log(idAnalytics);
    //console.log(JSON.stringify(idAnalytics));
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    console.log(urlMongo);
    await this.http.get(urlMongo + `reporte/analitica?cliente=${request.cliente}&dateInit=${request.dateInit}&dateEnd=${request.dateEnd}&oferta=${request.oferta}`, { headers: this.headers })
      .toPromise().then(async (res) => {
        response = res; 
      }).catch((err) => {
        response = err; 
      });
    console.log(response);
    return response;

  }

  async getReportFicoScore(){
    let response;
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    await this.http.get(urlMongo + `catalogo/ficoScore/report`, { headers: this.headers })
      .toPromise().then(async (res) => {
        console.log("response", res);
        response = res; 
      }).catch((err) => {
        response = err; 
      });
    console.log(response);
    return response;

  }







}