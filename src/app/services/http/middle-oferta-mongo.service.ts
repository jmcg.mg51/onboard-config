import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { urlMongo } from 'app/model/util/LigasUtil';
import { environment } from 'environments/environment';
import { CognitoService } from '../aws/cognito.service';
import { Router } from '@angular/router';
import { CookieService } from '../other/cookie.service';

@Injectable({
  providedIn: 'root'
})
export class MiddleOfertaMongoService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');
  mensajeSession = 'Sesión finalizada, favor de volver a ingresar';

  constructor(private http: HttpClient, public cognitoService: CognitoService,
              public router: Router, private cookieService: CookieService) { }

  async createOferta(id: string, data) {
    const token = this.cookieService.getCookie('jwtToken');
    console.log('entre al crear oferta');
    this.headers = this.headers.set('Authorization', token);
    const url = urlMongo + `oferta/${id}`;
    // console.log(url);
    let response = 'OK';
    console.log('voy a la promesa');
    const creaOf = this.http.post(url, data, { headers: this.headers });
    await creaOf.toPromise().
      then((res) => {
        console.log(res);
        if (res['errorType']) {
          response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(res['errorType'])}`;
        }
      }).catch((err) => {
        console.log(err);
        response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(err)}`;
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
      });
      console.log(response);
    return response;
  }

  // Get customer id
  async getOferta(id: string) {
    let response = {};
    const token = this.cookieService.getCookie('jwtToken');
    this.headers = this.headers.set('Authorization', token);
    await this.http.get(urlMongo + `oferta/${id}/`, { headers: this.headers })
      .toPromise().then((res) => {
        console.log(res);
        if (!res || res['errorType']) {
          response = { error: 'Error, favor de volver a intentar' };
        } else {
          response = res;
        }
      }).catch((err) => {
        console.log(err);
        response = { error: 'Error, favor de volver a intentar' };
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
      });
    return response;
  }

  // Update Oferta
  async updateOferta(id: string, data) {
    let response = 'OK';
    const token = this.cookieService.getCookie('jwtToken');
    this.headers = this.headers.set('Authorization', token);
    await this.http.put(urlMongo + `oferta/${id}/`, data, { headers: this.headers })
      .toPromise().
      then((res) => {
        // console.log(res);
        if (res['errorType']) {
          response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(res['errorType'])}`;
        }
      }).catch((err) => {
        console.log(err);
        response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(err)}`;
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
      });
    return response;
  }

  validaError(err: string) {
    if (err['error'] && err['error']['message'] && err['error']['message'].includes('expired')) {
      this.cognitoService.signOut();
      this.router.navigate(['/login']);
      return 'sessionExpired';
    }
    return '';
  }

}
