import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { CognitoService } from 'app/services/aws/cognito.service';
import { MiddleOfertaMongoService } from './middle-oferta-mongo.service';
import { urlMongo } from 'app/model/util/LigasUtil';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from '../other/cookie.service';

@Injectable({
  providedIn: 'root'
})
export class MiddleMongoService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');
  mensajeSession = 'Sesión finalizada, favor de volver a ingresar';

  constructor(private http: HttpClient, private ofertaService: MiddleOfertaMongoService,
    public cognitoService: CognitoService, public router: Router, private spinner: NgxSpinnerService,
    private cookieService: CookieService) { }

  // Get all customer
  async getCustomers() {
    let response = {};
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    await this.http.get(urlMongo + `cliente`, { headers: this.headers })
      // await this.http.get('https://dev-api.mitidentity.com/' + `cliente`)
      .toPromise().then(async (res) => {
        if (!res['errorMessage']) {
          if ( sessionStorage.getItem('codigo_comercio') && Array.isArray(res) ) {
            const filtrados = res.filter((item) => {
              return  item._id === `${sessionStorage.getItem('codigo_comercio')}`;
          });
            console.log(filtrados);
            res = filtrados;
          }
          response = res;
        }
      }).catch((err) => {
        console.log(err);
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
      });
    return response;
  }

  async getCustomer(id: string) {
    let response = {};
    const token = this.cookieService.getCookie('jwtToken');
    this.headers = this.headers.set('Authorization', token);
    await this.http.get(urlMongo + `cliente/${id}`, { headers: this.headers })
      .toPromise().then(async (res) => {
        if (res['errorType']) {
          response = { error: 'Error, favor de volver a intentar' };
        } else {
          response = res;
          const responseOferta = await this.ofertaService.getOferta(id);
          if (responseOferta['error']) {
            // response = { error: 'Error, favor de volver a intentar' };
            response = { response }
          } else {
            response = { response, responseOferta };
          }
        }
      }).catch((err) => {
        response = { error: 'Error, favor de volver a intentar' };
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
      });
    return response;
  }

  // Update Customer
  async updateCustomer(data, id: string) {
    const url = urlMongo + `cliente/${id}`;
    let response = 'OK';
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    console.log('lo que mandare es : ' , data);
    await this.http.put(url, data, { headers: this.headers })
      .toPromise().then(async (res) => {

        if (res['errorType']) {
          if (JSON.stringify(res['errorMessage']).includes('duplicate')) {
            response = `Error: El nombre de comercio o correo ya existe`;
          } else {
            response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(res['errorType'])}`;
          }
        }
        // else {
        //   response = await this.ofertaService
        //     .updateOferta(idOferta, dataOferta);
        // }

      }).catch((err) => {
        response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(err)}`;
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
      });
      console.log('regrese al crear ' , response);
    return response;
  }

  async createCustomer(data) {
    delete data._id;
    let response = 'OK';
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));

    await this.http.post(urlMongo + `cliente`, data, { headers: this.headers })
      .toPromise().then(async (res) => {
        console.log('Al crear el cliente');
        console.log(res);
        if (res['errorType']) {
          if (JSON.stringify(res['errorMessage']).includes('duplicate')) {
            response = `Error: El nombre de comercio o correo ya existe`;
          } else {
            response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(res['errorType'])}`;
          }
        }
        // else {
        //   response = await this.ofertaService
        //     .createOferta(res['insertedId'], dataOferta);
        // }
      }).catch((err) => {
        console.log(err);
        response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(err)}`;
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
      });


    return response;
  }

  async getSTValues() {
    let response = {};
    const token = this.cookieService.getCookie('jwtToken');
    this.headers = this.headers.set('Authorization', token);
    await this.http.get(urlMongo + `catalogo`, { headers: this.headers })
      .toPromise().then(async (res) => {
        if (res['errorType']) {
          response = { error: 'Error, favor de volver a intentar' };
        } else {
          response = res;
        }
      }).catch((err) => {
        response = { error: 'Error, favor de volver a intentar' };
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
      });
    return response;
  }

  async updateSTValues(data: any) {
    const url = urlMongo + `catalogo`;
    let response = 'OK';
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    await this.http.put(url, data, { headers: this.headers })
      .toPromise().then(async (res) => {
        if (res['errorType']) {
          response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(res['errorType'])}`;
        }
      }).catch((err) => {
        console.log(err);
        response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(err)}`;
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
      });
    return response;
  }

  validaError(err: string) {
    if (err['error'] && err['error']['message'] && err['error']['message'].includes('expired')) {
      this.cognitoService.signOut();
      this.router.navigate(['/login']);
      this.spinner.hide();
      return 'sessionExpired';
    }
    return '';
  }

  async getReportResult(request: any) {
    let response = {};
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    await this.http.post(urlMongo + `reporte`, request, { headers: this.headers })
      // await this.http.get('https://dev-api.mitidentity.com/' + `cliente`)
      .toPromise().then(async (res) => {
        response = res;
      }).catch((err) => {
        console.log(err);
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
      });

    return response;
  }

  async getDataOCR(request: any){
    let response = {};
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    await this.http.post(urlMongo + `reporte/ocrData`, request, { headers: this.headers })
    .toPromise().then(async (res) => {
      console.log("respuesta del servidor", res);
      response = res;
    }).catch((err) => {
      console.log(err);
      const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
    });

    return response;
  }

  async sendDataCliente(data) {
    let response = {};
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    await this.http.post(urlMongo + `cliente/email`, data, { headers: this.headers })
      .toPromise().then(async (res) => {
        console.log("respuesta SendInfoCliente",res);
        response=res;
      }).catch((err) => {
        console.log(err);
        response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(err)}`;
        const validaResponse = this.validaError(err);
        if(validaResponse !== '') {
          response = this.mensajeSession;
        }
      });


    return response;
  }

  async retrieEvaluation(trackId: string) {
    let response = '';
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    try {
      const resp = await this.http.post(urlMongo + `cliente/${trackId}/evaluacion`, {},{ headers: this.headers }).toPromise();
      if (resp['errorType']) {
        response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(resp['errorType'])}`;
      }
    } catch (error) {
      response = `Ocurrio un error, favor de reintentar. ${JSON.stringify(error)}`;
      this.validaError(error);
    }
    return response;
  }

}
