import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CognitoService } from 'app/services/aws/cognito.service';
import { urlMongo } from 'app/model/util/LigasUtil';
import { CookieService } from '../other/cookie.service';

@Injectable({
  providedIn: 'root'
})
export class NotificactionsService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');
  mensajeSession = 'Sesión finalizada, favor de volver a ingresar';

  constructor(private http: HttpClient, public cognitoService: CognitoService, public router: Router, private cookieService: CookieService) { }


  async saveEmailforNotification(data){
    let response;
    console.log(data);
    console.log(JSON.stringify(data));
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    console.log(urlMongo);
    await this.http.post(urlMongo + `catalogo/notification`, data, { headers: this.headers })
      .toPromise().then(async (res) => {
        response = res; 
      }).catch((err) => {
        response = "ERROR"; 
      });
    console.log(response);
    return response;

  }

  async getEmailsForNotifications(){
    let response;
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    console.log(urlMongo);
    await this.http.get(urlMongo + `catalogo/notification`, { headers: this.headers })
      .toPromise().then(async (res) => {
        response = res; 
      }).catch((err) => {
        response = []; 
      });
    console.log(response);
    return response;

  }


  async deleteEmailOfNotification(email:string){
    let response;
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    console.log(urlMongo);
    await this.http.delete(urlMongo + `catalogo/notification/${email}`, { headers: this.headers })
      .toPromise().then(async (res) => {
        response = res; 
      }).catch((err) => {
        response = "ERROR"; 
      });
    console.log(response);
    return response;

  }

 



}