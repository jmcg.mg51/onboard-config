import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CognitoService } from 'app/services/aws/cognito.service';
import { urlMongo } from 'app/model/util/LigasUtil';
import { CookieService } from '../other/cookie.service';

@Injectable({
  providedIn: 'root'
})
export class MiddleProcedureService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');
  mensajeSession = 'Sesión finalizada, favor de volver a ingresar';

  constructor(private http: HttpClient, public cognitoService: CognitoService, public router: Router, private cookieService: CookieService) { }

  async updateProcedure(request: any, trackID: string){
    let response = {};
    const url = urlMongo + `usuario/${trackID}/status`;
    console.log('esto es lo que mandare');
    console.log(request);
    this.headers = this.headers.set('Authorization', this.cookieService.getCookie('jwtToken'));
    console.log("TOKEN", this.headers);
    await this.http.put(url, request, { headers: this.headers })
      .toPromise().then((res) => {
        console.log("respuesta OK del servicio update trámite", res);
         response = res;

      }).catch((err) => {
        console.log("respuesta ERROR del servicio update trámite", err);
        response = err

      });

      return response;

  }


  ///usuario/{trackId}/status - PUT

//  {
//    "status":"PASS",
//    "cause":"por lo quesea!",
//    "authorizingPerson":"yony"
//}

}