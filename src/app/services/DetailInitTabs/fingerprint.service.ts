import { Injectable } from '@angular/core';
import * as L from 'leaflet';
import * as G from 'leaflet';

@Injectable()

export class FingerprintService {

  constructor() { }

    //FingerPrint
  fingerPrintResult: string;
  fingerPrintCountry: string;
  fingerPrintBot: string;
  fingerPrintRequestID: string;
  fingerPrintTag: string;
  fingerPrintIncognito: string;
  fingerPrintURL: string;
  fingerPrintIP: string;
  fingerPrintBrowser: string;
  fingerPrintBrowserFullVersion: string;
  fingerPrintSO: string;
  fingerPrintDevice: string;
  fingerPrintUserAgent: string;
  fingerPrintBotProbability: string;
  fingerPrintVisitorID: string;
  fingerPrintLatitud;
  fingerPrintLongitud;
  tabFingerPrint: boolean;
  tileLayer =  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=sk.eyJ1IjoibWl0b25ib2FyZGluZyIsImEiOiJja2UyMmV4NWIwNTRsMnNzNXk3ODlzcnM3In0.9qC0K_aTW4JzHFmZSYmWNQ', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'sk.eyJ1IjoibWl0b25ib2FyZGluZyIsImEiOiJja2UyMmV4NWIwNTRsMnNzNXk3ODlzcnM3In0.9qC0K_aTW4JzHFmZSYmWNQ'
    });
 


  initFingerprint(infoUser){
    this.tabFingerPrint = true;
    if(infoUser.globalScore.fingerPrintJS) {
      this.fingerPrintResult = infoUser.globalScore.fingerPrintJS.result;
      if(infoUser.globalScore.fingerPrintJS.ruleSet && infoUser.globalScore.fingerPrintJS.ruleSet.length > 0) {
        this.fingerPrintCountry = infoUser.globalScore.fingerPrintJS.ruleSet[0].message;
        this.fingerPrintBot = infoUser.globalScore.fingerPrintJS.ruleSet[1].message;
      }
    }
    if (infoUser.globalScore.fingerPrintJS.dataSource) {
      this.fingerPrintRequestID = infoUser.globalScore.fingerPrintJS.dataSource.requestId;
      this.fingerPrintVisitorID = infoUser.globalScore.fingerPrintJS.dataSource.visitorId;
      this.fingerPrintTag = infoUser.globalScore.fingerPrintJS.dataSource.tag.tag;
      this.fingerPrintIncognito = infoUser.globalScore.fingerPrintJS.dataSource.incognito;
      this.fingerPrintURL = infoUser.globalScore.fingerPrintJS.dataSource.url;
      this.fingerPrintIP = infoUser.globalScore.fingerPrintJS.dataSource.ip;
      this.fingerPrintBrowser = infoUser.globalScore.fingerPrintJS.dataSource.browserDetails.browserName;
      this.fingerPrintBrowserFullVersion = infoUser.globalScore.fingerPrintJS.dataSource.browserDetails.browserFullVersion;
      this.fingerPrintSO = infoUser.globalScore.fingerPrintJS.dataSource.browserDetails.os;
      this.fingerPrintDevice = infoUser.globalScore.fingerPrintJS.dataSource.browserDetails.device;
      this.fingerPrintUserAgent = infoUser.globalScore.fingerPrintJS.dataSource.browserDetails.userAgent;
      this.fingerPrintBotProbability = infoUser.globalScore.fingerPrintJS.dataSource.browserDetails.botProbability;
      this.fingerPrintLatitud = infoUser.globalScore.fingerPrintJS.dataSource.ipLocation.latitude;
      this.fingerPrintLongitud = infoUser.globalScore.fingerPrintJS.dataSource.ipLocation.longitude;
    }
  }

  
  initMapFingerPrint() {
    // var tileLayer =  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=sk.eyJ1IjoibWl0b25ib2FyZGluZyIsImEiOiJja2UyMmV4NWIwNTRsMnNzNXk3ODlzcnM3In0.9qC0K_aTW4JzHFmZSYmWNQ', {
    //   attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    //   maxZoom: 18,
    //   id: 'mapbox/streets-v11',
    //   tileSize: 512,
    //   zoomOffset: -1,
    //   accessToken: 'sk.eyJ1IjoibWl0b25ib2FyZGluZyIsImEiOiJja2UyMmV4NWIwNTRsMnNzNXk3ODlzcnM3In0.9qC0K_aTW4JzHFmZSYmWNQ'
    // });

    var cord1 = this.fingerPrintLatitud;
    var cord2 = this.fingerPrintLongitud;

    var mymap = L.map('mapid',{zoomControl: true, layers: [this.tileLayer],mmaxZoom: 18, minZoom: 6
    }).setView([cord1, cord2], 13);

    setTimeout(() => {mymap.invalidateSize(true)},1000);

    var greenIcon = L.icon({
      iconUrl: '/assets/img/pin-maker.jpg',
      iconSize:     [50, 50], // size of the icon
      shadowSize:   [50, 64], // size of the shadow
      iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
      shadowAnchor: [4, 62],  // the same for the shadow
      popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    });
    L.marker([cord1, cord2], {icon: greenIcon}).addTo(mymap).bindPopup("El usuario está aquí:"+cord1+" y "+cord2+"");
  } 

  initMapGPS(gpsLatitud, gpsLongitud) {
    // var tileLayer =  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=sk.eyJ1IjoibWl0b25ib2FyZGluZyIsImEiOiJja2UyMmV4NWIwNTRsMnNzNXk3ODlzcnM3In0.9qC0K_aTW4JzHFmZSYmWNQ', {
    //   attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    //   maxZoom: 18,
    //   id: 'mapbox/streets-v11',
    //   tileSize: 512,
    //   zoomOffset: -1,
    //   accessToken: 'sk.eyJ1IjoibWl0b25ib2FyZGluZyIsImEiOiJja2UyMmV4NWIwNTRsMnNzNXk3ODlzcnM3In0.9qC0K_aTW4JzHFmZSYmWNQ'
    // });
    console.log("las coordenadas que llegan son:", gpsLatitud, gpsLongitud);
    var cord1 = gpsLatitud;
    var cord2 = gpsLongitud;

    var mymap = G.map('mapGPS',{zoomControl: true, layers: [this.tileLayer],mmaxZoom: 18, minZoom: 6
    }).setView([cord1, cord2], 13);

    setTimeout(() => {mymap.invalidateSize(true)},1000);

    var greenIcon = L.icon({
      iconUrl: '/assets/img/pin-maker.jpg',
      iconSize:     [50, 50], // size of the icon
      shadowSize:   [50, 64], // size of the shadow
      iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
      shadowAnchor: [4, 62],  // the same for the shadow
      popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    });
    L.marker([cord1, cord2], {icon: greenIcon}).addTo(mymap).bindPopup("El usuario está aquí:"+cord1+" y "+cord2+"");
  } 

}