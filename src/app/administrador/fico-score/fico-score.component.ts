import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// import * as Swal from 'sweetalert2'


import { MiddleMongoService } from 'app/services/http/middle-mongo.service';
import { MiddleConfService } from 'app/services/http/middle-conf.service';
import { NgxSpinnerService } from 'ngx-spinner';

const Swal = require('sweetalert2');

@Component({
  selector: 'app-fico-score',
  templateUrl: './fico-score.component.html',
  styleUrls: ['./fico-score.component.css']
})
export class FicoScoreComponent implements OnInit {

  filtersLoaded: Promise<boolean>;
  isActiveValue = false;
  progreso = 0;
  progresoAlta = 0;
  progresoMedia = 0;
  progresoBaja = 0;
  progresoInformativo = 0;

  progresoFIS = 0;
  progresoAltaFIS = 0;
  progresoMediaFIS = 0;
  progresoBajaFIS = 0;
  progresoInformativoFIS = 0;

  progresoAltaInit = 0;
  progresoMediaInit = 0;
  progresoBajaInit = 0;
  progresoInformativoInit = 0;

  progresoAltaInitFIS = 0;
  progresoMediaInitFIS = 0;
  progresoBajaInitFIS = 0;
  progresoInformativoInitFIS = 0;
  showError; 

  constructor(private middle: MiddleMongoService, private middleConfig: MiddleConfService,
     private router: Router, private spinner: NgxSpinnerService) { }

  async ngOnInit() {
    sessionStorage.removeItem('tipoDeBusqueda');
    sessionStorage.removeItem('resultSearch');
    sessionStorage.removeItem('exportButton');
    sessionStorage.removeItem('tipoBusqueda');
    sessionStorage.removeItem('correoSearch');
    sessionStorage.removeItem('trackIDSearch');
    sessionStorage.removeItem('date1Search');
    sessionStorage.removeItem('date2Search');
    this.spinner.show();
    await this.getDBValues();
    this.filtersLoaded = Promise.resolve(true);
    this.spinner.hide();
  }

  activaswal() {
    Swal.fire('Any fool can use a computer');

  }
  async getDBValues () {
    const objectReturn = await this.middleConfig.getLevelFicoScore();
    if (objectReturn['error']) {
      console.log(objectReturn);
      this.router.navigate(['/dashboard']);
      this.spinner.hide();
    } else {
      console.log("valores de fico", objectReturn);
      if(objectReturn['FicoScore']){
        console.log("Si tengo fico", objectReturn['FicoScore'].exigente);
        this.progresoAlta = objectReturn['FicoScore'].exigente;
        this.progresoMedia = objectReturn['FicoScore'].medio;
        this.progresoBaja = objectReturn['FicoScore'].poco;

        this.progresoAltaInit = objectReturn['FicoScore'].exigente;
        this.progresoMediaInit = objectReturn['FicoScore'].medio;
        this.progresoBajaInit = objectReturn['FicoScore'].poco;
      }
      if(objectReturn['FIS']){
        this.progresoAltaFIS = objectReturn['FIS'].exigente;
        this.progresoMediaFIS = objectReturn['FIS'].medio;
        this.progresoBajaFIS = objectReturn['FIS'].poco;

        this.progresoAltaInitFIS = objectReturn['FIS'].exigente;
        this.progresoMediaInitFIS = objectReturn['FIS'].medio;
        this.progresoBajaInitFIS = objectReturn['FIS'].poco;

      }
      
      //this.progresoInformativo = 
      //this.isActiveValue = objectReturn['status'];
      //this.progreso = objectReturn['valor'];
    }
  }

  toogleCheckbox(event: any) {
    console.log('check = ' , event.target.name, event.target.value, event.target.checked);
    this.isActiveValue = event.target.checked;
  }

  async guardar() {
    this.spinner.show();
    console.log('voy a guardar');
    const FicoScore = {
      "exigente": this.progresoAlta,
      "medio": this.progresoMedia,
      "poco": this.progresoBaja,
      "type": "FiscoScore"
    }

    const FIS = {
      "exigente": this.progresoAltaFIS,
      "medio": this.progresoMediaFIS,
      "poco": this.progresoBajaFIS,
      "type": "FIS"
    }
    
    const data = {FicoScore, FIS}

  if(this.progresoAlta < this.progresoMedia || this.progresoMedia < this.progresoBaja ){
    this.showError = true;
    this.spinner.hide();
    return;
  }else{
    this.showError = false; 
  }

  if(this.progresoAltaFIS < this.progresoMediaFIS || this.progresoMediaFIS < this.progresoBajaFIS ){
    this.showError = true;
    this.spinner.hide();
    return;
  }else{
    this.showError = false; 
  }

  console.log("Lo que voy a guardar es:", data);
    const objectResponse = await this.middleConfig.updateLevelFicoScore(data);
   // const objectResponse = "OK";
    if (objectResponse === 'OK') {
      Swal.fire({
        icon: 'success',
        title: 'Valores Actualizados',
        // text: 'Something went wrong!',
      });
      await this.getDBValues();
      this.spinner.hide();
      // this.router.navigate(['/dashboard']);
    } else {
      console.log(objectResponse);
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Ocurrio un error',
        text: JSON.stringify(objectResponse),
      });
    }

  }

  asignValueToProgress(id, type){
    if(type === "FiscoScore"){
      switch (id) {
        case 'EXIGENTE':
          //this.buscarPorTexto(this.inputCorreo);
          this.progreso = this.progresoAlta;
            break;
        case 'MEDIO':
          this.progreso = this.progresoMedia;
          break;
        case 'BAJO':
          this.progreso = this.progresoBaja;
          break;
        case 'INFORMATIVO':
          this.progreso = this.progresoInformativo;
          break;
      }
    }else{
      switch (id) {
        case 'EXIGENTEFIS':
          //this.buscarPorTexto(this.inputCorreo);
          this.progresoFIS = this.progresoAltaFIS;
            break;
        case 'MEDIOFIS':
          this.progresoFIS = this.progresoMediaFIS;
          break;
        case 'BAJOFIS':
          this.progresoFIS = this.progresoBajaFIS;
          break;
        case 'INFORMATIVOFIS':
          this.progresoFIS = this.progresoInformativoFIS;
          break;
      }
    }

    
  }

  async onChangeValue(id, newValue: number, type:string) {
    //const elementHTML: any = document.getElementsByName('progreso')[0];
    console.log("valor id - type", id, type);
    const elementHTML: any = document.getElementById(id);
    if(type === "FiscoScore"){
      if (newValue > 1000) {
        this.progreso = 1000;
      } else if (newValue < 0) {
        this.progreso = 0;
      } else {
        this.progreso = newValue;
      }

      switch (id) {
        case 'EXIGENTE':
          //this.buscarPorTexto(this.inputCorreo);
          this.progresoAlta = this.progreso;
          elementHTML.value = Number(this.progresoAlta);
            break;
        case 'MEDIO':
          this.progresoMedia = this.progreso;
          elementHTML.value = Number(this.progresoMedia);
          break;
        case 'BAJO':
          this.progresoBaja = this.progreso;
          elementHTML.value = Number(this.progresoBaja);
          break;
        case 'INFORMATIVO':
          this.progresoInformativo = this.progreso;
          elementHTML.value = Number(this.progresoInformativo);
          break;
      }
    }else if(type === "FIS"){
      if (newValue > 1000) {
        this.progresoFIS = 1000;
      } else if (newValue < 0) {
        this.progresoFIS = 0;
      } else {
        this.progresoFIS = newValue;
      }

      switch (id) {
        case 'EXIGENTEFIS':
          //this.buscarPorTexto(this.inputCorreo);
          this.progresoAltaFIS = this.progresoFIS;
          elementHTML.value = Number(this.progresoAltaFIS);
            break;
        case 'MEDIOFIS':
          this.progresoMediaFIS = this.progresoFIS;
          elementHTML.value = Number(this.progresoMediaFIS);
          break;
        case 'BAJOFIS':
          this.progresoBajaFIS = this.progresoFIS;
          elementHTML.value = Number(this.progresoBajaFIS);
          break;
        case 'INFORMATIVOFIS':
          this.progresoInformativoFIS = this.progresoFIS;
          elementHTML.value = Number(this.progresoInformativoFIS);
          break;
      }
    }

    
  }

 async  cambiaValor(id, valor: number, type:string) {
    await this.asignValueToProgress(id, type);
    

    if(type === "FiscoScore"){
      console.log("change value fi");
      this.progreso += valor;
    if (this.progreso > 1000) {
      this.progreso = 1000;
    } else if (this.progreso < 0) {
      this.progreso = 0;
    }

      switch (id) {
        case 'EXIGENTE':
          this.progresoAlta = this.progreso;
            break;
        case 'MEDIO':
          this.progresoMedia = this.progreso;
          break;
        case 'BAJO':
          this.progresoBaja = this.progreso;
          break;
        case 'INFORMATIVO':
          this.progresoInformativo = this.progreso;
          break;
      }
    }
    else{
      console.log("change value FIS");
      this.progresoFIS += valor;
    if (this.progresoFIS > 1000) {
      this.progresoFIS = 1000;
    } else if (this.progresoFIS < 0) {
      this.progresoFIS = 0;
    }
      switch (id) {
        case 'EXIGENTEFIS':
          this.progresoAltaFIS = this.progresoFIS;
            break;
        case 'MEDIOFIS':
          this.progresoMediaFIS = this.progresoFIS;
          break;
        case 'BAJOFIS':
          this.progresoBajaFIS = this.progresoFIS;
          break;
        case 'INFORMATIVOFIS':
          this.progresoInformativoFIS = this.progresoFIS;
          break;
      }
    }

    
  }

}
