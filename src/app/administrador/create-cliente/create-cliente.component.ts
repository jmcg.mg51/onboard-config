import { CognitoService } from 'app/services/aws/cognito.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

import { MiddleMongoService } from './../../services/http/middle-mongo.service';
import { Ofertas, Clientes, Oferta, Users } from 'app/model/Clientes';
import { OfertaService } from 'app/services/http/oferta.service';
import { clearScreenDown } from 'readline';
import { serviciosCatalog } from 'app/model/util/OfertasServiciosUtil';

import { isEmpty, hexToB64Link, returnNameOfService } from 'app/model/UtilValidator';
const Swal = require('sweetalert2');

@Component({
  selector: 'app-create-cliente',
  templateUrl: './create-cliente.component.html',
  styleUrls: ['./create-cliente.component.css']
})
export class CreateClienteComponent implements OnInit {

  // para las ofertas
  ofertasExistentes: Ofertas;
  // servciosDeOfertasExistentes: Servicio[];
  ofertasN: {};
  abrir = false;

  // Clases
  cliente = new Clientes();
  ofertas: Ofertas;
  oferta: Oferta;
  ofertaAdd: Oferta;
  // servicio: Servicio;
  ofertasArray: Oferta[];
  arrayOffers: any;

  // Varibles
  agregar: boolean;
  id: string;
  submitted = false;
  isDup = false;
  botonValue = 'Guardar';
  index = 0;
  errorGenerico = '';
  mensaje: string;
  nombreOferta: string;
  userRole;
  texto="";
  response="";
  //  myForm = MyFormClient;
  // Formulario de Clientes en el HTML
  myForm = new FormGroup({
    nombre: new FormControl('', [this.isDupicate(), Validators.required]),
    correo: new FormControl('', [this.isDupicate(), Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
    password: new FormControl(''),
    token: new FormControl(''),
    telefono: new FormControl('', [Validators.pattern('(([0-9]{10} ext [0-9]{1,4})|([0-9]{10}))')]),
    lisPos: new FormControl(''),
    lis: new FormControl(''),
    hrefDaon: new FormControl(''),
    razon: new FormControl(''),
    nombreOferta: new FormControl(''),
    nombreOfertaAdd: new FormControl('', [this.isRequired()]),
    selectname: new FormControl(''),
    btnagregar: new FormControl(''),
  });
  filtersLoaded: Promise<boolean>;
  constructor(public router: Router, public userService: CognitoService,
    private actRoute: ActivatedRoute, private client: MiddleMongoService,
    private ofertaService: OfertaService, private spinner: NgxSpinnerService) {
    this.isDup = false;

    this.ofertas = new Ofertas();
    this.oferta = new Oferta();
  }

  async ngOnInit() {
    await this.spinner.show();
    this.userRole = this.userService.getRole();
    
    this.uploadImg = true;
    this.ofertas = new Ofertas();
    this.ofertas._id = undefined;
    this.oferta = new Oferta();

    this.ofertas.ofertas = [];
    this.cliente = new Clientes();

    // se recibe id cuando se va a actualizar el cliente
    this.actRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    if (this.id !== undefined && this.id !== '') {
      // await this.spinner.show();
      this.agregar = false;
      this.botonValue = 'Actualizar';
      const tokenOauth = sessionStorage.getItem('tokenEdit');
      this.myForm.controls['nombre'].disable();
      const objectResponse = await this.client.getCustomer(this.id);
       console.log(objectResponse);
      if (objectResponse['error']) {
        this.router.navigate(['/dashboard']);
        return;
      }
      
      Object.assign(this.cliente, objectResponse['response']);
      Object.assign(this.ofertas, objectResponse['responseOferta']);
      this.oferta = this.ofertas.ofertas[(this.ofertas.ofertas.length - 1)];
      this.arrayOffers = objectResponse;
      this.myForm.patchValue({
        correo: this.cliente.correo,
        nombre: this.cliente.nombre,
        password: this.cliente.pass,
        token: this.cliente.ApiToken,
        telefono: this.cliente.telefono,
        lisPos: this.cliente.listasBlancasPost,
        lis: this.cliente.listasBlancas,
        hrefDaon: this.cliente.hrefDaon,
        razon: this.cliente.razon
      });
      this.filtersLoaded = Promise.resolve(true);
      this.previewUrl=new Date().getDate().toString;
      this.previewUrl=this.cliente.urlIcon + '?temp=' + new Date().getTime();
      await this.spinner.hide();
    } else {
      this.agregar = true;
      await this.spinner.hide();
      this.filtersLoaded = Promise.resolve(true);
      
    }
    await this.sendOfer();
    if(this.userRole !== "AdministradorMit"){
      this.disableInputs();
    }
  }

  async sendOfer() {
    this.ofertasN = serviciosCatalog();
    this.abrir = true;
  }

  async getUserAndPass() {
    const datos = await (this.userService.creaClient(this.cliente.nombre));
    this.cliente.ApiToken = datos['UserPoolClient']['ClientId'];
    this.cliente.pass = datos['UserPoolClient']['ClientSecret'];
    if (this.cliente.ApiToken === undefined || this.cliente.pass === undefined) {
      return false;
    }
    return true;
  }

  async guardar() {
    this.errorGenerico = '';
    await this.spinner.show();
    this.isDup = false;
    this.submitted = true;
    if (this.myForm.invalid) {
      let mensajeErrorSwal = 'El correo es inválido';
      const validCorreo = this.f.correo.valid;
      const validTelefono = this.f.telefono.valid;
      if (validTelefono === false && validCorreo === true) {
        mensajeErrorSwal = 'El teléfono es inválido';
      } else if(validTelefono === false && validCorreo === false) {
        mensajeErrorSwal = 'El correo y teléfono son inválidos';
      }
      if(this.myForm.controls.correo.value===''){
        mensajeErrorSwal = 'No puedes dejar el correo vacío';
      }
      Swal.fire({
        icon: 'error',
        title: 'Ocurrio un error',
        text: mensajeErrorSwal,
      });
      await this.spinner.hide();
      return;
    }
    this.cliente._id = this.id;
    this.cliente.nombre = (this.myForm.controls.nombre.value) === '' ? this.cliente.nombre : this.myForm.controls.nombre.value;
    this.cliente.correo = (this.myForm.controls.correo.value) === '' ? this.cliente.correo : this.myForm.controls.correo.value;
    this.myForm.patchValue({ correo: this.cliente.correo });
    this.cliente.telefono = (this.myForm.controls.telefono.value) === '' ? this.cliente.telefono : this.myForm.controls.telefono.value;
    this.cliente.listasBlancasPost = (this.myForm.controls.lisPos.value) === '' ? this.cliente.listasBlancasPost : this.myForm.controls.lisPos.value;
    // this.cliente.listasBlancas = (this.myForm.controls.lis.value) === '' ? this.cliente.listasBlancas : ('' + this.myForm.controls.lis.value).split(",");
    this.cliente.listasBlancas = this.myForm.controls.lis.value
    this.cliente.hrefDaon = (this.myForm.controls.hrefDaon.value) === '' ? this.cliente.hrefDaon : this.myForm.controls.hrefDaon.value;
    this.cliente.iconFile = this.previewUrl;
    this.cliente.razon = (this.myForm.controls.razon.value) === '' ? this.cliente.razon : this.myForm.controls.razon.value;
    let mensajeSwal = 'Comercio creado';
    let respuestaDelServico = '';
    if (this.cliente._id === undefined) {
      const resultCognito = await this.getUserAndPass();
      if (!resultCognito) {
        respuestaDelServico = 'Error, favor de volver a intentar';
      } else {
        respuestaDelServico = await this.client.createCustomer(this.cliente);
      }
    } else {
      respuestaDelServico = await this.client.updateCustomer(this.cliente, this.id);
      mensajeSwal = 'Comercio Actualizado';
    }
    if (respuestaDelServico === 'OK') {
      Swal.fire({
        icon: 'success',
        title: mensajeSwal,
      });
      await this.spinner.hide();
      if (!this.id) {
        setTimeout(async () => {
          await this.spinner.hide();
          this.router.navigate(['/dashboard']);
        }, 1500);
      }
    } else {
      this.errorGenerico = respuestaDelServico;
      await this.spinner.hide();
    }
  }

  logout() {
    this.userService.signOut();
    this.router.navigate(['RutasPublicas.login']);
  }

  isDupicate(): ValidatorFn {
    return () => {
      if (this.isDup && this.submitted) {
        this.isDup = false;
        return { unique: true };
      } else {
        return null;
      }
    };
  }

  isRequired(): ValidatorFn {
    return () => {
      if (false) {
        return { required: true };
      } else {
        return null;
      }
    };
  }

regresar() {
  this.router.navigate(['/dashboard']);
}

get f() { return this.myForm.controls; }

fileData: File = null;
previewUrl:any = null;
uploadImg: boolean;
fileProgress(fileInput: any) {
  this.uploadImg = false;
      this.fileData = <File>fileInput.target.files[0];
      this.preview();
}

preview() {
    // Show preview
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      this.mensaje="Error, no es una imagen válida";
      this.uploadImg = false;
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
      this.uploadImg = true;
    }
}

disableInputs(){
  $("#DataForm :input").prop("disabled", true);
  $("#backButtonComercio").prop("disabled", false);
}

async enviarDatos(){
  console.log("this.cliente._id",this.cliente._id);
  let icon ="";
  let respuestaDelServico = await this.client.sendDataCliente({'idCliente':this.cliente._id});
  console.log("respuestaDelServico", respuestaDelServico);
  if(respuestaDelServico['httpStatus']=== 200){
    this.response = "El correo fue enviado";
    icon='success';
    this.cerrarModal();
  }else {
    this.response = "Hubo un problema, favor de volver a intentar";
    icon='error'
  }
  Swal.fire({
    icon:icon,
    title: 'Envio de información',
    text: this.response,
  });
}

async showModal(){
  console.log("this.ofertas",this.ofertas);
  this.texto = "";
  this.texto = "     Nombre cliente:  " + this.cliente.nombre + "\n";
  this.texto = this.texto + "Referencia interna:  " + this.cliente.hrefDaon + "\n";
  this.texto = this.texto + "                Webhook:  " + this.cliente.listasBlancasPost + "\n";
  this.texto = this.texto + "                 CallBack:  " + this.cliente.listasBlancas + "\n";
  this.texto = this.texto + "                    Correo:  " + this.cliente.correo + "\n";
  this.texto = this.texto + "          Secret token:  " + this.cliente.pass + "\n";
  this.texto = this.texto + "                Api token:  " + this.cliente.ApiToken + "\n";
  this.texto = this.texto + "\n";
  this.texto = this.texto + "               ***Ofertas***\n";
  if(this.ofertas.ofertas.length > 0){
    for(let oferta of this.ofertas.ofertas){
        this.texto = this.texto + "                      oferta:  " + oferta.nombre + "\n";
        this.texto = this.texto + "                          link:  " + this.generaLink() + "\n";
    }
  }
  const modal = document.getElementById('modalSendInfoCliente');
  modal.style.display = 'block';
}

async cerrarModal() {
  const modal = document.getElementById('modalSendInfoCliente');
  modal.style.display = 'none';
}

generaLink() {
  if (!this.cliente.nombre || this.cliente.nombre === '') {
    return;
  }
  let base64String = hexToB64Link(this.cliente._id, this.cliente.nombre);
  return base64String;
}

}
