import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleMongoService } from '../../../services/http/middle-mongo.service';
import { MiddleProcedureService } from '../../../services/http/middle-procedure.service';
import { IEvent } from 'angular8-yandex-maps';
import * as L from 'leaflet';
const Swal = require('sweetalert2');
import { MiddleBlackService } from '../../../services/http/middle-black.service';
import { CognitoService } from '../../../services/aws/cognito.service';
import { FingerprintService } from '../../../services/DetailInitTabs/fingerprint.service';
import { razonesFico, razonesFIS } from '../../../administrador/model/razones';
import { parseJSON } from 'jquery';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})

export class DetailComponent implements OnInit {

  //VAR: variables de propiedades de usuarios 
  infoUser: any;
  estatus = 'No disponible';
  score: string;
  blacklist = 'No disponible';
  secureTouch = 'No disponible';

  //Información usuario
  name = 'Dato no disponible';
  lastName = 'Dato no disponible';
  secondLastName = 'Dato no disponible';
  fullName = 'Dato no disponible';
  date = 'Dato no disponible';
  cellphone = 'Dato no disponible';
  email = 'Dato no disponible';
  birthday = 'Dato no disponible';
  pais = 'Dato no disponible';
  age = 'Dato no disponible';
  tag = '';
  comercio = '';
  oferta = '';
  curp = '';
  rfcValue: string;
  duplicated: string;
  listRules: any = [];

  //BlackList
  blackListResult: string;
  blackListMessage: string;

  //FingerPrint
  fingerPrintResult: string;
  fingerPrintCountry: string;
  fingerPrintBot: string;
  fingerPrintRequestID: string;
  fingerPrintTag: string;
  fingerPrintIncognito: string;
  fingerPrintURL: string;
  fingerPrintIP: string;
  fingerPrintBrowser: string;
  fingerPrintBrowserFullVersion: string;
  fingerPrintSO: string;
  fingerPrintDevice: string;
  fingerPrintBotAuth: string;
  fingerPrintUserAgent: string;
  fingerPrintBotProbability: string;
  fingerPrintVisitorID: string;
  fingerPrintLatitud;
  fingerPrintLongitud;

  //Clabe
  clabeResult: string;
  clabeMessage: string;
  clabeInfoH2H: string;

  //RFC
  rfcResult: string;
  rfcMessage: string;

  //Mark: views tab
  tabBiometrics: boolean;
  tabBlackList: boolean;
  tabClabe: boolean;
  tabFingerPrint: boolean = true;
  tabGPS: boolean;
  tabDocuments: boolean;
  tabSecureTouch: boolean;
  tabRFC: boolean;
  duplicatedTAB: boolean;

  //Mark: view
  identificationView: boolean;
  selfieView: boolean;
  liveView: boolean;
  selfieImage: string;
  liveImage: string;
  liveVideo: string;
  images: any = [];
  idTrack: any;
  resultBiometricsRules: any[];
  resultBiometricsStatus: string;
  showStatusChange: boolean;
  showErrorStatus: boolean;
  showBlackListSection: boolean;
  PASSVALUE: boolean;
  REJECTEDVALUE: boolean;
  showAlertBlackList: boolean;
  showAlertBlackListRFC: boolean;
  userRole;
  resultCedulaRules: any[];
  resultDataSourceCedula;
  resultSeguroSocialRules: any[];
  cedulaCodigo: string;
  cedulaValidacion: string;
  cedulaMensaje: string;
  cedulaResultado: string;
  datosAdicionales: any[];
  datosAdicionalesArray: any[];

  filtersLoaded: Promise<boolean>;

  //
  dateAlta: string;
  dateBaja: string;
  bossName: string;
  codeValitation: string;
  showDataSourceSS: boolean;
  showMessageCP: boolean;
  showCedulas: boolean;
  showDatosAdicionales: boolean;
  showPersonaMoral: boolean;
  nombrePersonaMoral: string;
  rfcPersonaMoral: string;
  cedulas;

  asalariado: string;
  showAsalariado: boolean;
  analisisScore;
  analisisEstatus;
  listRazones = [];
  listRazonesFIS = [];
  reglaFico: any;
  ficoScoreTAB: boolean;
  fisScore;
  fisResultado;
  fisScoreTAB: boolean;
  fisCodigo;
  errorFicoTrace;

  showComprobante: boolean;
  comprobanteImage: string;
  comprobanteColonia: string;
  comprobanteReferencia: string;
  comprobanteCalle: string;

  anio: string;
  nombre: string;
  numero: string;

  showContract: boolean;
  linkSource;

  urlConservation: string;
  showConservation: boolean;

  showFilesAttachments: boolean;
  fileAttachments = [];

  gpsLatitud;
  gpsLongitud

  constructor(private middleMongo: MiddleMongoService, public router: Router, private actRoute: ActivatedRoute,
    private spinner: NgxSpinnerService, private middleProcedure: MiddleProcedureService,
    private middle: MiddleBlackService, private cognito: CognitoService, private fingerP: FingerprintService) { }

  async ngOnInit() {
    this.spinner.show();
    this.actRoute.params.subscribe(params => {
      this.idTrack = params['trackID'];
    });
    try {
      await this.getInfoUser(this.idTrack);
      this.filtersLoaded = Promise.resolve(true);
    } catch (e) {
      console.log('No traje algun dato: ', e);
    }
    this.userRole = this.cognito.getRole();
    //Ocultar secciones, errores
    this.PASSVALUE = false;
    this.REJECTEDVALUE = false;
    this.showStatusChange = false;
    this.showErrorStatus = false;
    this.showBlackListSection = false;
    $("#changeStatus").prop('checked', false);
    this.spinner.hide();
  }

  ckeckChangeStatus() {
    var check = document.getElementById("changeStatus") as HTMLInputElement;
    if (check.checked) {
      this.showStatusChange = true;
      this.checkCurrentStatus(this.score);
    } else {
      this.showStatusChange = false;
      this.showBlackListSection = false;
    }
  }

  async getInfoUser(trackID: string) {
    this.spinner.show();
    var data = {
      trackId: trackID
    };
    this.infoUser = await this.middleMongo.getDataOCR(data);
    console.log("InfoUser,", this.infoUser);
    if (this.infoUser && this.infoUser !== null) {
      this.tag = this.infoUser.tag;
      this.comercio = this.infoUser.nombreCliente;
      this.oferta = this.infoUser.nombreOferta;

      if (this.infoUser.datosAdicionales) {
        const dataArray = [];
        this.showDatosAdicionales = true;
        this.datosAdicionales = this.infoUser.datosAdicionales
        console.log("Datos adicionales", this.datosAdicionales);

        const objectArray = Object.entries(this.datosAdicionales);
        objectArray.forEach(([key, value]) => {
          const data = {
            name: key,
            value: value
          }
          dataArray.push(data);
        });
        this.datosAdicionalesArray = dataArray;
        console.log("array datos", this.datosAdicionalesArray)

      } else {
        this.showDatosAdicionales = false;
      }
      try {
        if (this.infoUser && this.infoUser.userInfo && this.infoUser.userInfo.datosFiscales
          && this.infoUser.userInfo.datosFiscales.tipoPersona === 'moral') {
          this.showPersonaMoral = true;
          this.nombrePersonaMoral = this.infoUser.userInfo.datosFiscales.nombre;
          this.rfcPersonaMoral = this.infoUser.userInfo.datosFiscales.rfc.toUpperCase();
        } else {
          this.showPersonaMoral = false;
        }
      } catch (e) {
        console.log('dato no disponible : ', e);
      }
      if (this.infoUser['userInfo']['visualZone'] && this.infoUser['userInfo']['visualZone'][7]) {
        this.curp = this.infoUser['userInfo']['visualZone'][7].value;
      }

      this.curp = (!this.curp || this.curp === '' || this.curp === undefined) ? this.infoUser['userInfo']['curp'] : this.curp;
      if (this.infoUser.images) {
        this.tabDocuments = true;
        console.log("XXXXXX", this.infoUser.images);
        if (this.infoUser.images.documents) {
          this.identificationView = true;
          this.images = Object.entries(this.infoUser.images.documents);
          delete this.infoUser.images.documents;
        }

        if (this.infoUser.images.selfie) {

          this.selfieView = true;
          if (this.infoUser.images.selfie.value) {
            console.log("valor de imagen", this.infoUser.images.selfie.Location);
            this.selfieImage = 'data:image/JPG;base64,' + this.infoUser.images.selfie.value;
            delete this.infoUser.images.selfie.value;
          } else {
            this.selfieImage = this.infoUser.images.selfie.Location;
          }
        }

        // if (this.infoUser.images.live) {
        //   this.liveView = true;
        //   this.liveImage = this.infoUser.images.live.value;

        //   if (this.infoUser.images.live.value) {
        //     this.liveImage = 'data:image/JPG;base64,' + this.infoUser.images.live.value;
        //   } else {
        //     if (this.infoUser.images.live.image) {
        //       this.liveImage = this.infoUser.images.live.image.Location;
        //     }

        //   }
        //   if (this.infoUser.images && this.infoUser.images.live && this.infoUser.images.live.video) {
        //     this.liveVideo = this.infoUser.images.live.video.Location;
        //   }
        // }
      }

      if (this.infoUser && this.infoUser.userInfo && this.infoUser.userInfo['visualZone']) {
        if (this.infoUser.userInfo['visualZone'][25]) {
          this.fullName = this.infoUser.userInfo['visualZone'][25].value ?
            this.infoUser.userInfo['visualZone'][25].value.replace(/\^/g, ' ')
            : 'Dato no disponible'

          console.log(this.infoUser.userInfo['visualZone'][25].value);
          console.log(this.infoUser.userInfo['visualZone'][25].value.replace(/\^/g, ' '));

          // if(this.infoUser.userInfo['visualZone'][25].lastName && this.infoUser.userInfo['visualZone'][25].mothersLastName){
          //   this.lastName = this.infoUser.userInfo['visualZone'][25].lastName;
          //   this.secondLastName = this.infoUser.userInfo['visualZone'][25].mothersLastName;
          //   this.fullName = this.name ? this.name.concat(' ', this.lastName, ' ', this.secondLastName) : this.lastName;
          // }else if(this.infoUser.userInfo['visualZone'][8]){
          //   this.lastName = this.infoUser.userInfo['mrz'][8].value; 
          //   this.fullName = this.name.concat(' ' , this.lastName);
          // }


        }

        this.birthday = this.infoUser.userInfo['visualZone'][5] ? this.infoUser.userInfo['visualZone'][5].value : this.birthday;
        this.age = this.infoUser.userInfo['visualZone'][185] ? this.infoUser.userInfo['visualZone'][185].value : this.age;
        this.pais = this.infoUser.userInfo['visualZone'][38] ? this.infoUser.userInfo['visualZone'][38].value : this.pais;
        this.cellphone = this.infoUser['userInfo'].telefono;

        if (this.infoUser['userInfo'].datosFiscales) {
          this.rfcValue = this.infoUser['userInfo'].datosFiscales.rfc;
        }
        this.date = this.infoUser.date;
        this.email = this.infoUser.correo;
      }
      this.score = this.infoUser.globalScore.finalResult;

      if (this.infoUser.globalScore.blackList) {
        this.tabBlackList = true;

        if (this.infoUser.globalScore.blackList === null || this.infoUser.globalScore.blackList === "") {
          this.changeColor("Listas Negras");
        } else {
          this.blackListResult = this.infoUser.globalScore.blackList.result;
          this.blackListMessage = this.infoUser.globalScore.blackList.ruleSet[0].message;
        }


      }
      if (this.infoUser.globalScore.gps) {
        this.tabGPS = true;
      }

      if (this.infoUser.globalScore.fingerPrintJS) {
        this.fingerP.initFingerprint(this.infoUser);
        this.tabFingerPrint = true;
        this.fingerPrintResult = this.infoUser.globalScore.fingerPrintJS.result;
        if(this.infoUser.globalScore.fingerPrintJS.ruleSet && this.infoUser.globalScore.fingerPrintJS.ruleSet.length > 0) { 
          this.fingerPrintCountry = this.infoUser.globalScore.fingerPrintJS.ruleSet[0].message;
          this.fingerPrintBot = this.infoUser.globalScore.fingerPrintJS.ruleSet[1].message;
        }

        this.fingerPrintBotAuth = JSON.stringify(this.infoUser.globalScore.fingerPrintJS.bot, null, 2);
        if (this.infoUser.globalScore.fingerPrintJS.dataSource) {
          this.fingerPrintRequestID = this.infoUser.globalScore.fingerPrintJS.dataSource.requestId;
          this.fingerPrintVisitorID = this.infoUser.globalScore.fingerPrintJS.dataSource.visitorId;
          this.fingerPrintTag = this.infoUser.globalScore.fingerPrintJS.dataSource.tag.tag;
          this.fingerPrintIncognito = this.infoUser.globalScore.fingerPrintJS.dataSource.incognito;
          this.fingerPrintURL = this.infoUser.globalScore.fingerPrintJS.dataSource.url;
          this.fingerPrintIP = this.infoUser.globalScore.fingerPrintJS.dataSource.ip;
          this.fingerPrintBrowser = this.infoUser.globalScore.fingerPrintJS.dataSource.browserDetails.browserName;
          this.fingerPrintBrowserFullVersion = this.infoUser.globalScore.fingerPrintJS.dataSource.browserDetails.browserFullVersion;
          this.fingerPrintSO = this.infoUser.globalScore.fingerPrintJS.dataSource.browserDetails.os;
          this.fingerPrintDevice = this.infoUser.globalScore.fingerPrintJS.dataSource.browserDetails.device;
          this.fingerPrintUserAgent = this.infoUser.globalScore.fingerPrintJS.dataSource.browserDetails.userAgent;
          this.fingerPrintBotProbability = this.infoUser.globalScore.fingerPrintJS.dataSource.browserDetails.botProbability;
          this.fingerPrintLatitud = this.infoUser.globalScore.fingerPrintJS.dataSource.ipLocation.latitude;
          this.fingerPrintLongitud = this.infoUser.globalScore.fingerPrintJS.dataSource.ipLocation.longitude;
        }
      }

      if (this.infoUser.globalScore.securedTouch) {
        this.tabSecureTouch = true;
        this.secureTouch = this.infoUser.globalScore.securedTouch.result;
      }

      if (this.infoUser.globalScore.clabe) {
        this.tabClabe = true;

        //this.infoUser.globalScore.clabe = null; 
        if (this.infoUser.globalScore.clabe === null || this.infoUser.globalScore.clabe === "") {
          this.changeColor("Clabe");
        } else {
          this.clabeResult = this.infoUser.globalScore.clabe.result;
          this.clabeMessage = this.infoUser.globalScore.clabe.ruleSet[0].message;
          let jsonArray1 = this.infoUser.globalScore.clabe.dataSource;
          let jsonArray2 = this.infoUser.globalScore.clabe.ruleSet[1];
          this.clabeInfoH2H = JSON.stringify({ cdp: jsonArray1, info: jsonArray2 }, null, '\t');
        }

      }

      if (this.infoUser.globalScore.rfc) {
        this.tabRFC = true;
        this.rfcResult = this.infoUser.globalScore.rfc.result;
        this.rfcMessage = this.infoUser.globalScore.rfc.ruleSet[0].message;
      }

      if (this.infoUser.globalScore.biometrics) {
        this.tabBiometrics = true;
        //this.infoUser.globalScore.biometrics.result = null; 
        if (this.infoUser.globalScore.biometrics.result === null || this.infoUser.globalScore.biometrics.result === "") {
          this.changeColor("Biometricos");
        } else {
          this.resultBiometricsStatus = this.infoUser.globalScore.biometrics.result;
          this.resultBiometricsRules = this.infoUser.globalScore.biometrics.ruleSet;
        }

      }
      if (this.infoUser.estatus) {
        this.estatus = this.infoUser.estatus
      }

      if (this.infoUser.globalScore.cedula) {
        this.resultCedulaRules = this.infoUser.globalScore.cedula.ruleSet;
        this.resultDataSourceCedula = this.infoUser.globalScore.cedula.dataSource;

        if (this.infoUser.globalScore.cedula.userInfo) {
          this.anio = this.infoUser.globalScore.cedula.userInfo.anio;
          this.nombre = this.infoUser.globalScore.cedula.userInfo.nombre;
          this.numero = this.infoUser.globalScore.cedula.userInfo.numero;
        }

        if (this.resultDataSourceCedula = this.infoUser.globalScore.cedula.dataSource.cedulas) {
          this.showMessageCP = false;
          this.showCedulas = true;
          this.cedulas = this.infoUser.globalScore.cedula.dataSource.cedulas;
        } else {
          this.showCedulas = false;
          this.showMessageCP = true;
          this.cedulaMensaje = this.infoUser.globalScore.cedula.dataSource.mensaje;
        }
        this.cedulaCodigo = this.infoUser.globalScore.cedula.dataSource.codigoMensaje;
        this.cedulaValidacion = this.infoUser.globalScore.cedula.dataSource.codigoValidacion;
        this.cedulaResultado = this.infoUser.globalScore.cedula.dataSource.estatus;

      }

      if (this.infoUser.globalScore.ss) {
        this.resultSeguroSocialRules = this.infoUser.globalScore.ss.ruleSet
        if (this.infoUser.globalScore.ss.dataSource) {
          this.showDataSourceSS = true;
          this.bossName = this.infoUser.globalScore.ss.dataSource.historialLaboral.nombrePatron;
          this.codeValitation = this.infoUser.globalScore.ss.dataSource.codigoValidacion;
          this.dateAlta = this.infoUser.globalScore.ss.dataSource.historialLaboral.fechaAlta;
          this.dateBaja = this.infoUser.globalScore.ss.dataSource.historialLaboral.fechaBaja;

        } else {
          this.showDataSourceSS = false;
        }


      }

      if (this.infoUser.seguroSocial && this.infoUser.seguroSocial.asalariado) {
        console.log("Seguro Social", this.infoUser.seguroSocial);
        if (this.infoUser.seguroSocial.asalariado === true) {
          this.asalariado = "SI";
          this.showAsalariado = true;
        } else {
          this.asalariado = "NO";
          this.showAsalariado = true;
        }
      } else {
        this.showAsalariado = false;
      }


      if (this.infoUser.fingerPrintJS) {
        this.fingerPrintRequestID = this.infoUser.fingerPrintJS.requestId
        this.fingerPrintVisitorID = this.infoUser.fingerPrintJS.visitorId
        this.fingerPrintTag = this.infoUser.fingerPrintJS.tag.tag;
        this.fingerPrintIncognito = this.infoUser.fingerPrintJS.incognito;
        this.fingerPrintURL = this.infoUser.fingerPrintJS.url;
        this.fingerPrintIP = this.infoUser.fingerPrintJS.ip;
        this.fingerPrintBrowser = this.infoUser.fingerPrintJS.browserDetails.browserName;
        this.fingerPrintBrowserFullVersion = this.infoUser.fingerPrintJS.browserDetails.browserFullVersion;
        this.fingerPrintSO = this.infoUser.fingerPrintJS.browserDetails.os;
        this.fingerPrintDevice = this.infoUser.fingerPrintJS.browserDetails.device
        this.fingerPrintUserAgent = this.infoUser.fingerPrintJS.browserDetails.userAgent
        this.fingerPrintBotProbability = this.infoUser.fingerPrintJS.browserDetails.botProbability
      }

      if (this.infoUser.gps) {
        console.log("entro a gps");
        this.tabGPS = true;
        this.gpsLatitud = this.infoUser.gps.lat;
        this.gpsLongitud = this.infoUser.gps.lng;
        // this.fingerP.initMapGPS(this.gpsLatitud, this.gpsLongitud);
      }
      if (this.infoUser.globalScore.duplicated) {
        this.duplicatedTAB = true;
        this.infoUser.globalScore.duplicated.result === "PASS" ? this.duplicated = "NO" : this.duplicated = "SI";
        this.listRules = this.infoUser.globalScore.duplicated.ruleSet;
      } else {
        this.duplicated = "No disponible";
        this.duplicatedTAB = false;
      }
      /** TO DO ANALISIS CREDITICIO DUMMY */

      if (this.infoUser.globalScore.ficoScore) {
        this.ficoScoreTAB = true;

        if (this.infoUser.globalScore.ficoScore.dataSource && this.infoUser.globalScore.ficoScore.dataSource.error) {
          this.errorFicoTrace = JSON.stringify(this.infoUser.globalScore.ficoScore.dataSource.error);
          if (this.infoUser.globalScore.ficoScore.dataSource.error.data) {
            this.infoUser.globalScore.ficoScore.dataSource = this.infoUser.globalScore.ficoScore.dataSource.error.data;
          }
        }

        if (this.infoUser.globalScore.ficoScore.dataSource && this.infoUser.globalScore.ficoScore.dataSource.score) {
          console.log("valor FICO", this.infoUser.globalScore.ficoScore.dataSource.score.valor);
          this.analisisScore = this.infoUser.globalScore.ficoScore.dataSource.score.valor;
          let razones = this.infoUser.globalScore.ficoScore.dataSource.score.razones;
          for (var i = 0; i < razones.length; i++) {
            let objectResult = await this.getMessageByCode(razonesFico, razones[i]);
            this.listRazones.push(objectResult);

          }
        }
        this.analisisEstatus = "No disponible"//this.infoUser.globalScore.ficoScore.result;
        //this.listRazones = this.infoUser.globalScore.ficoScore.dataSource.score.razones;

        //TODOFICO
        console.log("lista razones", this.listRazones);
        this.reglaFico = this.infoUser.globalScore.ficoScore.ruleSet;
        if (this.reglaFico && this.reglaFico[0]) {
          this.analisisEstatus = this.reglaFico[0].result;
          this.reglaFico = JSON.stringify(this.reglaFico[0], undefined, '\t');
        }

        try {
          if (this.infoUser.globalScore.fis) {
            this.fisScoreTAB = true;
            if (this.infoUser.globalScore.fis.dataSource && this.infoUser.globalScore.fis.dataSource.error) {
              this.fisScore = JSON.stringify(this.infoUser.globalScore.fis.dataSource.error);
            }
            else {
              console.log("valores de FIS", this.infoUser.globalScore.fis)
              if (this.infoUser.globalScore.fis.dataSource &&
                this.infoUser.globalScore.fis.dataSource.scoreNoHit) {
                this.fisScore = this.infoUser.globalScore.fis.dataSource.scoreNoHit.valor;
              } else {
                this.fisScore = "Ocurrio un error del proveedor, No se pudo consultar"
              }
            }

            this.fisResultado = this.infoUser.globalScore.fis.result;
          }

        } catch (error) {
          console.log(error);
        }

      } else {
        this.ficoScoreTAB = false;
      }
      /** TO DO FINAL ANALISIS CREDITICIO DUMMY */

      /** Comprobante de domicilio */
      if (this.infoUser.globalScore.comprobante) {
        console.log(this.infoUser.globalScore.comprobante);
        this.showComprobante = true;
        this.tabDocuments = true;
        // this.comprobanteCalle = this.infoUser.globalScore.comprobante.dataSource.calle;
        // this.comprobanteColonia = this.infoUser.globalScore.comprobante.dataSource.colonia;
        // this.comprobanteReferencia = this.infoUser.globalScore.comprobante.dataSource.referencia;

        if (this.infoUser.globalScore.comprobante.dataSource.calle) {
          this.comprobanteCalle = this.infoUser.globalScore.comprobante.dataSource.calle;
        } else {
          this.comprobanteCalle = "No se pudo recuperar la información"
        }

        if (this.infoUser.globalScore.comprobante.dataSource.colonia) {
          this.comprobanteColonia = this.infoUser.globalScore.comprobante.dataSource.colonia;
        } else {
          this.comprobanteColonia = "No se pudo recuperar la información"
        }

        if (this.infoUser.globalScore.comprobante.dataSource.referencia) {
          this.comprobanteReferencia = this.infoUser.globalScore.comprobante.dataSource.referencia;
        } else {
          this.comprobanteReferencia = "No se pudo recuperar la información"
        }

        if (this.infoUser.globalScore.comprobante.dataSource && this.infoUser.globalScore.comprobante.dataSource.multimedia) {
          this.comprobanteImage = this.infoUser.globalScore.comprobante.dataSource.multimedia.Location;
          if(this.infoUser.globalScore.comprobante.dataSource.multimedia.value) {
            this.comprobanteImage = 'data:image/JPG;base64,' + this.infoUser.globalScore.comprobante.dataSource.multimedia.value;
          }
        }

      } else {
        this.showComprobante = false;
      }

      if (this.infoUser && this.infoUser.terminos) {
        this.tabDocuments = true;
        console.log("tengo terminos");
        this.showContract = true;
        //this.linkSource = `data:application/pdf;base64,${this.infoUser.terminos}`;
        this.linkSource = this.infoUser.terminos;
        delete this.infoUser.terminos;
      } else {
        this.showContract = false;
      }

      if (this.infoUser.images && this.infoUser.images.conservation) {
        this.tabDocuments = true;
        this.showConservation = true;
        if(this.infoUser.images.conservation.Location) {
          this.urlConservation = this.infoUser.images.conservation.Location;
          console.log(this.urlConservation);
          if (this.urlConservation.includes('+')) {
            this.urlConservation = this.urlConservation.replace(/\+/g, '%2B');
          }
        } else {
          this.urlConservation = this.infoUser.images.conservation.value;
          delete this.infoUser.images.conservation.value;
        }
      }

      if (this.infoUser.images && this.infoUser.images.archivosAdjuntos) {
        this.tabDocuments = true;
        this.showFilesAttachments = true;
        console.log("archivos", this.infoUser.images.archivosAdjuntos);
        for (var i = 0; i < this.infoUser.images.archivosAdjuntos.length; i++) {
          if(this.infoUser.images.archivosAdjuntos[i].name.includes('idDocFileUpload')) {
            if(this.infoUser.images.archivosAdjuntos[i].content && this.infoUser.images.archivosAdjuntos[i].content.value) {
              delete this.infoUser.images.archivosAdjuntos[i].content.value;
            }
            continue;
          }
          let location;
          if(this.infoUser.images.archivosAdjuntos[i].content.value) {
            // "data:application/pdf," +
            location = "data:application/pdf;base64," + this.infoUser.images.archivosAdjuntos[i].content.value;
            delete this.infoUser.images.archivosAdjuntos[i].content.value;
            // location = this.convertDataURIToBinary(location);
          } else {
            location = this.infoUser.images.archivosAdjuntos[i].content.Location;
          }
          this.fileAttachments.push({
            name: (this.infoUser.images.archivosAdjuntos[i].name).replace(/-/g, ' '),
            location
          });
        }
        //this.fileAttachments = this.infoUser.images.archivosAdjuntos;
        //this.fileAttachments.name = this.infoUser.images.archivosAdjuntos
        // console.log("los archivos son:", this.fileAttachments);

      }


    } else if (this.infoUser === null) {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Ocurrio un error',
        text: 'Error al obtener los datos del registro',
      });
      this.goBack();
    }
    this.spinner.hide();
  }

  BASE64_MARKER = ";base64,";
  convertDataURIToBinary (dataURI) {
    const base64Index = dataURI.indexOf(this.BASE64_MARKER) + this.BASE64_MARKER.length;
    const base64 = dataURI.substring(base64Index);
    const raw = window.atob(base64);
    const rawLength = raw.length;
    const array = new Uint8Array(new ArrayBuffer(rawLength));
  
    for (let i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  };

  goBack() {
    localStorage.setItem('exportButton', 'true');
    this.router.navigate(['/dashboard/reports']);
  }

  public onMouse(event: IEvent): void {
    if (event.type === 'mouseenter') {
      event.instance.options.set('preset', 'islands#greenIcon');
    }
    if (event.type === 'mouseleave') {
      event.instance.options.unset('preset');
    }
  }


  onTabChanged($event) {
    var huellaTab = $event.tab.textLabel
    if (huellaTab == "Huella") {
      this.fingerP.initMapFingerPrint();
      //this.fingerP.initMapGPS(this.gpsLatitud, this.gpsLongitud);
    }
    if (huellaTab == "GPS") {
      //this.fingerP.initMapFingerPrint();
      this.fingerP.initMapGPS(this.gpsLatitud, this.gpsLongitud);
    }
  }

  async saveStatus() {
    this.spinner.show();
    var inputEstatus = document.getElementById("selectStatus") as HTMLInputElement
    var estatus = inputEstatus.value;
    var inputDescriptionChange = document.getElementById("descriptionChange") as HTMLInputElement
    var descriptionChange = inputDescriptionChange.value;
    var inputAuthorizationPerson = document.getElementById("authorizationPerson") as HTMLInputElement
    var authorizationPerson = inputAuthorizationPerson.value;

    //Inputs validation
    if (estatus === "0" || descriptionChange === "" || authorizationPerson === "") {
      this.showErrorStatus = true;
      this.spinner.hide();
      return;
    } else {
      this.showErrorStatus = false;
      var data = { 'status': estatus, 'cause': descriptionChange, "authorizingPerson": authorizationPerson };
      this.updateProcedure(estatus, data, this.idTrack);
    }
  }

  changeSelectStatus(value: string) {
    if (value === "REJECTED") {
      this.showBlackListSection = true;
    } else {
      this.showBlackListSection = false;
    }
  }

  async updateProcedure(currentStatus, data, id) {
    switch (currentStatus) {
      case "PASS": {
        this.saveProcedure(data, id);
        break;
      }
      case "REJECTED": {
        const requestToSend = [];
        var checkEmail = document.getElementById("emailBlackList") as HTMLInputElement;
        var checkBlackList = document.getElementById("rfcBlackList") as HTMLInputElement;

        if (checkEmail.checked) {
          var optionSelectedEmail = document.getElementById("motiveEmail") as HTMLInputElement;
          var valueOptionSelected = optionSelectedEmail.value;
          var commentEmail = document.getElementById("commentEmail") as HTMLInputElement;
          var valueCommentEmail = commentEmail.value;

          if (valueOptionSelected == "0" || valueCommentEmail == "") {
            this.showAlertBlackList = true;
            this.spinner.hide();
            return;
          } else {
            this.showAlertBlackList = false;
          }
          const stringResult = { type: 'ListaNegra', subType: `${"EMAIL"}`, value: `${checkEmail.value}`, cause: `${valueOptionSelected}`, descCause: `${valueCommentEmail}` };
          requestToSend.push(stringResult);
        }

        if (checkBlackList.checked) {
          var valueBlackList = (checkBlackList.value).toUpperCase();
          var optionSelectedRFC = document.getElementById("motiveRFC") as HTMLInputElement;
          var valueOptionSelected = optionSelectedRFC.value;
          var commentRFC = document.getElementById("commentRFC") as HTMLInputElement;
          var valueCommentEmail = commentRFC.value;

          if (valueOptionSelected == "0" || valueCommentEmail == "") {
            this.showAlertBlackListRFC = true;
            this.spinner.hide();
            return;
          } else {
            this.showAlertBlackListRFC = false;
          }
          const stringResult = { type: 'ListaNegra', subType: `${"RFC"}`, value: `${valueBlackList}`, cause: `${valueOptionSelected}`, descCause: `${valueCommentEmail}` };
          requestToSend.push(stringResult);
        }

        if (requestToSend.length !== 0) {
          const respuestaServicio = await this.middle.createValueBlack(requestToSend);
          if (respuestaServicio === 'OK') {
            this.saveProcedure(data, id);
          } else {
            this.messageAlert("error", "Los valores no se pudieron actualizar, intentelo de nuevo");
          }
        } else {
          this.saveProcedure(data, id);
        }
      }
        break;
      default: {
        break;
      }
    }
  }

  async saveProcedure(data, id) {
    await this.middleProcedure.updateProcedure(data, id).then(response => {
      if (response['message'] === "Ok") {
        this.messageAlert("success", "Se ha actualizado con éxito");
      } else {
        this.messageAlert("error", "Ocurrio un error, intentelo de nuevo");
      }

    }, function (error) {
      console.log("Error del servicio update trámite:", error);
    });
    this.updateItemsResultOfSessionStorage(id, data.status)
    this.ngOnInit();
  }

  messageAlert(icon: string, title: string) {
    Swal.fire({
      icon: icon,
      title: title,
    });
    this.spinner.hide();
  }

  checkCurrentStatus(currentStatus) {
    switch (currentStatus) {
      case "PASS": {
        this.PASSVALUE = false;
        this.REJECTEDVALUE = true;
        break;
      }
      case "REJECTED": {
        this.PASSVALUE = true;
        this.REJECTEDVALUE = false;
        break;
      }
      case "REVIEW":
      case "PENDING": {
        this.PASSVALUE = true;
        this.REJECTEDVALUE = true;
        break;
      }
      default: {
        break;
      }
    }

  }

  updateItemsResultOfSessionStorage(id, estatus) {
    const search = parseJSON(sessionStorage.getItem("resultSearch"));
    search.map(function (item) {
      if (item.trackId === id) {
        item.globalScore.finalResult = estatus;
        sessionStorage.removeItem("resultSearch");
        sessionStorage.setItem("resultSearch", JSON.stringify(search));
        return item;
      }
    }).indexOf(id);
  }

  async retrieEvaluation() {
    this.spinner.show();
    let response = await this.middleMongo.retrieEvaluation(this.idTrack);
    if (response === '') {
      this.messageAlert("success", "Ejecutando evaluación");
    } else {
      this.messageAlert("error", "Ocurrio un error, intentelo de nuevo");
    }
  }

  async changeColor(id: string) {
    setTimeout(function () {
      const tabs = (document.getElementById("tabGroup") as HTMLInputElement).childNodes[0].childNodes[1].childNodes[0].childNodes[0].childNodes;
      for (var i = 0; i < tabs.length; i++) {
        const tabSelected = (tabs[i] as HTMLInputElement);
        console.log("tabs", tabSelected.innerText);
        if (tabSelected.innerText === id) {
          tabSelected.style.background = "red";
        }
      }
    },
      1000);
  }

  downloadPDF() {
    var byteCharacters = atob(this.linkSource);
    var byteNumbers = new Array(byteCharacters.length);
    for (var i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    var byteArray = new Uint8Array(byteNumbers);
    var file = new Blob([byteArray], { type: 'application/pdf;base64' });
    var fileURL = URL.createObjectURL(file);
    window.open(fileURL);

    // const downloadLink = document.createElement("a");
    // const fileName = "contrato.pdf";

    // downloadLink.href = this.linkSource;
    // downloadLink.download = fileName;
    // downloadLink.click();

  }

  downloadPDFConservation() {
    var byteCharacters = atob(this.urlConservation);
    var byteNumbers = new Array(byteCharacters.length);
    for (var i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    var byteArray = new Uint8Array(byteNumbers);
    var file = new Blob([byteArray], { type: 'application/pdf;base64' });
    var fileURL = URL.createObjectURL(file);
    window.open(fileURL);

    // const downloadLink = document.createElement("a");
    // const fileName = "contrato.pdf";

    // downloadLink.href = this.linkSource;
    // downloadLink.download = fileName;
    // downloadLink.click();

  }

  async getMessageByCode(array, code) {
    console.log("el codigo es", code);
    let razon = array.find(el => el.codigo == code);
    return razon;

  }

  seeDocument(data){
    console.log("voy a abrir el archivo", data);
    
    let file = data.location; 
  
    if(file && file.includes('data:application/pdf') ){
      let blob = null;
        const blobURL = URL.createObjectURL( this.pdfBlobConversion(file, 'application/pdf'));
        const theWindow = window.open(blobURL);
        const theDoc = theWindow.document;
        const theScript = document.createElement('script');
        function injectThis() {
            window.print();
        }
        theScript.innerHTML = 'window.onload = ${injectThis.toString()};';
        theDoc.body.appendChild(theScript);
    }else{
      var byteCharacters = atob(file);
      var byteNumbers = new Array(byteCharacters.length);
      for (var i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      var filePdf = new Blob([byteArray], { type: 'application/pdf;base64' });
      var fileURL = URL.createObjectURL(filePdf);
      window.open(fileURL);
    }
    
  }

  //converts base64 to blob type for windows
  pdfBlobConversion(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;
    b64Data = b64Data.replace(/^[^,]+,/, '');
    b64Data = b64Data.replace(/\s/g, '');
    var byteCharacters = window.atob(b64Data);
    var byteArrays = [];

    for ( var offset = 0; offset < byteCharacters.length; offset = offset + sliceSize ) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }


}

