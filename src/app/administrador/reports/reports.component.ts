import { Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerInputEvent } from '@angular/material';
import { MiddleMongoService } from '../../services/http/middle-mongo.service';
import { CognitoService } from '../../services/aws/cognito.service';
import { compareDates } from 'app/model/UtilValidator';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})

export class ReportsComponent implements OnInit {
  page = 1;
  pageSize = 2;
  collectionSize = 3;
  pageOfItems: Array<any>;
  //@ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  //MARK: buttons
  showButtonReport: boolean;
  showEndDateInput: boolean;
  showSearchInput: boolean;

  //usuarios: any;
  selected: string;
  fechaInicial: Date;
  fechaFinalMax: Date;
  fechaFinal: Date;
  result: any;
  singleUser: any;
  userObject:any;
  infoUser: any;
  informacion:string
  maxFechaInicial: Date;

  //MARK: variables de propiedades de usuarios 
  correo: string;
  estatus: string;
  score: string;
  blacklist: string; 
  fingerPrint: string;
  secureTouch: string; 
  clabe: string;
  rfc: string;
  biometricos: string; 

  //MARK: Save previewData
  typeSearch: string;
  startDate: string;
  endDate: string;
  inputCorreo: string;
  inputTrackID: string;
  showCorreo: boolean;
  showTrackId: boolean; 
  
  userRole; 

  

  constructor(private middleMongo: MiddleMongoService, public router: Router, 
    private spinner: NgxSpinnerService, private cognito: CognitoService) { 

  }

  async ngOnInit() {
    await this.spinner.show();

    if(sessionStorage.getItem('exportButton')){
      this.showButtonReport = true; 
    }
    this.userRole = this.cognito.getRole();

    this.maxFechaInicial = new Date();
    //this.selected = sessionStorage.getItem('tipoDeBusqueda');
    if(sessionStorage.getItem('tipoBusqueda')){
      this.typeSearch = sessionStorage.getItem('tipoBusqueda');
      switch (this.typeSearch) {
        case 'Correo':
          this.select('correo');
          this.selected = 'correo';
          this.inputCorreo = sessionStorage.getItem('correoSearch');
          console.log("correo de busqueda", this.inputCorreo);
          this.result = JSON.parse(sessionStorage.getItem('resultSearch'));
          //this.buscarPorTexto(this.inputCorreo);
            break;
        case 'Track Id':
          this.select('trackId');
          this.selected = 'trackId';
          this.inputTrackID = sessionStorage.getItem('trackIDSearch');
          $('#inputSearch').val(this.inputTrackID);
          this.result = JSON.parse(sessionStorage.getItem('resultSearch'));
          //this.buscarPorTexto(this.inputTrackID);
            break;
        case 'Fecha':
          this.select('fecha');
          this.selected = 'fecha';
          this.startDate = sessionStorage.getItem('date1Search');
          this.endDate = sessionStorage.getItem('date2Search');
          this.result = JSON.parse(sessionStorage.getItem('resultSearch'));
          break;
        default:
    
    }

    this.createTable(this.result);
    

    }else{
 
     
    }
    

    await this.spinner.hide();
  }

  offsetIzquierda(numero: Number) {
    let nuevo = '' + numero;
    if (nuevo.length === 1) {
      nuevo = '0' + nuevo;
    }
    return nuevo;
  }

  generaFechaToSend(date: Date) {
    return `${date.getFullYear()}-${this.offsetIzquierda(date.getMonth() + 1)}-${this.offsetIzquierda(date.getDate())}`;
  }

  // compareDates() {
  //   let currentDate = new Date();
  //   return Math.floor((Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()) - Date.UTC(this.fechaInicial.getFullYear(), this.fechaInicial.getMonth(), this.fechaInicial.getDate()) ) /(1000 * 60 * 60 * 24));
  // }
  async buscarPorFecha(type: string, event: MatDatepickerInputEvent<Date>) {
    if (type === 'fechaInicial') {
      this.fechaInicial = event.value;
      let diasMax = compareDates(this.fechaInicial, 90);

      this.fechaFinalMax = new Date(this.fechaInicial.getFullYear(),
        this.fechaInicial.getMonth(), this.fechaInicial.getDate() + diasMax);
        this.showEndDateInput = true;

      // this.fechaFinalMax.setDate(this.fechaFinalMax.getDate() + 2);
      console.log(this.fechaFinalMax);
    } else {
      this.spinner.show();
      console.log('si cambie de fecha');
      this.fechaFinal = event.value;
      var data; 

      //Envío de parámetro para filtrar resultados
      if(this.userRole !== "AdministradorMit"){
        var shop = sessionStorage.getItem('codigo_comercio'); 
          data = {
          date1: `${this.generaFechaToSend(this.fechaInicial)}`,
          date2: `${this.generaFechaToSend(this.fechaFinal)}`,
          idCliente: shop
        };

      }else{
          data = {
          date1: `${this.generaFechaToSend(this.fechaInicial)}`,
          date2: `${this.generaFechaToSend(this.fechaFinal)}`
        };
      }

      //const data = {
      //  date1: `${this.generaFechaToSend(this.fechaInicial)}`,
      //  date2: `${this.generaFechaToSend(this.fechaFinal)}`
      //};
      console.log('lo que voy a maandar es: ', data);
      this.result = await this.middleMongo.getReportResult(data);
      sessionStorage.setItem('resultSearch', JSON.stringify(this.result));
      if(this.result.length > 0) {
        this.showButtonReport = true;
        var table = $('#dtBasicExample').DataTable();
        table.clear();
        table.destroy();
      
        //crear tabla
        this.createTable(this.result);

      }
      
      this.spinner.hide();
      console.log(this.result);
    }
  }

  async buscarPorTexto() {
    sessionStorage.removeItem('exportButton');
    var informacionInput = $('#inputSearch').val().toString();
    this.informacion=informacionInput;
    this.spinner.show();
    let data = {};
    if (!informacionInput || informacionInput === '') {
      this.spinner.hide();
      return;
    }
    if (this.selected === 'correo') {
      console.log("data de correo");
      if(this.userRole !== "AdministradorMit"){
        var shop = sessionStorage.getItem('codigo_comercio'); 
        data = {
          correo: informacionInput,
          idCliente: shop
        };

      }else{
        data = {
          correo: informacionInput
        };
      };

      //data = {
      //  correo: informacionInput
      //};
    } else if (this.selected === 'trackId') {
      console.log("data de trackId");
      if(this.userRole !== "AdministradorMit"){
        var shop = sessionStorage.getItem('codigo_comercio'); 
        data = {
          trackId: informacionInput,
          idCliente: shop
        };

      }else{
        data = {
          trackId: informacionInput,
        };
      };
     // data = {
     //   trackId: informacionInput
     // };
    }
    console.log('voy a buscar : ', data);
    await this.middleMongo.getReportResult(data).then(response => {
     // $('#dtBasicExample').DataTable().ajax.reload();
     this.result = response;
  },function(error){
      console.log(error);
  });
    sessionStorage.setItem('resultSearch', JSON.stringify(this.result));
    if(this.result.length > 0) {
      this.showButtonReport = true;
      var table = $('#dtBasicExample').DataTable();
      table.clear();
      table.destroy();
      
      //crear tabla
      this.createTable(this.result);
 
    } 
    
    this.spinner.hide();
    console.log("RESULTADO=",this.result);
    
  }

  async showUser(trackId: string){
    this.spinner.show();
    var data = {};
    console.log("consulta: ", trackId);
    data = {
      trackId: trackId
    };
    this.singleUser = await this.middleMongo.getReportResult(data);
    var userP = this.singleUser[0];

    console.log("userP",userP);
    //asignar valores
    this.correo = userP.correo;
    this.score = userP.globalScore.finalResult;

    if(userP.globalScore.blackList){
      this.blacklist = userP.globalScore.blackList.result;
    }else{
      this.blacklist = "No disponible";
    }

    if(userP.globalScore.fingerPrintJS){
      this.fingerPrint = userP.globalScore.fingerPrintJS.result
    }else{
      this.fingerPrint = "No disponible";
    }

    if(userP.globalScore.securedTouch){
      this.secureTouch = userP.globalScore.securedTouch.result;
    }else{
      this.secureTouch = "No disponible";
    }

    if(userP.globalScore.clabe){
      this.clabe = userP.globalScore.clabe.result;
    }else{
      this.clabe = "No disponible";
    }

    if(userP.globalScore.rfc){
      this.rfc = userP.globalScore.rfc.result;
    }else{
      this.rfc = "No disponible";
    }

    if(userP.globalScore.biometrics){
      this.biometricos = userP.globalScore.biometrics.result;
    }else{
      this.biometricos = "No disponible";
    }
    if(userP.estatus){
      this.estatus = userP.estatus
    }else{
      this.estatus = "No disponible";
    }

    this.userObject = userP;
    var cardUser = document.getElementById('resultUser');
    cardUser.style.display='block';
    this.spinner.hide();
    console.log("resultado de busqueda:", this.userObject);
  }


async getInfoUser(trackID:string){
  this.spinner.show();
  var dataSourcePanel = document.getElementById('dataSourcePanel');
  dataSourcePanel.style.display='none';
  
  var data = {
    trackId: trackID
  };
  this.infoUser = await this.middleMongo.getDataOCR(data);
  console.log(this.infoUser);

  var dataSourcePanel = document.getElementById('dataInfoPanel');
  dataSourcePanel.style.display='block';
  this.spinner.hide();
}

async getDataSource(){
  this.spinner.show();
  var dataSourcePanel = document.getElementById('dataInfoPanel');
  dataSourcePanel.style.display='none';
  var dataSourcePanel = document.getElementById('dataSourcePanel');
  dataSourcePanel.style.display='block';
  this.spinner.hide();
}

downloadFile(data, filename='data') {
  console.log("downloadFile",data);
  let headerList = ['date' ,'nombreCliente','nombreOferta','estatus', 'smartSkip' ,'trackId','correo', 'globalScore' , ];
  let tagList    = ['Fecha','Empresa'      ,'Oferta'      ,'Estatus', 'Modo', 'TrackId','Correo', 'Global score','Regla general','Resultado regla gen'  , 'Regla set', 'Resultado regla set'];
  let csvData = this.ConvertToCSV(data,headerList,tagList );
  console.log("csvData",csvData)
  let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
  let dwldLink = document.createElement("a");
  let url = URL.createObjectURL(blob);
  let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
  if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
  }
  dwldLink.setAttribute("href", url);
  dwldLink.setAttribute("download", filename + ".csv");
  dwldLink.style.visibility = "hidden";
  document.body.appendChild(dwldLink);
  dwldLink.click();
  document.body.removeChild(dwldLink);
}

downloadFileSimple(data, filename='data') {
  console.log("downloadFile",data);
  let headerList = ['date' ,'nombreCliente','nombreOferta','estatus', 'smartSkip' ,'trackId','correo', 'globalScore' , ];
  let tagList    = ['Fecha','Empresa'      ,'Oferta'      ,'Estatus', 'Modo', 'TrackId','Correo', 'Global score',];
  let csvData = this.ConvertToCSVSimple(data,headerList,tagList );
  console.log("csvData",csvData)
  let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
  let dwldLink = document.createElement("a");
  let url = URL.createObjectURL(blob);
  let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
  if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
  }
  dwldLink.setAttribute("href", url);
  dwldLink.setAttribute("download", filename + ".csv");
  dwldLink.style.visibility = "hidden";
  document.body.appendChild(dwldLink);
  dwldLink.click();
  document.body.removeChild(dwldLink);
}

ConvertToCSV(objArray,headerList,tagList) {
  let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
  let str = '';
  let row = '\r\n';
  if (this.selected === 'correo') {
    row += ',Filtro Correo:,' + this.informacion + '\r\n' + '\r\n'; 
  } else if (this.selected === 'trackId') {
    row += ',Filtro TrackId:,' + this.informacion + '\r\n' + '\r\n'; 
  }else if (this.selected === 'fecha') {
    row += ',Filtro Fecha:,De,' + this.fechaInicial + ',A:,'+this.fechaFinal + '\r\n' + '\r\n'; 
  }
  row += 'S.No,';
  for (let index in tagList) {
      row += tagList[index] + ',';
  }
  row = row.slice(0, -1);
  str += row + '\r\n';
//  console.log("lent de objetos = "+array.length)
  for (let i = 0; i < array.length; i++) {
       let line = (i+1)+'';
       let lineaBase;
       let lineaBase2;
       for (let index in headerList) {
          let head = headerList[index];
          if(head==="globalScore"){
            let globalScoreArray = array[i][head];
            // console.log('el globalScoreArray: ' , globalScoreArray);
            line += ','+globalScoreArray.finalResult+',';
//            console.log('la linea aqui es: ' , line);
            lineaBase=line;
            let contadorGS = Object.keys(globalScoreArray).length-1;
            for (var x in globalScoreArray) {
              let obj = globalScoreArray[x];
              if(x === 'ficoScore' && !obj.result) {
                if(obj.ruleSet) {
                  let result = obj.ruleSet[0] ? obj.ruleSet[0].result : undefined;
                  obj.result = result ? result : 'REJECTED';
                }
                else {
                  console.log("Obj.result",x, "  :  " ,  obj);
                  obj.result = 'REJECTED';
                }
              }
              if(obj && obj.result){
                line +=   x +','+ obj.result  ; 
                lineaBase2 = x +','+ obj.result;
                if(obj.ruleSet){
                  for (let q = 0; q < obj.ruleSet.length; q++) {
                    console.log('regla: ' , obj.ruleSet[q]);
                    line +=  ','+ (obj.ruleSet[q].message === 'SUCCESS' ? '' : obj.ruleSet[q].message) +","+ (obj.ruleSet[q].message !== 'SUCCESS'? obj.ruleSet[q].result: '') + '\r\n' + lineaBase  + lineaBase2 ;
                    if((q+1)===obj.ruleSet.length){ 
                      if(contadorGS===0){ 
                        line +=  ','+ obj.ruleSet[q].name +","+ obj.ruleSet[q].result ;
                      }else{ 
                        line +=  ','+ obj.ruleSet[q].name +","+ obj.ruleSet[q].result + '\r\n' + lineaBase
                      }
                    }
                      else{

                    }
                  }
                }
              }
              contadorGS = contadorGS-1;
            }
          }
          else{
//            console.log('voy a llenar: ' , array[i], head, array[i][head]);
            // if(head === 'smartSkip') {
            //   console.log('si entre al smart');
            //   line += ',';
            //   if(array[i]['smartSkip'] && (array[i]['smartSkip'].selfie || array[i]['smartSkip'].documento || array[i]['smartSkip'].pruebaDeVida) ) {
            //     console.log('si entre a este if');
            //     line += 'SKIP';
            //   }else {
            //     line += '';
            //   }
            // } else {
              line += ',' + (array[i][head] ? array[i][head].replaceAll(',' , '') : array[i][head]);
            // }
          } 
       }
       str += line + '\r\n';
   }
   return str;
}

ConvertToCSVSimple(objArray,headerList,tagList) {
  let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
  let str = '';
  let row = '\r\n';
  if (this.selected === 'correo') {
    row += ',Filtro Correo:,' + this.informacion + '\r\n' + '\r\n'; 
  } else if (this.selected === 'trackId') {
    row += ',Filtro TrackId:,' + this.informacion + '\r\n' + '\r\n'; 
  }else if (this.selected === 'fecha') {
    row += ',Filtro Fecha:,De,' + this.fechaInicial + ',A:,'+this.fechaFinal + '\r\n' + '\r\n'; 
  }
  row += 'S.No,';
  for (let index in tagList) {
      row += tagList[index] + ',';
  }
  row = row.slice(0, -1);
  str += row + '\r\n';
  console.log("lent de objetos = "+array.length)
  for (let i = 0; i < array.length; i++) {
       let line = (i+1)+'';
       let lineaBase;
       let lineaBase2;
       for (let index in headerList) {
          let head = headerList[index];
          if(head==="globalScore"){
            let globalScoreArray = array[i][head];
            console.log('el globalScoreArray: ' , globalScoreArray);
            line += ','+globalScoreArray.finalResult+',';
            console.log('la linea aqui es: ' , line);
            lineaBase=line;
            let contadorGS = Object.keys(globalScoreArray).length-1;
            // for (var x in globalScoreArray) {
            //   let obj = globalScoreArray[x];
            //   console.log("Obj.result", obj);
            //   if(obj && obj.result){
            //     line +=   x +','+ obj.result  ; 
            //     lineaBase2 = x +','+ obj.result;
            //     if(obj.ruleSet){
            //       for (let q = 0; q < obj.ruleSet.length; q++) {
            //         if((q+1)===obj.ruleSet.length){ 
            //           if(contadorGS===0){ 
            //             line +=  ','+ obj.ruleSet[q].name +","+ obj.ruleSet[q].result ;}else{ 
            //             line +=  ','+ obj.ruleSet[q].name +","+ obj.ruleSet[q].result + '\r\n' + lineaBase}
            //           }else{
            //           line +=  ','+ obj.ruleSet[q].name +","+ obj.ruleSet[q].result + '\r\n' + lineaBase  + lineaBase2 ;
            //         }
            //       }
            //     }
            //   }
            //   contadorGS = contadorGS-1;
            // }
          }else{
            console.log('voy a llenar: ' , array[i], head, array[i][head]);
            if(head === 'smartSkip') {
              console.log('si entre al smart');
              line += ',';
              if(array[i]['smartSkip'] && (array[i]['smartSkip'].selfie || array[i]['smartSkip'].documento || array[i]['smartSkip'].pruebaDeVida) ) {
                console.log('si entre a este if');
                line += 'SKIP';
              }else {
                line += '';
              }
            } else {
              line += ',' + (array[i][head] ? array[i][head].replaceAll(',' , '') : array[i][head]);
            }
          } 
       }
       str += line + '\r\n';
   }
   return str;
}


async download(){
  this.downloadFile(this.singleUser, 'reporte');
}

async downloadReport(){
  console.log("RRRRRR= ", this.result );
  this.downloadFile(this.result, 'reporte');
}

async downloadReportSimplify(){
  console.log("RRRRRR= ", this.result );
  this.downloadFileSimple(this.result, 'reporte');
}


viewDetail(trackID: string) {
  console.log("click en: ", trackID);
  sessionStorage.setItem('exportButton', 'true');
  this.typeSearch = $('mat-select').text();
  sessionStorage.setItem('tipoBusqueda', this.typeSearch);
  sessionStorage.setItem('tipoDeBusqueda', this.selected );
  switch (this.typeSearch) {
    case 'Correo':
      var correoInput = String($('#inputSearch').val());
      sessionStorage.setItem('correoSearch', correoInput);
        break;
    case 'Track Id':
      var trackIDInput = String($('#inputSearch').val());
      sessionStorage.setItem('trackIDSearch', trackIDInput);
        break;
    case 'Fecha':
      var date1 = String($('#date1Search').val());
      var date2 = String($('#date2Search').val());
      sessionStorage.setItem('date1Search', date1);
      sessionStorage.setItem('date2Search', date2);
      break;
    default:

}
  this.router.navigate(['/dashboard/reports/detail' + `/${trackID}`]);

}

select(type){
  if(type == 'correo' || type == 'trackId'){
    this.showSearchInput = true;
    this.showEndDateInput = false;
    $("#inputSearch").val("");

    
  }else{
    this.showSearchInput = false;
  }
}

clearSessionSearch(){
  sessionStorage.removeItem('tipoDeBusqueda');
  sessionStorage.removeItem('tipoBusqueda');
  sessionStorage.removeItem('correoSearch');
  sessionStorage.removeItem('trackIDSearch');
  sessionStorage.removeItem('date1Search');
  sessionStorage.removeItem('date2Search');
}

createTable(response){
  $.each(response, function (i, item) {
    var index = parseInt(i.toString()) + 1;
    let trValue = '<tr>';
    let modoValue = '';
      if(item.smartSkip && (item.smartSkip['selfie']  || item.smartSkip['documento']  || item.smartSkip['pruebaDeVida']) ) {
      console.log("pintar row de color");
      trValue = '<tr style="background:orange">';
      modoValue = 'SKIP';
    }
      $(trValue).append(
        $('<td>').text(index.toString()),
        $('<td>').text(item.trackId),
        $('<td>').text(item.correo),
        $('<td>').text(item.nombreCliente),
        $('<td>').text(item.nombreOferta),
        $('<td>').text(item.estatus),
        $('<td>').text(modoValue),
        $('<td>').text(item.date),
        $('<td>').text(item.globalScore.finalResult),
        $('<td>').append('<button id="'+item.trackId+'" class="btn btn-primary mr-4 mt-3 ButtonViewDetail"  mat-raised-button color="primary" data-toggle="tooltip" data-placement="top" title="Ver usuario"><i id="'+item.trackId+'" class="fa fa-eye" aria-hidden="true"></i>Ver Detalle</button>')).appendTo('#dtBasicExample tbody');
});

$('#dtBasicExample').DataTable({
  "pagingType": "full_numbers", // "simple" option for 'Previous' and 'Next' buttons only
  "language": {
    "lengthMenu": "Mostrar _MENU_ resultados por página",
    "zeroRecords": "No hay resultados",
    "info": "Resultados: _START_ de _END_ de _TOTAL_ de trámites",
    "infoEmpty": "No hay resultados disponibles",
    "search":         "Buscar:",
    "infoFiltered": "(filtered from _MAX_ total records)",
    "paginate": {
      "first":      "Primero",
      "last":       "Ultimo",
      "next":       "Siguiente",
      "previous":   "Anterior"
  },
  },
  "drawCallback": () => {
    $( ".ButtonViewDetail" ).click((e) => {
      var id = e.target.id
      console.log("EL id seleccionado es", id);
      this.viewDetail(id);
    });
  },

});

$('.dataTables_length').addClass('bs-select');

}


}

