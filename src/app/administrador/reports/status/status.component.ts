import { Router } from '@angular/router';
import { NgModule, Component, OnInit } from '@angular/core';
// import * as Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleConfService } from 'app/services/http/middle-conf.service';
import { MiddleMongoService } from '../../../services/http/middle-mongo.service';
import { CognitoService } from '../../../services/aws/cognito.service';
import { Integer } from 'aws-sdk/clients/apigateway';
import { MatDatepickerInputEvent } from '@angular/material';
import {ExcelService} from '../../../services/other/excel.service';
import {TableService} from '../../../services/other/table.service';
import { MiddleReportsService } from './../../../services/http/middle-reports_service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Console } from 'console';
import { compareDates } from 'app/model/UtilValidator';

const Swal = require('sweetalert2');

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})

export class StatusComponent implements OnInit {

clients: any;
showErrorDates: boolean;
users: any;
fechaInicial;
fechaInicialToSend;
inputReadonly: boolean;
fechaFinal;
userRole;
showAllShops: boolean;

fechaFinalMax: Date;
maxFechaInicial: Date;

//DATA por export
usersExport;
registerDataExport;
exportDataRegisters;

//ID Table

idTable = "#dtResultReportStatus";
minFechaInicial: Date;
  
  constructor(private middle: MiddleConfService, private router: Router, private spinner: NgxSpinnerService,
    private middleMongo: MiddleMongoService, private excelService:ExcelService, private middleReport: MiddleReportsService,
    private tableService:TableService, private cognito: CognitoService) { }

  async ngOnInit() {
    this.spinner.show();
    this.userRole = this.cognito.getRole();
    console.log("El userRole es", this.userRole)
    this.maxFechaInicial = new Date();
    if(this.userRole !== "AdministradorMit"){
      this.showAllShops = false; 
    }else{
      this.showAllShops = true; 
    }
    this.getShops();
  }

  async getShops(){
    this.clients = await this.middleMongo.getCustomers();
    this.spinner.hide();
  }

  async generateReport(){
    this.spinner.show();
    this.showErrorDates = false;
    var table = $("#dtResultReportStatus").DataTable();
    table.clear();
    table.destroy();
    
    console.log("las fechas son", this.fechaInicial,  this.fechaFinal);
    if(!this.fechaInicial || !this.fechaFinal) {
        this.showErrorDates = true;
        this.spinner.hide();
        return;
    }

    var inputShop = document.getElementById("selectShop") as HTMLInputElement
    var shop = inputShop.value;

    var inputTypeReport = document.getElementById("selectTypeReport") as HTMLInputElement
    var typeReport = inputTypeReport.value;

    if(shop == "0"){
      if(this.userRole !== "AdministradorMit"){
        shop = sessionStorage.getItem('nombre_comercio'); 
      }else{
        shop = " " 
      }
      
    }

    if(typeReport == "value1"){
      typeReport = " "
    }

    const query = { dateInit: this.generaFechaToSend(this.fechaInicial), dateEnd: this.generaFechaToSend(this.fechaFinal), cliente: shop, status: typeReport };
    console.log("La query a buscar es:", query);

    let responseService = await this.middleReport.createReportOfStatus(query);
    console.log("La respuesta del servicio es:", responseService);
    if(responseService['error']) {
      Swal.fire({
        icon: 'error',
        title: 'Ocurrio un error al recuperar la información',
      });
    } else  {
      this.users = responseService;
      this.tableService.createTableStatus(this.idTable, this.users)
    }

   this.spinner.hide();

  }


  exportAsXLSX(type: string){
    this.spinner.show();
    this.exportDataRegisters = [];
    for(const value in this.users){
      var dataResponse = this.users[value];
      const nodoDataTable = { trackid: `${dataResponse.trackId}`, correo: `${dataResponse.correo}`, cliente: `${dataResponse.cliente}`, oferta: `${dataResponse.oferta}`, Fecha_de_tramite: `${dataResponse.creacionDate}`, Fecha_de_cambio: `${dataResponse.dateCambiada}`, Estatus_anterior: `${dataResponse.previousStatus}`, Estatus_actual: `${dataResponse.currentStatus}`, motivo: `${dataResponse.cause}`, autorizo: `${dataResponse.authorizingPerson}`, usuario_logueado: `${dataResponse.idUserCognito}` }; 
      this.exportDataRegisters.push(nodoDataTable);
    }

    
    console.log("El tipo para exportar es:", type);
    console.log("Los registros a exportar son:", this.exportDataRegisters);
    this.excelService.exportAsExcelFile(this.exportDataRegisters, 'Reporte de estatus');
    this.spinner.hide();

    }


  async buscarPorFecha(type: string, event: MatDatepickerInputEvent<Date>) {
    if (type === 'fechaInicial') {
      this.fechaInicial = event.value;
      let diasMax = compareDates(this.fechaInicial, 90);
      this.fechaFinalMax = new Date(this.fechaInicial.getFullYear(),
      this.fechaInicial.getMonth(), this.fechaInicial.getDate() + diasMax);
      console.log(this.fechaFinalMax);
    } else {
      this.spinner.show();
      console.log('si cambie de fecha');
      this.fechaFinal = event.value;
      
      this.spinner.hide();
    }
  }

  generaFechaToSend(date: Date) {
    return `${date.getFullYear()}-${this.offsetIzquierda(date.getMonth() + 1)}-${this.offsetIzquierda(date.getDate())}`;
  }

  offsetIzquierda(numero: Number) {
    let nuevo = '' + numero;
    if (nuevo.length === 1) {
      nuevo = '0' + nuevo;
    }
    return nuevo;
  }

}