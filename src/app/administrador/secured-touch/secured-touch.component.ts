import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// import * as Swal from 'sweetalert2'


import { MiddleMongoService } from 'app/services/http/middle-mongo.service';
import { NgxSpinnerService } from 'ngx-spinner';

const Swal = require('sweetalert2');

@Component({
  selector: 'app-secured-touch',
  templateUrl: './secured-touch.component.html',
  styleUrls: ['./secured-touch.component.css']
})
export class SecuredTouchComponent implements OnInit {

  filtersLoaded: Promise<boolean>;
  isActiveValue = false;
  progreso = 0;

  constructor(private middle: MiddleMongoService, private router: Router, private spinner: NgxSpinnerService) { }

  async ngOnInit() {
    sessionStorage.removeItem('tipoDeBusqueda');
    sessionStorage.removeItem('resultSearch');
    sessionStorage.removeItem('exportButton');
    sessionStorage.removeItem('tipoBusqueda');
    sessionStorage.removeItem('correoSearch');
    sessionStorage.removeItem('trackIDSearch');
    sessionStorage.removeItem('date1Search');
    sessionStorage.removeItem('date2Search');
    this.spinner.show();
    await this.getDBValues();
    this.filtersLoaded = Promise.resolve(true);
    this.spinner.hide();
  }

  activaswal() {
    Swal.fire('Any fool can use a computer');

  }
  async getDBValues () {
    const objectReturn = await this.middle.getSTValues();
    if (objectReturn['error']) {
      console.log(objectReturn);
      this.router.navigate(['/dashboard']);
      this.spinner.hide();
    } else {
      this.isActiveValue = objectReturn['status'];
      this.progreso = objectReturn['valor'];
    }
  }

  toogleCheckbox(event: any) {
    console.log('check = ' , event.target.name, event.target.value, event.target.checked);
    this.isActiveValue = event.target.checked;
  }

  async guardar() {
    this.spinner.show();
    console.log('voy a guardar');
    const objectResponse = await this.middle.updateSTValues({
      value: this.progreso,
      active: this.isActiveValue
    });
    if (objectResponse === 'OK') {
      Swal.fire({
        icon: 'success',
        title: 'Valores Actualizados',
        // text: 'Something went wrong!',
      });
      this.spinner.hide();
      // this.router.navigate(['/dashboard']);
    } else {
      console.log(objectResponse);
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Ocurrio un error',
        text: JSON.stringify(objectResponse),
      });
    }

  }

  onChangeValue(newValue: number) {
    const elementHTML: any = document.getElementsByName('progreso')[0];
    if (newValue > 1000) {
      this.progreso = 1000;
    } else if (newValue < 0) {
      this.progreso = 0;
    } else {
      this.progreso = newValue;
    }
    elementHTML.value = Number(this.progreso);
  }

  cambiaValor(valor: number) {
    this.progreso += valor;
    if (this.progreso > 1000) {
      this.progreso = 1000;
    } else if (this.progreso < 0) {
      this.progreso = 0;
    }
    console.log(this.progreso);
  }

}
