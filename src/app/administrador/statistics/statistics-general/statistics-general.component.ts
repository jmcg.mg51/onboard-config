import { Router } from '@angular/router';
import { NgModule, Component, OnInit } from '@angular/core';
// import * as Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleConfService } from 'app/services/http/middle-conf.service';
import { MiddleMongoService } from '../../../services/http/middle-mongo.service';
import { Integer } from 'aws-sdk/clients/apigateway';
import { MatDatepickerInputEvent } from '@angular/material';
import {ExcelService} from '../../../services/other/excel.service';
import {TableService} from '../../../services/other/table.service';
import { MiddleReportsService } from './../../../services/http/middle-reports_service';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule }from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CognitoService } from '../../../services/aws/cognito.service';
import { Console } from 'console';
import { compareDates } from 'app/model/UtilValidator';


const Swal = require('sweetalert2');


@Component({
  selector: 'app-statistics-general',
  templateUrl: './statistics-general.component.html',
  styleUrls: ['./statistics-general.component.css']
})



export class StatisticsGeneralComponent implements OnInit {

  clients: any;
  cllent: any;
  users: any;
  exportData: any;
  fechaInicial;
  fechaFinal;
  fechaFinalMax: Date;
  inputReadonly: boolean;
  showErrorDates: boolean;


  banks: any;
  ages: any;
  places: any;
  indentities: any;
  domains: any;
  auths: any;

  //tables IDs
  idTableGeneral = "#dtResultReport";
  idTableBank = "#dtResultBank";
  idTableAge = "#dtResultAge";
  idTablePlace = "#dtResultPlace";
  idTableIdentity = "#dtResultIdentity";
  idTableDomain = "#dtResultDomain";
  idTableAuth = "#dtResultAuth";

  //DATA FOR GRAPHICS
  bankData: any;
  placeData: any;
  ageData: any;
  sexData: any;
  identityData: any;
  domainData: any;
  authData: any;


  //DATA por export
  banksExport: any;
  agesExport: any;
  placesExport: any;
  identitiesExport: any;
  domainsExport: any;
  authsExport: any; 
  userRole; 
  showAllShops: boolean;
  minFechaInicial: Date;
  maxFechaInicial: Date;
  
  constructor(private middle: MiddleConfService, private router: Router, private spinner: NgxSpinnerService,
    private middleMongo: MiddleMongoService, private excelService:ExcelService, private middleReport: MiddleReportsService,
    private tableService:TableService, private browserModule: BrowserModule, private browserAnimation: BrowserAnimationsModule, 
    private ngxChart: NgxChartsModule, private cognito: CognitoService) { }


  async ngOnInit() {
    this.spinner.show();
    this.userRole = this.cognito.getRole();
    console.log("El userRole es", this.userRole)
    this.maxFechaInicial = new Date();
    if(this.userRole !== "AdministradorMit"){
      this.showAllShops = false; 
    }else{
      this.showAllShops = true; 
    }
    this.getShops();
    this.initValues();
    
  }

  async buscarPorFecha(type: string, event: MatDatepickerInputEvent<Date>) {
    if (type === 'fechaInicial') {
      this.fechaInicial = event.value;
      let diasMax = compareDates(this.fechaInicial, 90);
      this.fechaFinalMax = new Date(this.fechaInicial.getFullYear(),
      this.fechaInicial.getMonth(), this.fechaInicial.getDate() + diasMax);
      console.log(this.fechaFinalMax);
    } else {
      this.spinner.show();
      console.log('si cambie de fecha');
      this.fechaFinal = event.value;
      
      this.spinner.hide();
    }
  }

  async getShops(){
    this.clients = await this.middleMongo.getCustomers();
    this.spinner.hide();
  }


  async generateReport(){
    this.spinner.show();
    this.initValues();
    // var day = this.fechaInicial.getDay();
    // var month = this.fechaInicial.getMonth();
    // var year = this.fechaInicial.getFullYear();
    // var date =  year + "-" + month +  "-" + day //this.fechaInicial;

    // var dateFinal = this.getDateEnd();
    
    // if(date === "" || date === "NaN-aN-0NaN" || date === "NaN-aN-NaN" ){ 
    //   date = " ";
    //   dateFinal = " ";
    // }else{
    //   if(dateFinal === "" || dateFinal === "NaN-aN-0NaN" || dateFinal === "NaN-aN-NaN"){
    //     dateFinal = " ";
    //     this.showErrorDates = true;
    //     this.spinner.hide();
    //     return; 
    //   }else{
    //     this.showErrorDates = false;
    //   }
    // }

    console.log("las fechas son", this.fechaInicial,  this.fechaFinal);
    if(!this.fechaInicial || !this.fechaFinal) {
        this.showErrorDates = true;
        this.spinner.hide();
        return;
    }

    var inputShop = document.getElementById("selectShop") as HTMLInputElement
    var shop = inputShop.value;

    var inputTypeReport = document.getElementById("selectTypeReport") as HTMLInputElement
    var typeReport = inputTypeReport.value;

    if(shop == "0"){
      if(this.userRole !== "AdministradorMit"){
        shop = sessionStorage.getItem('nombre_comercio'); 
      }else{
        shop = " " 
      }
      
    }

    if(typeReport == "value1"){
      typeReport = " "
    }

    const query = { dateInit: this.generaFechaToSend(this.fechaInicial), dateEnd: this.generaFechaToSend(this.fechaFinal), cliente: shop, resultado: typeReport };
    console.log("La query a buscar es:", query);

    var typeReport = "reporteFilesUser";
    this.users = await this.middleReport.createReportOfFiles(typeReport, query);
    console.log('el resultado de los reportes es: ' , this.users);
    this.banks = this.users["bancos"];
    this.ages = this.users["edades"];
    this.places = this.users["plazas"];
    this.indentities = this.users["tipoIdentificaciones"];
    this.domains = this.users["dominios"];
    this.auths = this.users["tipoAutorizaciones"];

    const total = this.users["totalRegistros"];
    var totalFraude; 
    if(this.users["totalFraude"]){
      totalFraude = this.users["totalFraude"];
    }else{
      totalFraude = 0; 
    }
    

    this.tableService.createTableGeneral(this.idTableGeneral, this.users["users"])
    this.createArrayBank(this.banks, this.idTableBank, total, totalFraude);
    this.createArrayBank(this.ages, this.idTableAge, total, totalFraude);
    this.createArrayBank(this.places, this.idTablePlace, total, totalFraude);
    this.createArrayBank(this.indentities, this.idTableIdentity, total, totalFraude);
    this.createArrayBank(this.domains, this.idTableDomain, total, totalFraude);
    this.createArrayBank(this.auths, this.idTableAuth, total, totalFraude);

   this.spinner.hide();

  }

  generaFechaToSend(date: Date) {
    return `${date.getFullYear()}-${this.offsetIzquierda(date.getMonth() + 1)}-${this.offsetIzquierda(date.getDate())}`;
  }

  offsetIzquierda(numero: Number) {
    let nuevo = '' + numero;
    if (nuevo.length === 1) {
      nuevo = '0' + nuevo;
    }
    return nuevo;
  }


  exportAsXLSX(type: string){
    console.log("El tipo para exportar es:", type);
    switch (type) {
      case "general":
        this.excelService.exportAsExcelFile(this.users["users"], 'Usuarios');
        break;
      case "bancos":
        this.excelService.exportAsExcelFile(this.banksExport, 'bancos');
        break;
      case "edades":
        this.excelService.exportAsExcelFile(this.agesExport, 'edades');
        break;
      case "plazas":
        this.excelService.exportAsExcelFile(this.placesExport, 'lugares');
        break;
      case "identificacion":
        this.excelService.exportAsExcelFile(this.identitiesExport, 'identificaciones');
        break;
      case "dominio":
        this.excelService.exportAsExcelFile(this.domainsExport, 'dominios');
        break;
      case "autorizacion":
        this.excelService.exportAsExcelFile(this.authsExport, 'autorizacion');
        break;
    }

  }

  initValues(){
    // this.inputReadonly = true;
    this.bankData = [];
  this.placeData = [];
  this.ageData = [];
  this.sexData= [];
  this.identityData = [];
  this.domainData = [];
  this.authData = [];
  }



  getDateStart(event: MatDatepickerInputEvent<Date>){
    // var date = $('#date1Search').val();
    var date = event.value;
    var formatDate = new Date(date.toString());
    var day = formatDate.getDate().toString();
    if(day.toString().length < 2){
      day = "0" + day;
    }
    var dateFinal = formatDate.getFullYear()+"-"+("0"+(formatDate.getMonth()+1)).slice(-2)+"-"+ day;
    this.fechaInicial = new Date(dateFinal);
    this.inputReadonly = false;
    this.minFechaInicial = event.value;
    let diasMax = compareDates(this.fechaInicial, 90);
    this.fechaFinalMax = new Date(this.fechaInicial.getFullYear(),
    this.fechaInicial.getMonth(), this.fechaInicial.getDate() + diasMax);
    return this.fechaInicial;
  }

  getDateEnd(){
    var date = $('#date2Search').val();
    var formatDate = new Date(date.toString());
    var day; 
    if(formatDate.getDate().toString().length > 1){
      day = formatDate.getDate();
    }else{
      day = "0" + formatDate.getDate();
    }
    var dateFinal = formatDate.getFullYear()+"-"+("0"+(formatDate.getMonth()+1)).slice(-2)+"-"+ day;
    this.fechaFinal = dateFinal;
    return this.fechaFinal;
  }



createArrayBank(banksArreglo:any, table:string, total, totalFraude){
  const dataArray = [];
  const dataGraphicArray = [];
    var percetangeFraude;
    var percentajeTotal; 
    var countFraud;
    for (let banco in banksArreglo){
      if(banksArreglo[banco].Fraude){
        countFraud = parseInt(banksArreglo[banco].Fraude.count); 
      }else{
        countFraud = 0; 
      }
      console.log('totalFraude: ' , totalFraude);
      if(totalFraude !== 0 ){
        percetangeFraude = ((parseInt(countFraud) / parseInt(totalFraude))*100).toFixed(2) ; 
        if(total !== 0){
          percentajeTotal = ((parseInt(countFraud) / parseInt(total))*100).toFixed(2);
        }else{
          percentajeTotal = 0;
        }
      }else{
        percetangeFraude = 0;
        percentajeTotal = 0;
      }
      //percetangeFraude = ((parseInt(countFraud) / parseInt(totalFraude))*100).toFixed(2) ; 
      //percentajeTotal = ((parseInt(countFraud) / parseInt(total))*100).toFixed(2);

      const banksDataTable = { name: `${banco}`, total: `${countFraud}`, percentageFraude: `${percetangeFraude}`, percentageTotal: `${percentajeTotal}` }; 
      dataArray.push(banksDataTable);
      const banksDataGraphic = { name: `${banco}`, value: countFraud};
      console.log("los valores para crear la gráfica son:", banksDataGraphic);
      dataGraphicArray.push(banksDataGraphic);
    }
    switch (table) {
      case "#dtResultBank":
        this.bankData = dataGraphicArray; 
        this.banksExport = dataArray;
        break;
      case "#dtResultAge":
        this.ageData = dataGraphicArray; 
        this.agesExport = dataArray;
        break;
      case "#dtResultPlace":
        this.placeData = dataGraphicArray; 
        this.placesExport = dataArray;
        break;
      case "#dtResultIdentity":
        this.identityData = dataGraphicArray; 
        this.identitiesExport = dataArray;
        break;
      case "#dtResultDomain":
        this.domainData = dataGraphicArray; 
        this.domainsExport = dataArray;
        break;
      case "#dtResultAuth":
        console.log("------Entro en el switch de AUTH ------");
        this.authData = dataGraphicArray; 
        this.authsExport = dataArray;
        break;
    }
  this.tableService.createTableVariable(table, dataArray);

 }


}


