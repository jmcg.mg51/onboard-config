import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// import * as Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleConfService } from 'app/services/http/middle-conf.service';
import { MiddleMongoService } from './../../../services/http/middle-mongo.service';
import { Integer } from 'aws-sdk/clients/apigateway';
import { MatDatepickerInputEvent } from '@angular/material';
import {ExcelService} from './../../../services/other/excel.service';
import { MiddleReportsService } from './../../../services/http/middle-reports_service';
import { CognitoService } from '../../../services/aws/cognito.service';
import { TableService } from '../../../services/other/table.service';
import { Console } from 'console';
import { compareDates } from 'app/model/UtilValidator';

const Swal = require('sweetalert2');

@Component({
  selector: 'app-statistics-fraud',
  templateUrl: './statistics-fraud.component.html',
  styleUrls: ['./statistics-fraud.component.css']
})
export class StatisticsFraudComponent implements OnInit {


 
  showInvalidFormat: boolean;
  messageInvalidFormat = "El archivo no es valido, intenté con un formato CSV, XLS";
  valuesMonths = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
                    "Septiembre", "Octubre", "Noviembre", "Diciembre"];
  clients: any;
  cllent: any;
  offers: any;
  users: any;
  exportData: any;
  totalUsers;
  fraudulentos; 
  vencidos;
  corriente;
  transaccionado;
  carteraVencida;
  importeFraude;
  percentageFraud;
  percentageCorriente;
  percentageVencido;
  fechaInicial;
  fechaFinal;
  fechaFinalMax: Date;
  inputReadonly: boolean;
  showErrorDates: boolean;
  amountFraude;
  amountVencido;
  amountCorriente;
  showAllShops: boolean;
  userRole;
  maxFechaInicial: Date;
  minFechaInicial: Date;

  constructor(private middle: MiddleConfService, private router: Router, private spinner: NgxSpinnerService,
    private middleMongo: MiddleMongoService, private excelService:ExcelService,
    private middleReport: MiddleReportsService, private cognito: CognitoService, private table: TableService) { }

  async ngOnInit() {
    this.spinner.show();
    this.userRole = this.cognito.getRole();
    console.log("El userRole es", this.userRole)

    if(this.userRole !== "AdministradorMit"){
      this.showAllShops = false; 
    }else{
      this.showAllShops = true; 
    }

    this.getShops();

    this.loadJs();
    this.initValues();
    this.spinner.hide();
  }




  async onOptionsSelected(value: string){
    console.log("El cliente que buscaré es:", value);

    //this.offers
    
  }

  onOptionsSelectedOffer(){

  }

  async getShops(){
    this.clients = await this.middleMongo.getCustomers();
    console.log("El listado de comercios es", this.clients);
  }


  async generateReport(){
    this.spinner.show();
   this.initValues();
   var day = this.fechaInicial.getDay();
   var month = this.fechaInicial.getMonth();
   var year = this.fechaInicial.getFullYear();
   var date =  year + "-" + month +  "-" + day
    // var date = this.getDateStart();
   // var date = this.fechaInicial;
    var dateFinal = this.getDateEnd();
    if(date === "" || date === "NaN-aN-0NaN" || date === "NaN-aN-NaN" ){ 
      date = " "
      //dateFinal = " "
      this.showErrorDates = true;
      this.spinner.hide();
      return; 
    }else{
      if(dateFinal === "" || dateFinal === "NaN-aN-0NaN" || dateFinal === "NaN-aN-NaN"){
        dateFinal = " "
        this.showErrorDates = true;
        this.spinner.hide();
        return; 
      }else{
        this.showErrorDates = false;
      }
    }

    var inputShop = document.getElementById("selectShop") as HTMLInputElement
    var shop = inputShop.value;

    var inputTypeReport = document.getElementById("selectTypeReport") as HTMLInputElement
    var typeReport = inputTypeReport.value;


    if(shop == "0"){
      if(this.userRole !== "AdministradorMit"){
        shop = sessionStorage.getItem('nombre_comercio'); 
      }else{
        shop = " " 
      }
      
    }

    if(typeReport == "value1"){
      typeReport = " "
    }

    

    const query = {
      dateInit: date,
      dateEnd: dateFinal,
      cliente: shop,
      resultado: typeReport

    }

    console.log("La query a buscar es:", query);

    var typeReport = "reporteFiles"
    this.users = await this.middleReport.createReportOfFiles(typeReport, query);
    console.log("respuesta del servicio",this.users);
    this.totalUsers = this.users.length;

    for(var i = 0; i<this.users.length; i++){
      var usuario = this.users[i];
      var statusUser = usuario["resultado"];
      console.log("-el usuario", statusUser);
      switch (statusUser) {
        case "Fraude":
          console.log("El estatus es Fraude:", status);
          this.fraudulentos = this.fraudulentos + 1; 
          break;
        case "Corriente":
          console.log("El estatus es Corriente:", status);
          this.corriente = this.corriente + 1; 
          break;
        case "Vencido":
          console.log("El estatus es Vencido:", status);
          this.vencidos = this.vencidos + 1; 
          
          break;
      }

      
    }

    this.getPercentage(this.fraudulentos, this.vencidos, this.corriente, this.totalUsers);
    this.getAmount(this.users);
    //Crear tabla
    this.table.createTableStatisticsFraud("#dtResultReport", this.users);

     //this.createTable(this.users);

     this.spinner.hide();
  }

  exportAsXLSX(){
    console.log("Los usuarios a exportar son:", this.users);
    this.excelService.exportAsExcelFile(this.users, 'sample');
  }

  initValues(){
    // this.maxFechaInicial = new Date();
    this.totalUsers = 0;
    this.fraudulentos = 0;
    this.vencidos = 0;
    this.corriente = 0;
    this.transaccionado = 0;
    this.carteraVencida = 0;
    this.importeFraude = 0;
    this.percentageFraud = 0;
    this.percentageCorriente = 0;
    this.percentageVencido = 0;
    // this.inputReadonly = true; 
    // this.fechaInicial = "";
    this.amountCorriente = 0;
    this.amountFraude = 0;
    this.amountVencido = 0;
  }


  getPercentage(fraudulentos: Integer, vencidos:Integer, corriente:Integer, total:Integer ){
    if(((fraudulentos / total) * 100).toFixed(2) === "NaN" ){
      this.percentageFraud = 0;
    }else{
      this.percentageFraud = ((fraudulentos / total) * 100).toFixed(2);
    }

    if(((corriente / total) * 100).toFixed(2) === "NaN" ){
      this.percentageCorriente = 0;
    }else{
      this.percentageCorriente = ((corriente / total) * 100).toFixed(2);
    }

    if(((vencidos / total) * 100).toFixed(2) === "NaN" ){
      this.percentageVencido = 0;
    }else{
      this.percentageVencido = ((vencidos / total) * 100).toFixed(2);
    }

  } 

  getAmount(users: any){
    for(var i = 0; i<users.length; i++){
      var usuario = this.users[i];
      console.log("El usuario con los files es:",usuario['files'][0]);
      this.amountCorriente = this.amountCorriente + parseFloat(usuario['files'][0].importeTransaccional);
      this.amountFraude = this.amountFraude + parseFloat(usuario['files'][0].importeQuebranto);
      this.amountVencido = this.amountVencido + parseFloat(usuario['files'][0].importeVencido);
      
    }

    const formatterPeso = new Intl.NumberFormat('es-MX', {
      style: 'currency',
      currency: 'MXN',
      minimumFractionDigits: 0
    })

    this.amountCorriente = formatterPeso.format(this.amountCorriente);
    this.amountFraude = formatterPeso.format(this.amountFraude);
    this.amountVencido = formatterPeso.format(this.amountVencido);

    console.log("Los montos totales son:", this.amountVencido, this.amountFraude, this.amountCorriente)

  }

  getDateStart(event: MatDatepickerInputEvent<Date>){
    // var date = $('#date1Search').val();
    var date = event.value;
    var formatDate = new Date(date.toString());
    var day = formatDate.getDate().toString();
    if(day.toString().length < 2){
      day = "0" + day;
    }
    var dateFinal = formatDate.getFullYear()+"-"+("0"+(formatDate.getMonth()+1)).slice(-2)+"-"+ day;
    this.fechaInicial = new Date(dateFinal);
    this.inputReadonly = false;
    this.minFechaInicial = event.value;
    let diasMax = compareDates(this.fechaInicial, 90);
    this.fechaFinalMax = new Date(this.fechaInicial.getFullYear(),
      this.fechaInicial.getMonth(), this.fechaInicial.getDate() + diasMax);
    return this.fechaInicial;
  }

  getDateEnd(){
    var date = $('#date2Search').val();
    var formatDate = new Date(date.toString());
    var day; 
    if(formatDate.getDate().toString().length > 1){
      day = formatDate.getDate();
    }else{
      day = "0" + formatDate.getDate();
    }
    var dateFinal = formatDate.getFullYear()+"-"+("0"+(formatDate.getMonth()+1)).slice(-2)+"-"+ day;
    this.fechaFinal = dateFinal;
    return this.fechaFinal;
  }

  public loadJs() {
    const body = <HTMLDivElement>document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/js/exportFile.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);

  }

 




    
 }


