import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// import * as Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleConfService } from 'app/services/http/middle-conf.service';

const Swal = require('sweetalert2');

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.css']
})
export class RulesComponent implements OnInit {

  filtersLoaded: Promise<boolean>;
  statusValue: boolean;
 

  constructor(private middle: MiddleConfService, private router: Router, private spinner: NgxSpinnerService) { }

  async ngOnInit() {
    this.spinner.show();
    sessionStorage.removeItem('tipoDeBusqueda');
    sessionStorage.removeItem('resultSearch');
    sessionStorage.removeItem('exportButton');
    sessionStorage.removeItem('tipoBusqueda');
    sessionStorage.removeItem('correoSearch');
    sessionStorage.removeItem('trackIDSearch');
    sessionStorage.removeItem('date1Search');
    sessionStorage.removeItem('date2Search');

    await this.getStatusRules();
    //await this.getDBValues();
    //this.filtersLoaded = Promise.resolve(true);
    this.spinner.hide();
  }

  guardar(){
    this.spinner.show();
      var check = document.getElementById("statusRules") as HTMLInputElement;
      var statusCheck = check.checked; 

      console.log("estatus de rules", statusCheck);
      this.updateStatusRules(statusCheck);
  }

  async getStatusRules () {
    const objectReturn = await this.middle.getStatusRules().then(response => {
        console.log(response);
        if(response["status"] === true){
            var check = document.getElementById("statusRules") as HTMLInputElement
            check.checked= true;
        }

    },function(error){
        console.log(error);
    });
  }

  async updateStatusRules (statusService: boolean) {
    var data = {
        "status": statusService
    }
    const objectReturn = await this.middle.updateStatusRules(data).then(response => {
        console.log(response);
        this.spinner.hide();
        if(response === "OK"){
            Swal.fire({
                icon: "success",
                title: "Se ha actualizado con éxito",
                // text: 'Something went wrong!',
              });
        }else{
            Swal.fire({
                icon: "error",
                title: "Ocurrio un error, intentelo de nuevo",
                // text: 'Something went wrong!',
              });
        }

    },function(error){
        console.log(error);
    });
  }


}
