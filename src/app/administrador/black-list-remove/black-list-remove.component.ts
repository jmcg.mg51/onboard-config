import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CognitoService } from 'app/services/aws/cognito.service';
import { MiddleBlackService } from 'app/services/http/middle-black.service';
import { NgxSpinnerService } from 'ngx-spinner';

const Swal = require('sweetalert2');

@Component({
  selector: 'app-black-list-remove',
  templateUrl: './black-list-remove.component.html',
  styleUrls: ['./black-list-remove.component.css']
})
export class BlackListRemoveComponent implements OnInit {

  selected: string;
  informacion = '';
  CLABE_LENGTH = 18;
  CLABE_WEIGHTS = [3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7];
  _rfc_pattern_pm = '^(([A-Z�&]{3})([0-9]{2})([0][13578]|[1][02])(([0][1-9]|[12][\\d])|[3][01])([A-Z0-9]{3}))|' +
  '(([A-Z�&]{3})([0-9]{2})([0][13456789]|[1][012])(([0][1-9]|[12][\\d])|[3][0])([A-Z0-9]{3}))|' +
  '(([A-Z�&]{3})([02468][048]|[13579][26])[0][2]([0][1-9]|[12][\\d])([A-Z0-9]{3}))|' +
  '(([A-Z�&]{3})([0-9]{2})[0][2]([0][1-9]|[1][0-9]|[2][0-8])([A-Z0-9]{3}))$';
 _rfc_pattern_pf = '^(([A-Z�&]{4})([0-9]{2})([0][13578]|[1][02])(([0][1-9]|[12][\\d])|[3][01])([A-Z0-9]{3}))|' +
  '(([A-Z�&]{4})([0-9]{2})([0][13456789]|[1][012])(([0][1-9]|[12][\\d])|[3][0])([A-Z0-9]{3}))|' +
  '(([A-Z�&]{4})([02468][048]|[13579][26])[0][2]([0][1-9]|[12][\\d])([A-Z0-9]{3}))|' +
  '(([A-Z�&]{4})([0-9]{2})[0][2]([0][1-9]|[1][0-9]|[2][0-8])([A-Z0-9]{3}))$';

  constructor(public router: Router, public userService: CognitoService, private actRoute: ActivatedRoute,
              private middle: MiddleBlackService, private spinner: NgxSpinnerService) {

  }

  ngOnInit() {
  }

  onKey(value: string) {
    this.informacion = value;
  }

  async borrarValueBlackList() {
    await this.spinner.show();
    console.log(this.informacion);
    if (!this.informacion || this.informacion === '') {
      await this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Ocurrio un error',
        text: 'Debes agregar un valor',
      });
      return;
    }

    if(this.selected === 'RFC') {
      this.informacion = this.informacion.toUpperCase();
    }
    if( (this.selected === 'CLABE' && !this.validaClabe(this.informacion)) ||
        (this.selected === 'RFC' && ( !this.informacion.match(this._rfc_pattern_pf) && !this.informacion.match(this._rfc_pattern_pm)) ||
        this.selected === 'EMAIL' && !this.informacion.match('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')) ) {
          Swal.fire({
            icon: 'error',
            title: 'Error en el formato',
            text: 'El formato ingresado es incorrecto',
          });
          await this.spinner.hide();
          return;
    }
    const request = {
      type: this.selected,
      value: this.informacion
    };
    console.log(request);
    const result = await this.middle.removeValueBlack(request);
    if (result === 'NOT') {
      Swal.fire({
        icon: 'error',
        title: 'Valor no encontrado',
        text: 'Los valores no se pudieron actualizar',
      });
      await this.spinner.hide();
    } else {
      Swal.fire({
        icon: 'success',
        title: 'Valores Actualizados',
        // text: 'Something went wrong!',
      });
      await this.spinner.hide();
      this.router.navigate(['/dashboard']);
    }
  }

 validaClabe(clabe) {
    return this.isANumber(clabe) &&
      clabe.length === this.CLABE_LENGTH &&
      clabe.substring(this.CLABE_LENGTH - 1) === this.getDc(clabe);
  }
  isANumber(str) {
    return !/\D/.test(str);
  }
  getDc(clabe) {
    const clabeList = clabe.split('');
    const clabeInt = clabeList.map((i) => Number(i));
    const weighted = [];
    for (let i = 0; i < this.CLABE_LENGTH - 1; i++) {
      weighted.push(clabeInt[i] * this.CLABE_WEIGHTS[i] % 10);
    }
    const summed = weighted.reduce((curr, next) => curr + next) % 10;
    const controlDigit = (10 - summed) % 10;
    return controlDigit.toString();
  }
}
