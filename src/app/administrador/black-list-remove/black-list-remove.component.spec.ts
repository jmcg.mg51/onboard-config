import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackListRemoveComponent } from './black-list-remove.component';

describe('BlackListRemoveComponent', () => {
  let component: BlackListRemoveComponent;
  let fixture: ComponentFixture<BlackListRemoveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackListRemoveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackListRemoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
