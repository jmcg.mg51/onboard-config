import { Component, OnInit } from '@angular/core';
import { MiddleConfService } from 'app/services/http/middle-conf.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CognitoService } from 'app/services/aws/cognito.service';
import { NgxSpinnerService } from 'ngx-spinner';

const Swal = require('sweetalert2');

@Component({
  selector: 'app-h2h-config',
  templateUrl: './h2h-config.component.html',
  styleUrls: ['./h2h-config.component.css']
})
export class H2hConfigComponent implements OnInit {

  informacion: number;
  informacionReintento: number;

  constructor(public router: Router, public userService: CognitoService, private actRoute: ActivatedRoute,
    private middle: MiddleConfService, private spinner: NgxSpinnerService) {

  }

  async ngOnInit() {
    sessionStorage.removeItem('tipoDeBusqueda');
    sessionStorage.removeItem('resultSearch');
    sessionStorage.removeItem('exportButton');
    sessionStorage.removeItem('tipoBusqueda');
    sessionStorage.removeItem('correoSearch');
    sessionStorage.removeItem('trackIDSearch');
    sessionStorage.removeItem('date1Search');
    sessionStorage.removeItem('date2Search');
    await this.spinner.show();
    let result = await this.middle.getFieldH2H(false);
    console.log('resultado de h2h');
    console.log(result);
    this.informacion = result['value'];
    result = await this.middle.getFieldH2H(true);
    console.log('resultado de h2hRetries');
    console.log(result);
    this.informacionReintento = result['value'];
    await this.spinner.hide();
  }

  onKey(value: number, retries) {
    // const elementHTML: any = document.getElementsByName('progreso')[0];
    if (retries) {
      this.informacionReintento = value;
    } else {
      this.informacion = value;
    }
    // console.log('el onkey , ' , this.informacion);
  }


  async updateH2HValue(retries) {
    if(!this.informacionReintento && !this.informacionReintento) {
      Swal.fire({
        icon: "error",
        title: "Favor de ingresar un valor correcto",
        // text: 'Something went wrong!',
      });
      return;
    }
    await this.spinner.show();
    const request = {
    };
    console.log('retries = ', retries);
    if (retries) {
      request['value'] = this.informacionReintento;
      request['retries'] = true;
    } else {
      request['value'] = this.informacion;
    }
    console.log('request: ', request);
    const response = await this.middle.updateFieldH2H(request);
    if (response === "OK") {
      Swal.fire({
        icon: "success",
        title: "Se ha actualizado con éxito",
        // text: 'Something went wrong!',
      });
    } else {
      Swal.fire({
        icon: "error",
        title: "Ocurrio un error, intentelo de nuevo",
        // text: 'Something went wrong!',
      });
    }
    await this.spinner.hide();
  }

}
