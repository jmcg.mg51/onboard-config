import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { H2hConfigComponent } from './h2h-config.component';

describe('H2hConfigComponent', () => {
  let component: H2hConfigComponent;
  let fixture: ComponentFixture<H2hConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ H2hConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(H2hConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
