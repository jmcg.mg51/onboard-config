import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
// import * as Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleConfService } from 'app/services/http/middle-conf.service';
import {CognitoService} from 'app/services/aws/cognito.service';
import {MiddleReportsService} from 'app/services/http/middle-reports_service';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { TableService } from '../../services/other/table.service';


const Swal = require('sweetalert2');

@Component({
  selector: 'app-circulo-credito',
  templateUrl: './circulo-credito.component.html',
  styleUrls: ['./circulo-credito.component.css']
})
export class CirculoCreditoComponent implements OnInit {
  @Input() idCliente = '';

  filtersLoaded: Promise<boolean>;
  statusValue: boolean;
   // Elementos de entrada
   
   // MARK: Variables
   allFiles: {};
   perfil: string;
   userRole;
   showAlertPerfil: boolean;
 

  constructor(private middle: MiddleConfService, private router: Router, private spinner: NgxSpinnerService,
    private cognito: CognitoService, private reportService: MiddleReportsService, private table: TableService) { }

  async ngOnInit() {
    this.spinner.show();
    this.userRole = this.cognito.getRole();
    console.log("El userRole es", this.userRole);
    await this.getFilesOfS3();

    this.spinner.hide();

    this.filtersLoaded = Promise.resolve(true);

  }


  /**
   * Obtiene todos los clientes de un comercio en especifico
   * PARAM
   * id: identificador del comercio
   */
   async getFilesOfS3(){ 
     this.allFiles = await this.reportService.getReportFicoScore();
     console.log(this.allFiles);
    //  await this.table.createTableFiles("#dtResultReport", this.allFiles);
   // console.log("todos los usuarios", this.allUsers);

}


downloadPDF(item) {
  if(!item || !item.Value ) {
    console.log('error en el archivo: ' , item);
    return;
  }
  // console.log('e item es: ' , item);
  var byteCharacters = atob(item.Value);
  var byteNumbers = new Array(byteCharacters.length);
  for (var i = 0; i < byteCharacters.length; i++) {
    byteNumbers[i] = byteCharacters.charCodeAt(i);
  }
  var byteArray = new Uint8Array(byteNumbers);
  var file = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' });
  var fileURL = URL.createObjectURL(file);
  window.open(fileURL);
}

}