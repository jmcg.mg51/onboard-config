import { Component, OnInit, Input, ViewChildren, QueryList, ElementRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

import { MiddleOfertaMongoService } from 'app/services/http/middle-oferta-mongo.service';
import { environment } from 'environments/environment';
import { CognitoService } from '../../services/aws/cognito.service';
import { isEmpty, hexToB64Link, returnNameOfService } from 'app/model/UtilValidator';
import { clearScreenDown } from 'readline';
import { _countGroupLabelsBeforeOption } from '@angular/material';

const Swal = require('sweetalert2'); 

@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.component.html',
  styleUrls: ['./ofertas.component.css']
})
export class OfertasComponent implements OnInit {

  // Elementos de entrada
  @Input() servicios: [];
  @ViewChildren('checkboxes') checkboxes: QueryList<ElementRef>;
  @ViewChild('nombreOferta', { static: true }) nombreOferta: ElementRef;
  @ViewChild('dominioPerso', { static: true }) dominioPerso: ElementRef;
  @ViewChild('politicaEvaluacion', { static: true }) politicaEvaluacion: ElementRef;
  @Input() todasLasOfertasAgregadas = {};
  @Input() idCliente = '';
  @Input() arrayOffers = {};

  // Variables necesarias
  mapaServiciosAPintar: Map<string, Object[]>;
  nombreDeLaOferta = '';
  isCreateOfer = false;
  servicioTemp: object;
  form: FormGroup;
  link: string;
  showItem: boolean;
  valueINE: boolean;
  valuePass: boolean;
  showAlert: boolean
  colorButton: string;
  colorLabel: string;
  showLinkGenerator: boolean;
  showButtonCopy: boolean;
  // curpFlag = false;
  curpFlagPass = false;
  clabeFlag = false;
  ineFlag = false;
  direccionFlag = false;
  error: string;
  showAlerPrivacity: boolean;
  showMessageH2H = false;
  showMessageAdic = false;
  validarCurp: boolean;
  disableInputOffer: boolean;
  userRole;
  varDatosAdicionales = false;
  filtersLoaded: Promise<boolean>;
  showLeveFicoScore: boolean;
  showTerms: boolean;
  ficoLevel;
  fisLevel;
  ficoTerminos;
  textoTerminos;
  previewText; 
  textoModal;
  fico; 
  validationFicoAndSignature: boolean;
  showLevelFico: boolean;
  varArchivosAdjuntos: boolean;
  alertFicoScore: boolean;
  showErrorMIT: boolean;
  capturaManual: boolean;
  resultadoFlag: boolean;
  biometricoFlag: boolean;
  reviewPassFlag: boolean;
  rfcFlag: boolean;
  showCFDI: boolean;
  disableInputOfferPolitica: boolean;

  constructor(private formBuilder: FormBuilder, private middleOferta: MiddleOfertaMongoService,
    public router: Router, private spinner: NgxSpinnerService, private cognito: CognitoService) { }

  async ngOnInit() {
    this.spinner.show();
    this.showLeveFicoScore = false; 
    this.showErrorMIT = false;
    this.userRole = this.cognito.getRole();
    this.showAlert = false;
    this.servicioTemp = {};
    this.form = this.formBuilder.group({
      orders: new FormArray([])
    });
    if (!this.todasLasOfertasAgregadas || isEmpty(this.todasLasOfertasAgregadas['ofertas'])) {
      this.todasLasOfertasAgregadas = {};
      this.todasLasOfertasAgregadas['ofertas'] = [];
      this.isCreateOfer = true;
    }
    console.log('ofertas al pri ', this.todasLasOfertasAgregadas);
    console.log("servicios_",this.servicios);
    this.mapaServiciosAPintar = await this.serviciosToMap(this.servicios);
    console.log("this.mapaServiciosAPintar",this.mapaServiciosAPintar);

    if (this.userRole !== "AdministradorMit") {
      this.disableInputOffer = true;
      this.disableInputOfferPolitica = true;
      this.disableInputs();
    } else {
      this.disableInputOffer = false;
      this.disableInputOfferPolitica = true;
    }
    console.log('datosAdd: ', this.checkboxes);
    this.spinner.hide();
  }

   serviciosToMap(serviciosAConvertir: any[]) {
    const map = new Map();
    for (const item in serviciosAConvertir) {
      if (map.has(serviciosAConvertir[`${item}`].tipo)) {
        const arreglo = map.get(serviciosAConvertir[`${item}`].tipo);
        arreglo.push({ nombre: serviciosAConvertir[`${item}`].nombre, props: serviciosAConvertir[`${item}`].propiedades });
        map.set(serviciosAConvertir[`${item}`].tipo, arreglo);
      } else {
        console.log("item",serviciosAConvertir[item]);
        const arreglo = [{ nombre: serviciosAConvertir[`${item}`].nombre, props: serviciosAConvertir[`${item}`].propiedades }];
        map.set(serviciosAConvertir[`${item}`].tipo, arreglo);
      }
    }

    console.log("map.entries()",map.entries());
    for (const [key, value] of map.entries()) {
      console.log("key",key, value);
      console.log("value", key);
    }

    return  map  /*new Map ([...map.entries()].sort( (a: any, b: any) => {
console.log("a",a[0]);
console.log("b",b[0]);
      if(a[0] === 'Análisis Crediticio') {
        return 0;
      } 
      return 1;
    } ));;*/
  }

  bouthParameter() {
    console.log(this.servicioTemp);
    this.rfcFlag = $(`[name="Validacion Información-RFC"]`).is(':checked');
    this.clabeFlag = $(`[name="Validacion Información-Cuenta Clabe"]`).is(':checked');
    // $('[name="Validacion Información-RFC-vacio"]').prop("disabled", !this.rfcFlag);
    $('[name="Validacion Información-RFC-vacio"]').prop("checked", this.rfcFlag &&
    $(`[name="Validacion Información-RFC-vacio"]`).is(':checked'));
    $('[name="Validacion Información-RFC-Fisica"]').prop("checked", this.rfcFlag &&
    $(`[name="Validacion Información-RFC-Fisica"]`).is(':checked'));
    $('[name="Validacion Información-RFC-Moral"]').prop("checked", this.rfcFlag &&
    $(`[name="Validacion Información-RFC-Moral"]`).is(':checked'));
    $('[name="Validacion Información-RFC-Sin validar"]').prop("checked", this.rfcFlag &&
    $(`[name="Validacion Información-RFC-Sin validar"]`).is(':checked'));
    $('[name="Validacion Información-Cuenta Clabe-Rechazo Score Global"]').prop("checked", this.clabeFlag &&
    $(`[name="Validacion Información-Cuenta Clabe-Rechazo Score Global"]`).is(':checked'));
    this.biometricoFlag = $(`[name="Biometricos-selfie"]`).is(':checked') ||
    $(`[name="Biometricos-documento"]`).is(':checked') ||
    $(`[name="Biometricos-Prueba De Vida"]`).is(':checked')
    || $(`[name="Biometricos-enroll"]`).is(':checked');
    this.capturaManual = $('[name="Captura Manual-Documento Manual"]').is(':checked');
    console.log('la captura manual es: ' , this.capturaManual);
    this.resultadoFlag = $('[name="Captura Manual-Resultado"]').is(':checked') && this.capturaManual;
    this.reviewPassFlag = this.resultadoFlag && ($('#Resultado-REVIEW').is(':checked') || $('#Resultado-PASS').is(':checked'));
    $('#Resultado-REVIEW').prop( "disabled", !this.resultadoFlag || $('#Resultado-PASS').is(':checked'));
    $('#Resultado-PASS').prop( "disabled", !this.resultadoFlag || $('#Resultado-REVIEW').is(':checked'));
    console.log('el resultado flag es: ' , this.resultadoFlag);
    this.varDatosAdicionales = $('[name="Valores Adicionales-Datos Adicionales"]').is(':checked');
    this.varArchivosAdjuntos = $('[name="Archivos Adjuntos-Archivos Adjuntos"]').is(':checked');
    this.showMessageAdic = this.varDatosAdicionales || this.varArchivosAdjuntos;

    this.validaCalculoPersonaFisica();
    this.showItem = $('[name="Biometricos-documento"]').is(':checked') ;
    this.showMessageH2H = $('[name="Validacion Información-Cuenta Clabe"]').is(':checked');
    this.curpFlagPass = $('#documento-PASSPORT').is(':checked') && this.showItem;
    this.ineFlag = (this.showItem && $('[name="Biometricos-documento-INE"]').is(':checked'));
    this.direccionFlag = this.showItem && (this.ineFlag || this.curpFlagPass);

    this.valueINE = $('[name="Biometricos-documento-INE"]').is(':checked');
    this.showAlert = !this.valueINE || !$('[name="Biometricos-documento-PASSPORT"]').is(':checked');
    // this.curpFlag = !$('[name="Biometricos-documento-PASSPORT"]').is(':checked');
    this.valuePass = $('[name="Biometricos-documento-PASSPORT"]').is(':checked');
    let estaCurpCheck = $('[name="Validacion Información-CURP"]').is(':checked');
    this.validarCurp = (this.valuePass && estaCurpCheck) || this.valueINE || this.capturaManual;

    this.showLeveFicoScore = $('[name="Análisis Crediticio-FICO Score"]').is(':checked');
    this.showLevelFico = $('[name="Análisis Crediticio-Reporte Crediticio"]').is(':checked');
    this.validationFicoAndSignature = $('[name="Firma Digital-NIP"]').is(':checked');

    if(this.showLeveFicoScore || this.showLevelFico){
      $('[name="Firma Digital-NIP"]').prop( "disabled", true );
      this.alertFicoScore = true; 
     // (document.getElementById("selectFicoScore") as HTMLInputElement).value = "0"
      this.setFicoScoreValue(this.ficoLevel, this.fisLevel);
    }else{
      $('[name="Firma Digital-NIP"]').prop( "disabled", false );
      this.alertFicoScore = false;
      this.showLeveFicoScore = false; 
    }

    if(this.validationFicoAndSignature){
      $('[name="Análisis Crediticio-FICO Score"]').prop( "disabled", true );
      $('[name="Análisis Crediticio-Reporte Crediticio"]').prop( "disabled", true );
      
    }else{
      $('[name="Análisis Crediticio-FICO Score"]').prop( "disabled", false );
      $('[name="Análisis Crediticio-Reporte Crediticio"]').prop( "disabled", false );
      this.validationFicoAndSignature = false; 
    }

    $('[name="Análisis Crediticio-FICO Score-Enviar Contrato"]').prop("checked", this.showLeveFicoScore &&
    $(`[name="Análisis Crediticio-FICO Score-Enviar Contrato"]`).is(':checked'));

    this.showCFDI = this.rfcFlag && !$(`[name="Validacion Información-RFC-vacio"]`).is(':checked');
    $('[name="Validacion Información-CFDI"]').prop("checked", this.showCFDI &&
    $(`[name="Validacion Información-CFDI"]`).is(':checked'));

    if(this.varDatosAdicionales === false){
      console.log("entro a deschequear los campos")
      //Valores Adicionales-Datos Adicionales-0
      $('[name="Valores Adicionales-Datos Adicionales-0"]').prop("checked", false);
      $('[name="Valores Adicionales-Datos Adicionales-1"]').prop("checked", false);
      $('[name="Valores Adicionales-Datos Adicionales-2"]').prop("checked", false);
      $('[name="Valores Adicionales-Datos Adicionales-3"]').prop("checked", false);
    }
    

  }

  validaCalculoPersonaFisica() {
    let isSelectedRfcValue = 
      $(`[name="Validacion Información-RFC-vacio"]`).is(':checked') || 
      $(`[name="Validacion Información-RFC-Fisica"]`).is(':checked') ||
      $(`[name="Validacion Información-RFC-Moral"]`).is(':checked') ;

    $('[name="Validacion Información-RFC-Calculo Persona Fisica"]').prop("checked", this.rfcFlag &&
    $(`[name="Validacion Información-RFC-Calculo Persona Fisica"]`).is(':checked') && !isSelectedRfcValue);

  }

  tempDatosAdicionales() {
    let nameAux = `[name="Valores Adicionales-Datos Adicionales-Nombre Comercial"]`;
    let aux = $(nameAux).is(':checked');
    console.log('esta chequeado: ' , aux);
    console.log('esta chequeado: ' , aux && this.varDatosAdicionales);
    $(nameAux).prop('checked', this.varDatosAdicionales && aux);
    let event = {};
    event['target'] = {};
    event['target']['name'] = nameAux;
    event['target']['checked'] = this.varDatosAdicionales && aux;
    console.log('el event aqui es: ' , event);
    this.toogleCheckboxProperties(event);

    nameAux = `[name="Valores Adicionales-Datos Adicionales-Sector"]`;
    aux = $(nameAux).is(':checked');
    console.log('esta chequeado: ' , aux);
    console.log('esta chequeado: ' , aux && this.varDatosAdicionales);
    $(nameAux).prop('checked', this.varDatosAdicionales && aux);
    event = {};
    event['target'] = {};
    event['target']['name'] = nameAux;
    event['target']['checked'] = this.varDatosAdicionales && aux;
    this.toogleCheckboxProperties(event);
  }

  varChecked() {
    console.log(this.servicioTemp);
    this.bouthParameter();
    this.tempDatosAdicionales();
    // for (let i = 3; i <= 4; i++) {
      let nameAux = "[name=\"Valores Adicionales-Datos Adicionales-Dato3\"]";
      let aux = $(nameAux).is(':checked');
      console.log('esta chequeado: ' , aux);
      console.log('esta chequeado: ' , aux && this.varDatosAdicionales);
      $(nameAux).prop('checked', this.varDatosAdicionales && aux);
      let event = {};
      event['target'] = {};
      event['target']['name'] = nameAux;
      event['target']['checked'] = this.varDatosAdicionales && aux;
      console.log('event: ' , event);
      this.toogleCheckboxProperties(event);
    // }


    $('#documento-PASSPORT').prop('checked',  this.showItem
      && $('#documento-PASSPORT').is(':checked') );
    $('#documento-INE').prop('checked', this.showItem
      && $('#documento-INE').is(':checked') );


      /**Validaciones para documento maual**/
      console.log('aqui el check del resultado es: ' , this.resultadoFlag);
      $('[name="Captura Manual-Resultado"]').prop('checked', this.resultadoFlag);

      $('[name="Captura Manual-Documento Manual-INE"]').prop('checked', 
        $('[name="Captura Manual-Documento Manual-INE"]').is(':checked') && this.capturaManual);
      $('[name="Captura Manual-Documento Manual-PASSPORT"]').prop('checked', 
        $('[name="Captura Manual-Documento Manual-PASSPORT"]').is(':checked') && this.capturaManual);

        $('#Resultado-PASS').prop('checked', 
        $('#Resultado-PASS').is(':checked') && this.resultadoFlag);
      $('#Resultado-REVIEW').prop('checked', 
        $('#Resultado-REVIEW').is(':checked') && this.resultadoFlag);
        /**Validaciones para documento maual**/

      $('[name="Validacion Información-Validar CURP"]').prop('checked', (this.showItem || this.capturaManual)
      && $('[name="Validacion Información-Validar CURP"]').is(':checked') );
    $('[name="Validacion Información-Validar INE"]').prop('checked', this.showItem
      && $('[name="Validacion Información-Validar INE"]').is(':checked') );

      $('[name="Biometricos-Enroll"]').prop('checked', (this.showItem )
      && $('[name="Biometricos-Enroll"]').is(':checked') );
  }

  toogleCheckbox(event: any) {
    console.log('datosAdd2: ', this.servicioTemp);
    const valores = event.target.name.split('-');
    this.varChecked();
    if (event.target.checked) {
      console.log('voy a ir por el nombre del ');
      this.servicioTemp[returnNameOfService(valores[1])] = {
        nombre: valores[1],
        tipo: valores[0]
      };
      console.log('el nombre es: ', this.servicioTemp);
    } else {
      delete this.servicioTemp[returnNameOfService(valores[1])];
      if(event.target.name == "Biometricos-documento") {
        delete this.servicioTemp['validarIne'];
        if (this.servicioTemp['curp']) {
          delete this.servicioTemp['curp'];
          this.validarCurp = false;
          $('#ValidacionInformación-CURP').prop('checked', false);
        }
        if (this.servicioTemp['validarCurp']) {
          delete this.servicioTemp['validarCurp'];
          this.validarCurp = false;
          $('[name="Validacion Información-Validar CURP"]').prop('checked', false);
        }
        if (this.servicioTemp['direccion']) {
          delete this.servicioTemp['direccion'];
          this.direccionFlag = false;
          $('[name="Validacion Información-Dirección"]').prop('checked', false);
        }
      }
      else if(event.target.name == "Captura Manual-Documento Manual") { 
        delete this.servicioTemp['resultado'];
      }
    }

  }

  valCheck2() {
    let aux = $('[name="Biometricos-documento-INE"]').is(':checked');
    this.bouthParameter();
  
    $('#ValidacionInformación-INEDIRECCION').prop('checked', aux && $('#ValidacionInformación-INEDIRECCION').is(':checked'));
    $('[name="Validacion Información-Validar CURP"]').prop('checked', this.validarCurp && $('[name="Validacion Información-Validar CURP"]').is(':checked'));
    $('[name="Validacion Información-Validar INE"]').prop('checked', aux && $('[name="Validacion Información-Validar INE"]').is(':checked'));
    $('[name="Validacion Información-Dirección"]').prop('checked', this.direccionFlag && $('[name="Validacion Información-Dirección"]').is(':checked'));
    $('#ValidacionInformación-CURP').prop('checked', this.valuePass && $('#ValidacionInformación-CURP').is(':checked') );
    $('#ValidacionInformación-DIRECCION').prop('checked', this.valuePass && $('#ValidacionInformación-DIRECCION').is(':checked'));

  }

  toogleCheckboxProperties(event: any) {
    const valores = event.target.name.split('-');
    const nameKey = returnNameOfService(valores[1]);
    var substring = '"]'
    if(nameKey.includes(substring)){
      console.log("mi nombre esta mal");
      nameKey.replace(substring,'')
    }
    console.log("el nuevo namekey es", nameKey);
    this.valCheck2();
    if (!this.servicioTemp[nameKey]) {
      return;
    }
    console.log(this.servicioTemp);
    if (event.target.checked) {
      if (!this.servicioTemp[nameKey]['propiedades']) {
        this.servicioTemp[nameKey]['propiedades'] = [];
      }
      if (this.servicioTemp[nameKey]['propiedades'].indexOf(valores[2]) === -1) {
        
        this.servicioTemp[nameKey]['propiedades'].push(valores[2]);
        //console.log("",this.servicioTemp[nameKey]['propiedades'].push(valores[2]));
      } else {
        console.log('ni siquiera entre al if');
      }
      console.log(this.servicioTemp);
    } else {
      if (!this.servicioTemp[nameKey]['propiedades']) {
        return;
      }
      const index = this.servicioTemp[nameKey]['propiedades'].indexOf(valores[2]);
      this.servicioTemp[nameKey]['propiedades'].splice(index, 1);

      if (event.target.name == "Biometricos-documento-PASSPORT") {
        if (this.servicioTemp['curp']) {
          delete this.servicioTemp['curp'];
        }
      }
    }

  }

  get formData() { return this.form; }

  customErrorSubmit(textError){
    this.showAlert = true;
    this.error = textError;
    this.spinner.hide();
  }

  async submit() {
    this.showAlert = false;
    this.showAlerPrivacity = false;
    this.alertFicoScore = false; 
    this.error = '';
    await this.spinner.show();


    var selectOption = document.getElementById("selectname") as HTMLInputElement;
    var option = selectOption.value;

    if (option === "noData") {
      this.customErrorSubmit('Debe seleccionar la opción Nueva oferta');
      return;
    }

    if (isEmpty(this.servicioTemp)) {
      this.customErrorSubmit('Debe seleccionar al menos un servicio');
      return;
    }

    if (this.showItem && ( !this.valueINE && !this.valuePass && !this.capturaManual) ) {
      this.customErrorSubmit('Debe seleccionar el tipo de documento');
      return;
    }

    //Validar datos adicionales
    if((document.getElementById("Valores Adicionales-Datos Adicionales") as HTMLInputElement).checked 
    && (!((document.getElementById("Datos Adicionales-Dato1") as HTMLInputElement).checked)
    && !((document.getElementById("Datos Adicionales-Dato2") as HTMLInputElement).checked )
    && !((document.getElementById("Datos Adicionales-Dato3") as HTMLInputElement).checked )
    && !((document.getElementById("Datos Adicionales-Dato4") as HTMLInputElement).checked ))){
      this.customErrorSubmit('Verifica que tienes al menos un dato adicional agregado');
      return;
    }

    if((document.getElementById("Archivos Adjuntos-Archivos Adjuntos") as HTMLInputElement).checked 
      && (!((document.getElementById("Archivos Adjuntos-Adjunto1") as HTMLInputElement).checked)
      && !((document.getElementById("Archivos Adjuntos-Adjunto2") as HTMLInputElement).checked )
      && !((document.getElementById("Archivos Adjuntos-Adjunto3") as HTMLInputElement).checked )
      && !((document.getElementById("Archivos Adjuntos-Adjunto4") as HTMLInputElement).checked ))){
        this.customErrorSubmit('Verifica que tienes al menos un archivo agregado');
        return;
      }



    //Validar FICOSCORE
    var ficoCheck = (document.getElementById("Análisis Crediticio-FICO Score") as HTMLInputElement).checked;
    var signatureNip = (document.getElementById("Firma Digital-NIP") as HTMLInputElement).checked;
    

    if(ficoCheck || signatureNip ){
      if(ficoCheck === true){
        var ficoSelect = (document.getElementById("selectFicoScore") as HTMLInputElement).value;
        var FisSelect = (document.getElementById("selectFIS") as HTMLInputElement).value;
      if(ficoSelect !== "0" && FisSelect !== "0"){
        this.ficoLevel = ficoSelect;
        this.fisLevel = FisSelect;
      }else{
        //SHOW ERROR "selecciona un nivel para ficoscore"
        this.customErrorSubmit('selecciona un nivel para ficoscore y FIS');
        return; 
      }
      }

      var myFile = $('#fileTerminos').prop('files')[0];
      if(myFile !== undefined){
        this.textoTerminos = await this.leeArchivo(myFile);
      }else if(document.getElementById("name-file").innerHTML === "terminos_y_condiciones.txt" ){
        this.textoTerminos = this.textoModal;
      }else{
        //SHOW ERROR "Agrega un archivo de términos y condiciones"
        this.customErrorSubmit('Agrega un archivo de términos y condiciones');
        return; 
      }
      
      if(ficoCheck && !this.textoTerminos.includes("Autorizo expresamente a Mercadotecnia Ideas y Tecnolog")){
        this.showErrorMIT = true;
        this.spinner.hide();
        return; 
      }else{
        this.showErrorMIT = false;
      }

    }


    // var politicaInputDOM = document.getElementById("politicaInput") as HTMLInputElement;
    // var valuePolitica = politicaInputDOM.value;

    // if (valuePolitica === "") {
    //   this.customErrorSubmit('Debe ingresar una política de evaluación');
    //   return;
    // }

    const cuantasOfertas = this.todasLasOfertasAgregadas['ofertas'].length;
    let mensajeSwal = 'Oferta Creada';

    for (let i = 0; i < cuantasOfertas; i++) {

      if (this.todasLasOfertasAgregadas['ofertas'][i].nombre === this.nombreDeLaOferta) {
        this.todasLasOfertasAgregadas['ofertas'].splice(i, 1);
        mensajeSwal = 'Oferta Actualizada';
        break;
      }
    }

    if (JSON.stringify(this.todasLasOfertasAgregadas['ofertas']).includes(`"${this.nombreOferta.nativeElement.value}"`)) {
      Swal.fire({
        icon: 'error',
        title: 'No puedes repetir nombre de la oferta'
      });
      this.uncheckAll();
      this.spinner.hide();
      return;
    }
    this.colorButton = $('#colorButton').val().toString();
    this.colorLabel = $('#colorLabel').val().toString();

    var nuevaOferta = {};
    

    if(this.servicioTemp["datosAdicionales"]){
      this.servicioTemp['datosAdicionales'].propiedades = [];
      var arrayElements = document.getElementsByName("variablesDatosAdicionales");
      console.log("el array de elementos es:", arrayElements);
      let dato; 
      let checkboxValue;
      var arrayChecks = ["Dato1", "Dato2", "Dato3", "Dato4"];
      // for(var i = 0; i < arrayElements.length; i++){
      //   dato = (arrayElements[i] as HTMLInputElement);
      //   console.log(dato.value);
      // }

      

      if((document.getElementById("Datos Adicionales-Dato1") as HTMLInputElement).checked){
        this.servicioTemp['datosAdicionales'].propiedades.push((arrayElements[0] as HTMLInputElement).value); 
      }

      if((document.getElementById("Datos Adicionales-Dato2") as HTMLInputElement).checked){
        this.servicioTemp['datosAdicionales'].propiedades.push((arrayElements[1] as HTMLInputElement).value); 
      }

      if((document.getElementById("Datos Adicionales-Dato3") as HTMLInputElement).checked){
        this.servicioTemp['datosAdicionales'].propiedades.push((arrayElements[2] as HTMLInputElement).value); 
      }

      if((document.getElementById("Datos Adicionales-Dato4") as HTMLInputElement).checked){
        this.servicioTemp['datosAdicionales'].propiedades.push((arrayElements[3] as HTMLInputElement).value); 
      }

      // var comercioInput = (document.getElementById("Datos Adicionales-Nombre Comercial") as HTMLInputElement).checked;
      // var sectorInput = (document.getElementById("Datos Adicionales-Sector") as HTMLInputElement).checked;
      // var dato3 = (document.getElementById("Datos Adicionales-Dato3") as HTMLInputElement).checked;
      // var dato4 = (document.getElementById("Datos Adicionales-Dato4") as HTMLInputElement).checked;

      // this.servicioTemp['datosAdicionales'].propiedades = [];

      // if(comercioInput){
      //   this.servicioTemp['datosAdicionales'].propiedades.push("Nombre Comercial");
      // }

      // if(sectorInput){
      //   this.servicioTemp['datosAdicionales'].propiedades.push("Sector");
      // }

      // if(dato3){
      //   this.servicioTemp['datosAdicionales'].propiedades.push("Dato3");
      // }

      // if(dato4){
      //   this.servicioTemp['datosAdicionales'].propiedades.push("Dato4");
      // }

      /*
      console.log("previo al for", this.servicioTemp['datosAdicionales'].propiedades);
      const length = this.servicioTemp['datosAdicionales'].propiedades.length
      for(var i = 0; i < length ; i ++){
        if(this.servicioTemp['datosAdicionales'].propiedades[i] === 'Nombre Comercial"]' ){
          console.log(this.servicioTemp['datosAdicionales'].propiedades[i]);
          this.servicioTemp['datosAdicionales'].propiedades.splice(i, 1); 
        }
  
        if(this.servicioTemp['datosAdicionales'].propiedades[i] === 'Sector"]'){
          console.log(this.servicioTemp['datosAdicionales'].propiedades[i]);
          this.servicioTemp['datosAdicionales'].propiedades.splice(i, 1); 
        }

        if(this.servicioTemp['datosAdicionales'].propiedades[i] === 'Dato4"]'){
          console.log(this.servicioTemp['datosAdicionales'].propiedades[i]);
          this.servicioTemp['datosAdicionales'].propiedades.splice(i, 1); 
        }

        if(this.servicioTemp['datosAdicionales'].propiedades[i] === 'Dato3"]'){
          console.log(this.servicioTemp['datosAdicionales'].propiedades[i]);
          this.servicioTemp['datosAdicionales'].propiedades.splice(i, 1); 
        }


        
      }*/

      console.log("posterior al for", this.servicioTemp['datosAdicionales'].propiedades);

      let result = this.servicioTemp['datosAdicionales'].propiedades.filter((item,index)=>{
        return this.servicioTemp['datosAdicionales'].propiedades.indexOf(item) === index;
      })
      console.log("RESULT al for eliminar duplicados", result);
      this.servicioTemp['datosAdicionales'].propiedades = result; 
      console.log("posterior al for eliminar duplicados", this.servicioTemp['datosAdicionales'].propiedades);
    }
/** INICI archivos adjuntos */
    if(this.servicioTemp["archivosAdjuntos"]){
      this.servicioTemp['archivosAdjuntos'].propiedades = [];
      var arrayElements = document.getElementsByName("variablesArchivosAdjuntos");
      console.log("el array de elementos es:", arrayElements);
      let dato; 


      if((document.getElementById("Archivos Adjuntos-Adjunto1") as HTMLInputElement).checked){
        this.servicioTemp['archivosAdjuntos'].propiedades.push((arrayElements[0] as HTMLInputElement).value); 
      }

      if((document.getElementById("Archivos Adjuntos-Adjunto2") as HTMLInputElement).checked){
        this.servicioTemp['archivosAdjuntos'].propiedades.push((arrayElements[1] as HTMLInputElement).value); 
      }

      if((document.getElementById("Archivos Adjuntos-Adjunto3") as HTMLInputElement).checked){
        this.servicioTemp['archivosAdjuntos'].propiedades.push((arrayElements[2] as HTMLInputElement).value); 
      }

      if((document.getElementById("Archivos Adjuntos-Adjunto4") as HTMLInputElement).checked){
        this.servicioTemp['archivosAdjuntos'].propiedades.push((arrayElements[3] as HTMLInputElement).value); 
      }

      console.log("posterior al for", this.servicioTemp['archivosAdjuntos'].propiedades);

      let result = this.servicioTemp['archivosAdjuntos'].propiedades.filter((item,index)=>{
        return this.servicioTemp['archivosAdjuntos'].propiedades.indexOf(item) === index;
      })
      console.log("RESULT al for eliminar duplicados", result);
      this.servicioTemp['archivosAdjuntos'].propiedades = result; 
      console.log("posterior al for eliminar duplicados", this.servicioTemp['archivosAdjuntos'].propiedades);
    }
    /** END archivos adjuntos */
    nuevaOferta["nombre"] = this.nombreOferta.nativeElement.value;
    nuevaOferta["dominioPerso"] = this.dominioPerso.nativeElement.value;
    nuevaOferta["politicaEvaluacion"] = this.politicaEvaluacion.nativeElement.value;
    nuevaOferta["servicios"] =  this.servicioTemp;
    nuevaOferta["configuration"] = {};
    nuevaOferta["configuration"]["colorButton"] = this.colorButton;
    nuevaOferta["configuration"]["colorLabel"] = this.colorLabel;


    if(this.validationFicoAndSignature){
      nuevaOferta["documentoTerminos"] = this.textoTerminos;
    }

    if(ficoCheck){
      nuevaOferta["nivelFicoScore"] = this.ficoLevel;
      nuevaOferta["nivelFIS"] = this.fisLevel;
      nuevaOferta["documentoTerminos"] = this.textoTerminos;
    }


    // if(ficoCheck){
    //   nuevaOferta = {
    //     nombre: this.nombreOferta.nativeElement.value,
    //     politicaEvaluacion: this.politicaEvaluacion.nativeElement.value,
    //     servicios: this.servicioTemp,
    //     configuration: {
    //       colorButton: this.colorButton,
    //       colorLabel: this.colorLabel
    //     },
    //     nivelFicoScore: this.ficoLevel,
    //     documentoTerminos: this.textoTerminos
  
    //   };
    // }else{
    //   nuevaOferta = {
    //     nombre: this.nombreOferta.nativeElement.value,
    //     politicaEvaluacion: this.politicaEvaluacion.nativeElement.value,
    //     servicios: this.servicioTemp,
    //     configuration: {
    //       colorButton: this.colorButton,
    //       colorLabel: this.colorLabel
    //     }
  
    //   };
    // }

    console.log("la nueva  oferta es", nuevaOferta);

    this.todasLasOfertasAgregadas['ofertas'].push(nuevaOferta);

    this.showAlert = false;
    await this.createOrUpdateOfer(this.todasLasOfertasAgregadas, mensajeSwal);
    this.servicioTemp = {};
    this.nombreDeLaOferta = '';
    this.uncheckAll();
    const tam = this.todasLasOfertasAgregadas['ofertas'].length;
    console.log('el tamaño es: ', tam);
    $('#selectname').val('' + (tam - 1)).prop('selected', true);
    this.onOptionsSelected('' + (tam - 1));
    await this.spinner.hide();
  }

  async createOrUpdateOfer(objectRequest: any, mensajeSwal: string) {
    let response: string;
    response = '';
    if (this.isCreateOfer === true) {
      response = await this.middleOferta.createOferta(this.idCliente, objectRequest);
      this.isCreateOfer = false;
    } else {
      response = await this.middleOferta.updateOferta(this.idCliente, objectRequest);
    }
    Swal.fire({
      icon: (response === 'OK' ? 'success' : 'error'),
      title: (response === 'OK' ? mensajeSwal : response)
    });
  }

  resetColorsValue() {
    $('#colorButton').val('#000000');
    $('#colorLabel').val('#FFFFFF');
    document.getElementById('previewButton').style.color = "white";
    document.getElementById('previewButtonSecun').style.color = "black";
    document.getElementById('previewButton').style.background = "black"
  }

  uncheckAll() {
    this.nombreOferta.nativeElement.value = '';
    this.dominioPerso.nativeElement.value = '';
    this.politicaEvaluacion.nativeElement.value = '';
    this.textoModal = '';
    this.checkboxes.forEach((element) => {
      element.nativeElement.checked = false;
    });
    this.resetColorsValue();
    this.ineFlag = false;
    this.validarCurp = false;
    this.showLinkGenerator = false;
    this.capturaManual = false;
    this.resultadoFlag = false;
    this.biometricoFlag = false;
    this.reviewPassFlag = false;
    // this.curpFlag = false;
    this.valuePass = false;
    this.varDatosAdicionales = false;
    this.link = '';
    this.nombreDeLaOferta = '';
    this.servicioTemp = {};
    this.showItem = false;
    var arrayItems = ["Dato 1", "Dato 2", "Dato 3", "Dato 4"];
    var arrayItemsArchivos = ["Adjunto 1", "Adjunto 2", "Adjunto 3", "Adjunto 4"];
    var idCheck;
    var idCheckArchivo;
    for(var i = 0 ; i < arrayItems.length; i++){
      idCheck = "Valores Adicionales-Datos Adicionales-"+i; 
      (document.getElementsByName(idCheck)[0] as HTMLInputElement).checked = false;
      (document.getElementsByName("variablesDatosAdicionales")[i] as HTMLInputElement).value = arrayItems[i];
     // document.getElementsByName("Valores Adicionales-Datos Adicionales-"+i) as HTMLInputElement
    }

    for(var j = 0 ; j < arrayItemsArchivos.length; j++){
      idCheckArchivo = "Archivos Adjuntos-Archivos Adjuntos-"+j; 
      (document.getElementsByName(idCheckArchivo)[0] as HTMLInputElement).checked = false;
      (document.getElementsByName("variablesArchivosAdjuntos")[j] as HTMLInputElement).value = arrayItemsArchivos[j];
     // document.getElementsByName("Valores Adicionales-Datos Adicionales-"+i) as HTMLInputElement
    }
  }

  onOptionsSelected(value: string) {
    console.log("select", value);
    if(value === "value1" || value === "noData"){
      (document.getElementById("politicaInput") as HTMLInputElement).style.display = "none";
    }else{
      (document.getElementById("politicaInput") as HTMLInputElement).style.display = "block";
    }
    this.uncheckAll();
    this.showLink();
    const servicesOfOffer = this.todasLasOfertasAgregadas['ofertas'][value];
    console.log("valor de x", servicesOfOffer);
    if (!servicesOfOffer) {
      this.showLeveFicoScore = false;
      return;
    }
    this.servicioTemp = this.todasLasOfertasAgregadas['ofertas'][value].servicios;
    this.nombreDeLaOferta = servicesOfOffer.nombre;
    this.nombreOferta.nativeElement.value = servicesOfOffer.nombre;
    this.dominioPerso.nativeElement.value = servicesOfOffer.dominioPerso ? servicesOfOffer.dominioPerso : '';
    this.politicaEvaluacion.nativeElement.value = (servicesOfOffer.politicaEvaluacion || '');

    /** Selecciona el color del button y label de la oferta seleccionada */
    if (servicesOfOffer.configuration) {
      var colorButtonService = this.todasLasOfertasAgregadas['ofertas'][value].configuration.colorButton;
      var colorLabelService = this.todasLasOfertasAgregadas['ofertas'][value].configuration.colorLabel;
      //set color preview button
      $('#colorButton').val(colorButtonService);
      $('#colorLabel').val(colorLabelService);
      document.getElementById('previewButton').style.color = colorLabelService;
      document.getElementById('previewButton').style.background = colorButtonService;
      document.getElementById('previewButtonSecun').style.color = colorLabelService;
    }

    if(servicesOfOffer.servicios['datosAdicionales'] && servicesOfOffer.servicios['datosAdicionales']['propiedades']){
      console.log("si traigo datos adicionales");
      this.varDatosAdicionales = true;
      let arrayProperties = servicesOfOffer.servicios['datosAdicionales']['propiedades']; 
      let idCheck;
      for(var i = 0 ; i < arrayProperties.length; i++){
        idCheck = "Valores Adicionales-Datos Adicionales-"+i; 
        console.log("idCheck", document.getElementsByName(idCheck) );
        (document.getElementsByName(idCheck)[0] as HTMLInputElement).checked = true;
        (document.getElementsByName("variablesDatosAdicionales")[i] as HTMLInputElement).value = arrayProperties[i];
       // document.getElementsByName("Valores Adicionales-Datos Adicionales-"+i) as HTMLInputElement
      }
    }

    if(servicesOfOffer.servicios['archivosAdjuntos'] && servicesOfOffer.servicios['archivosAdjuntos']['propiedades']){
      console.log("si traigo datos adicionales");
      let arrayPropertiesArchivos = servicesOfOffer.servicios['archivosAdjuntos']['propiedades']; 
      let idCheck;
      for(var j = 0 ; j < arrayPropertiesArchivos.length; j++){
        idCheck = "Archivos Adjuntos-Archivos Adjuntos-"+j; 
        (document.getElementsByName(idCheck)[0] as HTMLInputElement).checked = true;
        (document.getElementsByName("variablesArchivosAdjuntos")[j] as HTMLInputElement).value = arrayPropertiesArchivos[j];
       // document.getElementsByName("Valores Adicionales-Datos Adicionales-"+i) as HTMLInputElement
      }
    }

    if (servicesOfOffer.nivelFicoScore) {
      this.showLeveFicoScore = true;
      this.ficoLevel = servicesOfOffer.nivelFicoScore;
      this.textoModal = servicesOfOffer.documentoTerminos;
      this.fisLevel = servicesOfOffer.nivelFIS;
      console.log("Valores de fico", this.ficoLevel);

      if(this.textoModal != undefined || this.textoModal != ""){
        console.log("Hay archivo");
        setTimeout(function(){ 
          document.getElementById("name-file").innerHTML= "terminos_y_condiciones.txt";
        },
           200);
        
      }else{
        console.log("No hay archivo");
        document.getElementById("name-file").innerHTML= "Elige un archivo"
      }
     // (document.getElementById("selectFicoScore") as HTMLInputElement).value = servicesOfOffer.nivelFicoScore;

    }

    if(servicesOfOffer.documentoTerminos){
      this.textoModal = servicesOfOffer.documentoTerminos;
      console.log("Valores de fico", this.ficoLevel);

      if(this.textoModal != undefined || this.textoModal != ""){
        console.log("Hay archivo");
        setTimeout(function(){ 
          document.getElementById("name-file").innerHTML= "terminos_y_condiciones.txt";
        },
           200);
        
      }else{
        console.log("No hay archivo");
        document.getElementById("name-file").innerHTML= "Elige un archivo"
      }

    }
    /** Verifica propiedades de ficoScore 
    if (servicesOfOffer.nivelFicoScore) {
      this.showLeveFicoScore = true;
      const levelFico = (document.getElementById("selectFicoScore") as HTMLInputElement);
      levelFico.value = servicesOfOffer.nivelFicoScore;
    }

    if (servicesOfOffer.documentoTerminos) {

    }*/
    let nameAux2;
    for (const item in servicesOfOffer.servicios) {
      const nombreChecked = servicesOfOffer.servicios[`${item}`].tipo + '-' + servicesOfOffer.servicios[`${item}`].nombre;
      this.checkboxes.forEach((element) => {
        console.log(nombreChecked);
        if (element.nativeElement.name === nombreChecked) {
          element.nativeElement.checked = true;
          console.log('el nombre que obtengo es: ', nombreChecked);
          for (let dato in servicesOfOffer.servicios[`${item}`].propiedades) {
            let nombreDelDato = servicesOfOffer.servicios[`${item}`].propiedades[dato];
            let nameAux = `[name="${nombreChecked}-${nombreDelDato}"]`;
            
            
           
            //console.log('el nombre del dato es: ', nameAux);
           // var input = "input" + nameAux
           //console.log("input",nameAux);

            $(nameAux).prop('checked', true);
          }
          
        }
      });
    }
    this.bouthParameter();
    console.log(this.servicioTemp);
    // this.servicioTemp = this.todasLasOfertasAgregadas['ofertas'][value].servicios;

  }

  generaLink() {
    if (!this.nombreDeLaOferta || this.nombreDeLaOferta === '') {
      return;
    }
    let base64String = hexToB64Link(this.idCliente, this.nombreDeLaOferta);
    this.link = environment.baseLinkServicios + base64String;
    this.showButtonCopy = true;
  }

  

  getColorSystem() {
    this.colorButton = $('#colorButton').val().toString();
    this.colorLabel = $('#colorLabel').val().toString();
    document.getElementById('previewButton').style.color = this.colorLabel;
    document.getElementById('previewButton').style.background = this.colorButton;
    document.getElementById('previewButtonSecun').style.color = this.colorLabel;
  }

  showLink() {
    var selectedOffer = $("#selectname option:selected").text();
    if (selectedOffer === "Nueva oferta" || selectedOffer === "Seleccionar una opción") {
      this.showLinkGenerator = false;
    } else {
      this.showLinkGenerator = true;
    }
  }

  copyLink() {
    var aux = document.createElement("input");
    var value = $("#link").val().toString();
    aux.setAttribute("value", value);
    document.body.appendChild(aux);
    aux.select();
    document.execCommand("copy");
    document.body.removeChild(aux);
  }

  disableInputs() {
    $(".checkInputs :input").prop("disabled", true);
    $("#ValidacionInformación-CURP").prop("disabled", true);
    
    
  }

  asIsOrder(a, b) {
    return 0;
  }

  async processFile(file){

    var myFile = $('#fileTerminos').prop('files')[0];
    document.getElementById("name-file").innerHTML= myFile.name;
    console.log("myFile", myFile);
    let text = await myFile.text();
    if(text){
      this.previewText = text;
      console.log("el texto a previsualizar es:", this.previewText);
      this.textoModal = this.previewText;
     // this.disablePreview = false; 
    }
    //console.log("texto", text);
  }

  uploadFile(){
    console.log("cambio el input", $('.input-file'));
    $('.input-file').each(function() {
      var $input = $(this),
          $label = $input.next('.js-labelFile'),
          labelVal = $label.html();
      
     $input.on('change', function(element) {
      var valueElement  = element.target as HTMLInputElement;
      console.log("El nombre del archivo es", valueElement);
      var ext = valueElement.value.match(/\.([^\.]+)$/)[1];
        switch (ext) {
          case 'txt':
            if (valueElement.value) fileName = valueElement.value.split('\\').pop();
            fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
            break;
          default:
            alert('Not allowed');
            var fileName = '';
        } 
        
     });
    });
  }

  abrirModal(){
    //console.log(this.previewText);
    //this.showAlerPrivacity = false;
    const modal = document.getElementById('modalTerminos');
    modal.style.display = 'block';
  }

  cerrarModal(){
    const modal = document.getElementById('modalTerminos');
    modal.style.display = 'none';
  }

  async leeArchivo(file){
    let text = await file.text();
    console.log("texto", text);
    return text; 
  }

  async setFicoScoreValue(nivelFicoScore, nivelFis){
    console.log("nivelFICOSET",nivelFicoScore);
    console.log("la funcion setFico", $('[name="Análisis Crediticio-FICO Score"]').is(':checked'));
    if($('[name="Análisis Crediticio-FICO Score"]').is(':checked') && nivelFicoScore != undefined && nivelFis != undefined ){
      console.log("es valor es true");
        
        setTimeout(function(){ 
          if(document.getElementById("selectFicoScore") && nivelFicoScore != "0"){
          var levelFico = (document.getElementById("selectFicoScore") as HTMLSelectElement);
          levelFico.value = nivelFicoScore;
        }
        if(document.getElementById("selectFIS") && nivelFis != "0"){
          var levelFis = (document.getElementById("selectFIS") as HTMLSelectElement);
          levelFis.value = nivelFis;
        }
        },
           200);
        
     
      
    }else{
      setTimeout(function(){ 
        (document.getElementById("selectFIS") as HTMLInputElement).value = "0";
      (document.getElementById("selectFicoScore") as HTMLInputElement).value = "0";
      },
         200);
      
    }
  }
}
