import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
// import * as Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleConfService } from 'app/services/http/middle-conf.service';
import {CognitoService} from 'app/services/aws/cognito.service';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';

const Swal = require('sweetalert2');

@Component({
  selector: 'app-perfiles',
  templateUrl: './perfiles.component.html',
  styleUrls: ['./perfiles.component.css']
})
export class PerfilesComponent implements OnInit {
  @Input() idCliente = '';

  filtersLoaded: Promise<boolean>;
  statusValue: boolean;
   // Elementos de entrada
   
   // MARK: Variables
   allUsers: {};
   typeMessage: string;
   typeAlert: string; 
   showAlert: boolean;
   perfil: string;
   userRole;
   showAlertPerfil: boolean;

   perfiles = [{"name":"Ejecutivo MIT", "value":"EjecutivoMIT"},
   {"name":"Administrador MIT", "value":"AdministradorMit"}];
 

   constructor(private middle: MiddleConfService, private router: Router, private spinner: NgxSpinnerService,
    private cognito: CognitoService) {
     }

  async ngOnInit() {
 
    console.log('voy a crear la tablaaaaaaaa');
    this.spinner.show();
    sessionStorage.removeItem('tipoDeBusqueda');
    sessionStorage.removeItem('resultSearch');
    sessionStorage.removeItem('exportButton');
    sessionStorage.removeItem('tipoBusqueda');
    sessionStorage.removeItem('correoSearch');
    sessionStorage.removeItem('trackIDSearch');
    sessionStorage.removeItem('date1Search');
    sessionStorage.removeItem('date2Search');

    this.userRole = this.cognito.getRole();
    console.log("El userRole es", this.userRole);
     await this.getUsers();

    this.spinner.hide();


  }


  /**
   * Obtiene todos los clientes de un comercio en especifico
   * PARAM
   * id: identificador del comercio
   */
   async getUsers(){ 
    let users;
    users = await this.cognito.getAllUsers("0");
    this.allUsers = users;
    console.log("todos los usuarios", this.allUsers);
    this.cleanInputs();
    this.createTable(this.allUsers, this.userRole);

    
    //this.ngOnInit();
}

/**
 * Crea un usuario en un comercio especifico
 * PARAM
 * name: nombre del usuario que se dará de alta
 */
async addUser(name: string){ 
    this.spinner.show();

    var inputPerfil = document.getElementById("selectPerfil") as HTMLInputElement
    this.perfil = inputPerfil.value;
    console.log("usuario a crear", name ,this.perfil);
    if(name === ""){
      this.showAlert = true; 
      this.spinner.hide();
      return;
    }else if(this.perfil == "value1"){
      this.showAlert = false; 
      this.showAlertPerfil = true; 
      this.spinner.hide();
      return;
    }else{
      this.showAlertPerfil = false; 
      this.showAlert = false; 
    }

    if(name.match('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')){
      name = name.trim();
      this.showAlert = false; 
    }else{
      this.showAlert = true; 
      this.spinner.hide();
      return;
    }
    
    var nameComercio = this.perfil//localStorage.getItem("nombreComercio");
    var idComercio = "0";
    let createUser;

    console.log("Voy a crear el usuario");
    createUser = this.cognito.createUserFromAdmin(name, nameComercio, idComercio, this.perfil).then(response => {
      console.log(response);
      if(response === "An account with the given email already exists."){
        var iconM = "error";
        var titleM = "El usuario ya se encuentra registrado";
        this.swalAlertConfig("Error", iconM, titleM);
        this.spinner.hide();
        return; 
      }
      this.typeMessage = response; 
      this.typeAlert = "Add"; 
      this.cerrarModal();
      this.spinner.hide();
      this.showMessageAlert(this.typeAlert, this.typeMessage);
  },function(error){
      console.log(error);
      this.spinner.hide();
  });
  
}

/**
 * Elimina un usuario especifico de un comercio
 * PARAM
 * id: identificador del usuario
 */

async deleteUser(id:string){
  console.log("el usuario a borrar es", id);
  let deleteUser;
  deleteUser = this.cognito.deleteUserFromAdmin(id).then(response => {
      console.log(response);
      this.typeMessage = response; 
      this.typeAlert = "Delete";
      this.showMessageAlert(this.typeAlert, this.typeMessage);
      this.spinner.hide();
  },function(error){
      console.log(error);
      this.spinner.hide();
  });
 
}

/**
 * Mostrar mensajes de confirmación o error
 */

 async showMessageAlert(typeAlert:string, typeMessage: string){
  switch(typeAlert) { 
      case "Add": { 
          if(typeMessage == "OK"){
              var iconM = "success";
              var titleM = "Se ha enviado el usuario y contraseña al correo registrado";
              this.swalAlertConfig(typeMessage, iconM, titleM);
              break;
          }else{
              var iconM = "error";
              var titleM = "El usuario no se agrego, intentelo más tarde";
              this.swalAlertConfig(typeMessage, iconM, titleM);
              break;
          }  
      } 
      case "Delete": { 
          if(typeMessage == "OK"){
              var iconM = "success";
              var titleM = "El usuario se ha eliminado con éxito";
              this.swalAlertConfig(typeMessage, iconM, titleM);
              break;
          }else{
              var iconM = "error";
              var titleM = "El usuario no se ha podido eliminar, intentelo más tarde";
              this.swalAlertConfig(typeMessage, iconM, titleM);
              break;
          }
      } 
      default: { 
         //statements; 
         break; 
      } 
   }
  
 }

 async swalAlertConfig(type:string, iconM:string, titleM:string){
  if(type == "OK"){
      Swal.fire({
          icon: iconM,
          title: titleM,
          // text: 'Something went wrong!',
        });
        this.getUsers();
  }else{
      Swal.fire({
          icon: iconM,
          title: titleM,
          // text: 'Something went wrong!',
        });
  }
 }


 cleanInputs(){
  if(document.getElementById("inputEmail")){
    var inputValue = document.getElementById("inputEmail") as HTMLInputElement;
    inputValue.value = ""; 
  }

 }


 showModalAdduser(){
  const modal = document.getElementById('modalAddUser');
  modal.style.display = 'block';
 }

 async cerrarModal() {
  this.showAlert = false; 
  this.cleanInputs();
  const modal = document.getElementById('modalAddUser');
  modal.style.display = 'none';
}

disableInputUser(){
  $("#btn-add").prop("disabled", true);
}


createTable(response , role){
  console.log("el rol es:", role);
  var table = $("#dtProfiles").DataTable();
    table.clear()
    table.destroy();
    console.log("Estoy creando la tabla con", response);

  $.each(response, function (i, item) {
    console.log("el item es:", item);
    var index = parseInt(i.toString()) + 1;
    $.each(item.Attributes, function (j, attribute) {
      console.log("los atributos del item son:", attribute);

      // if(attribute.Name === "email") {
      //   $('<tr>').append('<td>'+index+' </td> <td>'+attribute.Value+' </td> <td>'
      //   + item.Attributes[0].Value  +' </td> <td>'+item.UserStatus+' </td> <td> '+ ( '<button id="'+item.Username+'"  class="btn btn-info ButtonDeleteProfile"><i class="material-icons">&#xE872;</i></button>') +' </td>').appendTo('#dtProfiles tbody')
      // }

      (attribute.Name === "email" ? $('<tr>').append('<td>'+index+' </td> <td>'+attribute.Value+' </td> <td>'+(item.Attributes ? ( item.Attributes.length > 0 ? 
        item.Attributes[0].Value : '' ) : '')+' </td> <td>'+item.UserStatus+' </td> <td> '+ ( '<button id="'
        + item.Username + '"  class="btn btn-info ButtonDeleteProfile"><i class="material-icons" id="'
        + item.Username + '"  >&#xE872;</i></button>') +' </td>').appendTo('#dtProfiles tbody') : '' )
})
});

$("#dtProfiles").DataTable({
  "pagingType": "full_numbers", // "simple" option for 'Previous' and 'Next' buttons only
  "language": {
    "lengthMenu": "Mostrar _MENU_ resultados por página",
    "zeroRecords": "No hay resultados",
    "info": "Resultados: _START_ de _END_ de _TOTAL_ de trámites",
    "infoEmpty": "No hay resultados disponibles",
    "search":         "Buscar:",
    "infoFiltered": "(filtered from _MAX_ total records)",
    "paginate": {
      "first":      "Primero",
      "last":       "Ultimo",
      "next":       "Siguiente",
      "previous":   "Anterior"
  },
  },
  "drawCallback": () => {
    $( ".ButtonDeleteProfile" ).on('click',(e) => {
      console.log("Id1: ", e['id']);
      console.log("Id2: ", e.target.id);
      var id = e.target.id;
      console.log("EL id seleccionado es", id);
      this.deleteUser(id);
    });
  },

});

//$('.dataTables_length').addClass('bs-select');

}

  

 


}