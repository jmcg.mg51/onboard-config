import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// import * as Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleConfService } from 'app/services/http/middle-conf.service';
import { MiddleReportsService } from './../../services/http/middle-reports_service';
import { MiddleMongoService } from './../../services/http/middle-mongo.service';
import { forkJoin } from 'rxjs';

const Swal = require('sweetalert2');

@Component({
  selector: 'app-fraud',
  templateUrl: './fraud.component.html',
  styleUrls: ['./fraud.component.css']
})
export class FraudComponent implements OnInit {


  clients: any;
  client;
  showInvalidFormat: boolean;
  errors: any;
  showErrors: boolean;
  showErrorInputs: boolean;
  reader;
  messageInvalidFormat = "El archivo no es valido, intenté con un formato CSV, XLS";
  valuesMonths2 = [{"name":"Enero", "value":"01"},{"name":"Febrero", "value":"02"}, {"name":"Marzo", "value":"03"},
  {"name":"Abril", "value":"04"}, {"name":"Mayo", "value":"05"}, {"name":"Junio", "value":"06"}, {"name":"Julio", "value":"07"},
  {"name":"Agosto", "value":"08"}, {"name":"Septiembre", "value":"09"}, {"name":"Octubre", "value":"10"}, {"name":"Noviembre", "value":"11"},
  {"name":"Diciembre", "value":"12"}];

  constructor(private middle: MiddleConfService, private router: Router, private spinner: NgxSpinnerService,
              private middleReport: MiddleReportsService, private middleMongo: MiddleMongoService) { }

  async ngOnInit() {
    this.spinner.show();
    this.validateMonths();
    this.getShops();
    this.uploadFile();
    this.spinner.hide();
  }

  /**
   * Get data: File and Month
   */
  getData(){
    var fileUpload = $("[type='file']")[0] as HTMLInputElement;
    var files = fileUpload.files;
    var inputMonth = document.getElementById("selectMonth") as HTMLInputElement
    var month = inputMonth.value;

    const currentYear = new Date().getFullYear();
    const lote = month + "-" + currentYear;
  
    var inputShop = document.getElementById("selectShop") as HTMLInputElement
    var shop = inputShop.value;

    if(shop == "0"){
      this.showErrorInputs = true; 
      return; 
    }else{
      this.showErrorInputs = false; 
       this.client = shop;
    }

    //const client = "PruebaTenant";

    console.log("Este es el lote", lote);
    console.log(files[0] );

    if(files[0] == undefined || month == "value1"){
      console.log("Entro al if");
      this.showErrorInputs = true; 
      return; 

    }else{
      console.log("Entro al else");
      this.showErrorInputs = false; 
      this.showErrors = false;
      this.save(files[0], lote, this.client);
    }
    //var formdata = new FormData();
    //formdata.append("file-"+month, files[0]);
    //console.log(formdata.get("file-"+month));
 
    

  }



  save(file: File, loteI: string, client: string){
    this.spinner.show();
    this.reader = new FileReader();
    this.reader.readAsDataURL(file);
    console.log(this.reader.result);
    this.reader.onload = () => {
        console.log(this.reader.result);
        const data = {
          cliente: client,
          lote: loteI,
          fileB64: this.reader.result
        }
        console.log("Lo que voy a mandar es:", data);
        this.middleReport.createReportOfUser(data).then(async (res) => {
          console.log("El resultado de subir el archivo es:", res);
          if(res['status'] == "OK!"){
            Swal.fire({
              icon: "success",
              title: "El archivo se ha subido con éxito",
            });
            
          var fileUpload = $("[type='file']")[0] as HTMLInputElement;
          fileUpload.value = "";
          this.ngOnInit();
          this.spinner.hide();
            
          }else{
          Swal.fire({
              icon: "error",
              title: "Ocurrio un error, por favor verifica el archivo",
              // text: 'Something went wrong!',
            });
            this.showErrors = true;
            this.errors = res["obj"];
            console.log("Los errores a pintar son:", this.errors);
            
            var fileUpload = $("[type='file']")[0] as HTMLInputElement;
            fileUpload.value = "";
            this.spinner.hide();
      }
        }).catch((err) => {
          console.log("El error de subir el archivo es:", err);
        });

        }
       

  }


  uploadFile(){
    console.log("cambio el input");
    $('.input-file').each(function() {
      var $input = $(this),
          $label = $input.next('.js-labelFile'),
          labelVal = $label.html();
      
     $input.on('change', function(element) {
      var valueElement  = element.target as HTMLInputElement
      var ext = valueElement.value.match(/\.([^\.]+)$/)[1];
        switch (ext) {
          case 'txt':
            if (valueElement.value) fileName = valueElement.value.split('\\').pop();
            fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
            break;
          default:
            alert('Not allowed');
            var fileName = '';
        } 
        
     });
    });
  }

  // onOptionsSelected(){
    
  // }

  validateMonths(){
    const currentMonth = new Date().getMonth();
    
    var months; 
    this.valuesMonths2 = [{"name":"Enero", "value":"01"},{"name":"Febrero", "value":"02"}, {"name":"Marzo", "value":"03"},
    {"name":"Abril", "value":"04"}, {"name":"Mayo", "value":"05"}, {"name":"Junio", "value":"06"}, {"name":"Julio", "value":"07"},
    {"name":"Agosto", "value":"08"}, {"name":"Septiembre", "value":"09"}, {"name":"Octubre", "value":"10"}, {"name":"Noviembre", "value":"11"},
    {"name":"Diciembre", "value":"12"}];
    
    switch(currentMonth) { 
      case 0: { 
        months = currentMonth
        this.valuesMonths2 = this.valuesMonths2.slice(0);
        break; 
      } 
      case 1: {
        months = currentMonth - 1;
        this.valuesMonths2 = this.valuesMonths2.slice(0);
        break;  
      }
      case 2: { 
        months = currentMonth - 2;
        this.valuesMonths2 = this.valuesMonths2.slice(0);
        break;   
      } 
      default: { 
        months = currentMonth - 3;
        this.valuesMonths2 = this.valuesMonths2.slice(months);
        break; 
      } 
   }
    
  }

  async getShops(){
    this.clients = await this.middleMongo.getCustomers();
    this.spinner.hide();
  }


}