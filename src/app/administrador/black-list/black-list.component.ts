import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CognitoService } from 'app/services/aws/cognito.service';
import { MiddleBlackService } from '../../services/http/middle-black.service';

declare var jQuery: any;
declare var $: any;
const Swal = require('sweetalert2');

@Component({
  selector: 'app-black-list',
  templateUrl: './black-list.component.html',
  styleUrls: ['./black-list.component.css']
})
export class BlackListComponent implements OnInit {
  id: string;
  filtersLoaded: Promise<boolean>;
  showError: boolean;
  error: string; 

  constructor(public router: Router, public userService: CognitoService,
    private actRoute: ActivatedRoute, private middle: MiddleBlackService,
    private spinner: NgxSpinnerService) {

  }

  async ngOnInit() {
    sessionStorage.removeItem('tipoDeBusqueda');
    sessionStorage.removeItem('resultSearch');
    sessionStorage.removeItem('exportButton');
    sessionStorage.removeItem('tipoBusqueda');
    sessionStorage.removeItem('correoSearch');
    sessionStorage.removeItem('trackIDSearch');
    sessionStorage.removeItem('date1Search');
    sessionStorage.removeItem('date2Search');
    await this.spinner.show();

    var buttonSave = document.getElementById("btn-save") as HTMLInputElement;
    buttonSave.disabled = true; 

    this.loadJs();
    this.filtersLoaded = Promise.resolve(true);
    await this.spinner.hide();

  }

  // logout() {
  //   this.userService.signOut();
  //   this.router.navigate(['RutasPublicas.login']);
  // }

  // regresar() {
  //   this.router.navigate(['/dashboard']);
  // }

  async saveBlackList() {
    console.log('btn save');
    const requestToSend = [];

    $('tr.itemBlackList').each(function () {
      const rowTable = $(this);
      const type = rowTable.find('td.type').html();
      let value = rowTable.find('td.value').html();
      let motive = rowTable.find('td.motive').html();
      let comment = rowTable.find('td.commentInput').html();

      var stringSubType = "<select";
      var stringValue = "<input";

      if(type === 'RFC') {
        
        value = value.toUpperCase();
        console.log("El valor del frc es:", value);
      }
      if(type.includes(stringSubType) || value.includes(stringValue)){
        return; 
      }else{
        this.showError= false;
        const stringResult = { type: 'ListaNegra', subType: `${type}`, value: `${value}`, cause: `${motive}`, descCause: `${comment}`   };
        requestToSend.push(stringResult);
      }
   });

    console.log(requestToSend.length);
    if (requestToSend.length === 0) {
      await this.spinner.hide();
      //this.error = "Por favor, asegurate de agregar el tipo de lista negra que quiere guardar";
      //this.showError= true;
      return;
    }else{
      var buttonSave = document.getElementById("btn-save") as HTMLInputElement;
      buttonSave.disabled = false; 
    }
    console.log("los valores a enviar", requestToSend);



    //CAmbiar el comentario para guardar listas negras
    const respuestaServicio = await this.middle.createValueBlack(requestToSend);
    if (respuestaServicio === 'OK') {
      Swal.fire({
        icon: 'success',
        title: 'Valores Agregados',
        // text: 'Something went wrong!',
      });
      this.router.navigate(['/dashboard']);
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Ocurrio un error',
        text: 'Los valores no se pudieron actualizar',
      });
    }
  }

  public loadJs() {
    const body = <HTMLDivElement>document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/js/blackList.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);

  }

}
