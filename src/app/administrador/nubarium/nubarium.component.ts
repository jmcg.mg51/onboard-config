import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// import * as Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleConfService } from 'app/services/http/middle-conf.service';

const Swal = require('sweetalert2');

@Component({
  selector: 'app-nubarium',
  templateUrl: './nubarium.component.html',
  styleUrls: ['./nubarium.component.css']
})
export class NubariumComponent implements OnInit {

  filtersLoaded: Promise<boolean>;
  statusValue: boolean;
 

  constructor(private middle: MiddleConfService, private router: Router, private spinner: NgxSpinnerService) { }

  async ngOnInit() {
    this.spinner.show();
    sessionStorage.removeItem('tipoDeBusqueda');
    sessionStorage.removeItem('resultSearch');
    sessionStorage.removeItem('exportButton');
    sessionStorage.removeItem('tipoBusqueda');
    sessionStorage.removeItem('correoSearch');
    sessionStorage.removeItem('trackIDSearch');
    sessionStorage.removeItem('date1Search');
    sessionStorage.removeItem('date2Search');

    await this.getStatusNubarium();
    //await this.getDBValues();
    //this.filtersLoaded = Promise.resolve(true);
    this.spinner.hide();
  }

  guardar(){
    this.spinner.show();
      var check = document.getElementById("statusNubarium") as HTMLInputElement;
      var statusCheck = check.checked; 

      console.log("estatus de nubarium", statusCheck);
      this.updateStatusNubarium(statusCheck);
  }

  async getStatusNubarium () {
    const objectReturn = await this.middle.getStatusNuvarium().then(response => {
        console.log(response);
        if(response["status"] === true){
            var check = document.getElementById("statusNubarium") as HTMLInputElement
            check.checked= true;
        }

    },function(error){
        console.log(error);
    });
  }

  async updateStatusNubarium (statusService: boolean) {
    var data = {
        "status": statusService
    }
    const objectReturn = await this.middle.updateStatusNubarium(data).then(response => {
        console.log(response);
        this.spinner.hide();
        if(response === "OK"){
            Swal.fire({
                icon: "success",
                title: "Se ha actualizado con éxito",
                // text: 'Something went wrong!',
              });
        }else{
            Swal.fire({
                icon: "error",
                title: "Ocurrio un error, intentelo de nuevo",
                // text: 'Something went wrong!',
              });
        }

    },function(error){
        console.log(error);
    });
  }


}