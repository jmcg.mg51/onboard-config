import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

import { MiddleBlackService } from 'app/services/http/middle-black.service';

@Component({
  selector: 'app-black-list-report',
  templateUrl: './black-list-report.component.html',
  styleUrls: ['./black-list-report.component.css']
})
export class BlackListReportComponent implements OnInit {

  displayedColumns: string[] = ['valor', 'causa', 'descripcion'];
  dataSource: any;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  filtersLoaded: Promise<boolean>;

  constructor(private middle: MiddleBlackService, private spinner: NgxSpinnerService) { }

  async ngOnInit() {
    sessionStorage.removeItem('tipoDeBusqueda');
    sessionStorage.removeItem('resultSearch');
    sessionStorage.removeItem('exportButton');
    sessionStorage.removeItem('tipoBusqueda');
    sessionStorage.removeItem('correoSearch');
    sessionStorage.removeItem('trackIDSearch');
    sessionStorage.removeItem('date1Search');
    sessionStorage.removeItem('date2Search');

    const result = await this.middle.getBlackListReport();
    console.log('el resultado es: ' , result);
    this.dataSource = new MatTableDataSource<any>(result);
    this.dataSource.paginator = this.paginator;
    this.filtersLoaded = Promise.resolve(true);
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
