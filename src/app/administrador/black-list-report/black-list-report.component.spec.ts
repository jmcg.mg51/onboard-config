import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackListReportComponent } from './black-list-report.component';

describe('BlackListReportComponent', () => {
  let component: BlackListReportComponent;
  let fixture: ComponentFixture<BlackListReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackListReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackListReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
