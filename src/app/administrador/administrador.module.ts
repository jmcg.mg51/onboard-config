import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';

import { CreateClienteComponent } from './create-cliente/create-cliente.component';
import { AdministradorRoutes } from './administrador.routing';
import { from } from 'rxjs';
import { OfertasComponent } from './ofertas/ofertas.component';
import { SecuredTouchComponent } from './secured-touch/secured-touch.component';
import { ReportsComponent } from './reports/reports.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material';
import { MatTable, MatTableModule, MatPaginatorModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { BlackListComponent } from './black-list/black-list.component';
import { BlackListRemoveComponent } from './black-list-remove/black-list-remove.component';
import { BlackListReportComponent } from './black-list-report/black-list-report.component';

import { DetailComponent } from './reports/detail/detail.component';
import { UsersComponent } from './users/users.component';
import { NubariumComponent } from './nubarium/nubarium.component';
import { AngularYandexMapsModule, IConfig } from 'angular8-yandex-maps';
import { RulesComponent } from './rules/rules.component';

import { H2hConfigComponent } from './h2h-config/h2h-config.component';
import { FraudComponent } from './fraud/fraud.component';
import { StatisticsFraudComponent } from './statistics/statistics-fraud/statistics-fraud.component';
import { StatisticsGeneralComponent } from './statistics/statistics-general/statistics-general.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { StatusComponent } from './reports/status/status.component';
import { AnalyticsSpecificComponent } from './analytics/analytics-specific/analytics-specific.component';
import { AnalyticsKpisComponent } from './analytics/analytics-kpis/analytics-kpis.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { FicoScoreComponent } from './fico-score/fico-score.component';
import { PerfilesComponent } from './perfiles/perfiles.component';
import { CirculoCreditoComponent } from './circulo-credito/circulo-credito.component';
//import { NgLeafletModule } from '@canvasoft/ng-leaflet';
//import { LeafletComponent } from 'ng-leaflet/src/components/leaflet.component';

//import { YaMapComponent } from 'angular8-yandex-maps/lib/components/ya-map/ya-map.component';

const mapConfig: IConfig = {
  apikey: '658f67a2-fd77-42e9-b99e-2bd48c4ccad4',
  lang: 'en_US',
};

@NgModule({
  declarations: [CreateClienteComponent, OfertasComponent,
    SecuredTouchComponent, BlackListComponent, ReportsComponent, DetailComponent, BlackListRemoveComponent, BlackListReportComponent,
    UsersComponent, NubariumComponent, RulesComponent, H2hConfigComponent, FraudComponent, 
    StatisticsFraudComponent, AnalyticsSpecificComponent, StatisticsGeneralComponent, StatusComponent, 
    AnalyticsSpecificComponent, AnalyticsKpisComponent, NotificationsComponent, FicoScoreComponent, PerfilesComponent,
    CirculoCreditoComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(AdministradorRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    MatDatepickerModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    NgxChartsModule,
    //LeafletComponent
   // UiLeafletModule,
  ],
  exports: [
    CreateClienteComponent,
    MatTableModule
    //LeafletComponent
    //UiLeafletModule,
  ]
})
export class AdministradorModule { }
