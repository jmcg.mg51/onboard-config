import { Component, OnInit, Input, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';

import { Oferta } from 'app/model/Clientes';
import { MiddleOfertaMongoService } from 'app/services/http/middle-oferta-mongo.service';
import {CognitoService} from 'app/services/aws/cognito.service'
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'environments/environment';
import { off } from 'process';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';

const Swal = require('sweetalert2');


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  // Elementos de entrada
  @Input() idCliente = '';

  // MARK: Variables
  allUsers: {};
  typeMessage: string;
  typeAlert: string; 
  showAlert: boolean;
  perfil: string;
  userRole;
  showAlertPerfil: boolean;

  perfiles = [{"name":"DIRECTOR", "value":"Director"},
              {"name":"EJECUTIVO", "value":"Ejecutivo"}, 
              {"name":"GERENTE", "value":"Gerente"}];
  

  constructor(private formBuilder: FormBuilder, private middleOferta: MiddleOfertaMongoService,
    public router: Router, private spinner: NgxSpinnerService,
    private cognito: CognitoService, private actRoute: ActivatedRoute, public dialog: MatDialog) { }

  ngOnInit() {
    this.actRoute.params.subscribe(params => {
        this.idCliente = params['id'];
      });
    console.log("idClienteUsuario", this.idCliente);
    this.getUsers(this.idCliente);

    this.userRole = this.cognito.getRole();
    console.log("El userRole es", this.userRole)

  }

  /**
   * Obtiene todos los clientes de un comercio en especifico
   * PARAM
   * id: identificador del comercio
   */
  async getUsers(id:string){ 
      let users;
      users = await this.cognito.getAllUsers(id);
      this.allUsers = users;
      console.log("todos los usuarios", this.allUsers);
      this.cleanInputs();

      
      //this.ngOnInit();
  }

  /**
   * Crea un usuario en un comercio especifico
   * PARAM
   * name: nombre del usuario que se dará de alta
   */
  async addUser(name: string){ 
      this.spinner.show();

      var inputPerfil = document.getElementById("selectPerfil") as HTMLInputElement
      this.perfil = inputPerfil.value;
      console.log("usuario a crear", name ,this.perfil);
      if(name === ""){
        this.showAlert = true; 
        this.spinner.hide();
        return;
      }else if(this.perfil == "value1"){
        this.showAlert = false; 
        this.showAlertPerfil = true; 
        this.spinner.hide();
        return;
      }else{
        this.showAlertPerfil = false; 
        this.showAlert = false; 
      }

      if(name.match('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')){
        name = name.trim();
        this.showAlert = false; 
      }else{
        this.showAlert = true; 
        this.spinner.hide();
        return;
      }
      
      var nameComercio = localStorage.getItem("nombreComercio");
      var idComercio = this.idCliente;
      let createUser;

      console.log("Voy a crear el usuario");
      createUser = this.cognito.createUserFromAdmin(name, nameComercio, idComercio, this.perfil).then(response => {
        console.log(response);
        if(response === "An account with the given email already exists."){
          var iconM = "error";
          var titleM = "El usuario ya se encuentra registrado";
          this.swalAlertConfig("Error", iconM, titleM);
          this.spinner.hide();
          return; 
        }
        this.typeMessage = response; 
        this.typeAlert = "Add"; 
        this.cerrarModal();
        this.spinner.hide();
        this.showMessageAlert(this.typeAlert, this.typeMessage);
    },function(error){
        console.log(error);
        this.spinner.hide();
    });
    
  }

  /**
   * Elimina un usuario especifico de un comercio
   * PARAM
   * id: identificador del usuario
   */

  async deleteUser(id:string){
    console.log("el usuario a borrar es", id);
    let deleteUser;
    deleteUser = this.cognito.deleteUserFromAdmin(id).then(response => {
        console.log(response);
        this.typeMessage = response; 
        this.typeAlert = "Delete";
        this.showMessageAlert(this.typeAlert, this.typeMessage);
        this.spinner.hide();
    },function(error){
        console.log(error);
        this.spinner.hide();
    });
   
  }

  /**
   * Mostrar mensajes de confirmación o error
   */

   async showMessageAlert(typeAlert:string, typeMessage: string){
    switch(typeAlert) { 
        case "Add": { 
            if(typeMessage == "OK"){
                var iconM = "success";
                var titleM = "Se ha enviado el usuario y contraseña al correo registrado";
                this.swalAlertConfig(typeMessage, iconM, titleM);
                break;
            }else{
                var iconM = "error";
                var titleM = "El usuario no se agrego, intentelo más tarde";
                this.swalAlertConfig(typeMessage, iconM, titleM);
                break;
            }  
        } 
        case "Delete": { 
            if(typeMessage == "OK"){
                var iconM = "success";
                var titleM = "El usuario se ha eliminado con éxito";
                this.swalAlertConfig(typeMessage, iconM, titleM);
                break;
            }else{
                var iconM = "error";
                var titleM = "El usuario no se ha podido eliminar, intentelo más tarde";
                this.swalAlertConfig(typeMessage, iconM, titleM);
                break;
            }
        } 
        default: { 
           //statements; 
           break; 
        } 
     }
    
   }

   async swalAlertConfig(type:string, iconM:string, titleM:string){
    if(type == "OK"){
        Swal.fire({
            icon: iconM,
            title: titleM,
            // text: 'Something went wrong!',
          });
          this.getUsers(this.idCliente);
    }else{
        Swal.fire({
            icon: iconM,
            title: titleM,
            // text: 'Something went wrong!',
          });
    }
   }


   cleanInputs(){
    if(document.getElementById("inputEmail")){
      var inputValue = document.getElementById("inputEmail") as HTMLInputElement;
      inputValue.value = ""; 
    }

   }


   showModalAdduser(){
    const modal = document.getElementById('modalAddUser');
    modal.style.display = 'block';
   }

   async cerrarModal() {
    this.showAlert = false; 
    this.cleanInputs();
    const modal = document.getElementById('modalAddUser');
    modal.style.display = 'none';
  }
 
  disableInputUser(){
    $("#btn-add").prop("disabled", true);
  }

}

