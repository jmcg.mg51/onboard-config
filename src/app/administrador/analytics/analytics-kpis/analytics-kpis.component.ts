import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// import * as Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner';
import { MiddleConfService } from 'app/services/http/middle-conf.service';
import { MiddleMongoService } from '../../../services/http/middle-mongo.service';
import { Integer } from 'aws-sdk/clients/apigateway';
import { MatDatepickerInputEvent } from '@angular/material';
import {ExcelService} from '../../../services/other/excel.service';
import { MiddleReportsService } from '../../../services/http/middle-reports_service';
import { CognitoService } from '../../../services/aws/cognito.service';
import { TableService } from '../../../services/other/table.service';
import { NgxChartsModule }from '@swimlane/ngx-charts';
import { Console } from 'console';

const Swal = require('sweetalert2');

@Component({
  selector: 'app-analytics-kpis',
  templateUrl: './analytics-kpis.component.html',
  styleUrls: ['./analytics-kpis.component.css']
})
export class AnalyticsKpisComponent implements OnInit {

  clients: any;
  offers: any;
  exportData: any;
  fechaInicial;
  fechaFinal;
  fechaFinalMax;
  inputReadonly: boolean;
  showErrorDates: boolean;
  showAllShops: boolean;
  userRole;
  disableOffers: boolean;

  //MARK: DATA 
  registers = []; 
  timeAverage; 
  interactionsAverage; 
  node;
  nameNodeMayor;
  registersSuccesful;
  registersError; 
  registersTotal;
  pcAbandonment;
  movilAbandonment;
  hibridAbandonment;
  maxFechaInicial: Date;
  //GRAPHICS DATA
  dataRegisters;
  dataAbandonment;
  dataGraphicRegister: any;
  dataGraphicAbandonment: any;
  dataGraphicRegisterFinal; 
  dataGraphicAbandonmentFinal;

  //Excel DATA
  exportDataRegisters;
  showSpinnerOffer: boolean;

  constructor(private middle: MiddleConfService, private router: Router, private spinner: NgxSpinnerService,
    private middleMongo: MiddleMongoService, private excelService:ExcelService,
    private middleReport: MiddleReportsService, private cognito: CognitoService,
    private ngxChart: NgxChartsModule, private table: TableService) { }

  async ngOnInit() {
    this.spinner.show();
    this.userRole = this.cognito.getRole();
    console.log("El userRole es", this.userRole)
    this.maxFechaInicial = new Date();
    if(this.userRole !== "AdministradorMit"){
      this.showAllShops = false; 
    }else{
      this.showAllShops = true; 
    }

    this.getShops();
    this.loadJs();
    this.initValues();
    this.spinner.hide();
  }

  async onOptionsSelected(value: string){
    console.log("El cliente que buscaré es:", value);
    //this.offers
    this.disableOffers = true;
    this.showSpinnerOffer = true;
    const offersService =  await this.middleMongo.getCustomer(value);
    console.log(offersService);
    for(value in offersService){
      console.log("Las ofertas del comercio son:", offersService[value].ofertas);
      this.offers = offersService[value].ofertas;
    }
    this.disableOffers = false; 
    this.showSpinnerOffer = false;
  }

  onOptionsSelectedOffer(){

  }

  async getShops(){
    this.clients = await this.middleMongo.getCustomers();
    console.log("El listado de comercios es", this.clients);
  }

  async generateReport(){
    this.dataGraphicRegister = [];
    this.dataGraphicAbandonment = [];
    this.spinner.show();
    this.initValues();

    var date = this.getDateStart();
    var dateFinal = this.getDateEnd();
    if(date === "" || date === "NaN-aN-0NaN" || date === "NaN-aN-NaN" ){ 
      date = " "
      //dateFinal = " "
      this.showErrorDates = true;
      this.spinner.hide();
      return; 
    }else{
      if(dateFinal === "" || dateFinal === "NaN-aN-0NaN" || dateFinal === "NaN-aN-NaN"){
        dateFinal = " "
        this.showErrorDates = true;
        this.spinner.hide();
        return; 
      }else{
        this.showErrorDates = false;
      }
    }

    var inputShop = document.getElementById("selectShop") as HTMLInputElement;
    var shop = inputShop.value;
    var shopId = $("#selectShop option:selected").text();

    var inputOffer = document.getElementById("selectOffer") as HTMLInputElement;
    var offer = inputOffer.value;

    if(shop == "0"){
      if(this.userRole !== "AdministradorMit"){
        shop = sessionStorage.getItem('nombre_comercio'); 
      }else{
        shop = " " 
      }
    }else{
      shop = $("#selectShop option:selected").text();
    }
    if(offer == "0"){
      if(this.userRole !== "AdministradorMit"){
        offer = sessionStorage.getItem('nombre_comercio'); 
      }else{
        offer = " " 
      }
    }else{
      shop = $("#selectShop option:selected").text();
    }

    const query = {
      dateInit: date,
      dateEnd: dateFinal,
      cliente: shop,
      oferta: offer
    }

    console.log("La query a buscar es:", query);

    this.registers = await this.middleReport.createReportDetailAnalytics(query);
    //this.users = await this.middleReport.createReportOfFiles(typeReport, query);
    if(this.registers['registros']){
      console.log("respuesta del servicio", this.registers);
    console.log("respuesta", JSON.stringify(this.registers));

    var tempTimeAverage = this.registers["tiempoPromedio"] ? this.registers["tiempoPromedio"].toFixed(0) : 0;
    this.timeAverage = tempTimeAverage.toString() + " segundos"
    this.registersTotal = this.registers["registros"].length; 
    this.interactionsAverage = this.registers["promedioIntentos"] ? this.registers["promedioIntentos"].toFixed(1) : 0;

    this.createArrayGraphicRegisters(this.registers["estadoSolicitudes"]);

    this.calculateDeviceAbandonment(this.registers["dispositivosAbandono"]);
    this.calculateNodoDesercion(this.registers["nodosDesercion"])
    this.createDetailTable(this.registers["registros"]);

     this.spinner.hide();

    }else{
      console.log("No hay valores");
      this.spinner.hide();
    }
    
  }

  calculateNodoDesercion(nodos){
    console.log("Los nodos con mayor deserció son:", nodos);
    var nodoMayor = 0;
    var nameNodoMayor;
    for(const value in nodos){
      console.log(nodos[value]);
      if (nodoMayor < nodos[value]){
        nodoMayor = nodos[value];
        nameNodoMayor = value;
      }
    }
    this.nameNodeMayor = nameNodoMayor;
  }

  calculateDeviceAbandonment(abandonment){
    const dispositivosAbandono =  abandonment
    var registerAbandonoDataTable; 
    var registerTemp; 
  for (const index in dispositivosAbandono){
    console.log("Valor de solitud", dispositivosAbandono[index].count);

    if(dispositivosAbandono[index].totalAbandonoDispositivo[0] === "Movil"){
      registerTemp = dispositivosAbandono[index].count;
      registerAbandonoDataTable = { "name": "Movil", "value": registerTemp};
      this.dataGraphicAbandonment.push(registerAbandonoDataTable);
      this.movilAbandonment = dispositivosAbandono[index].count;
      
    }
    else if(dispositivosAbandono[index].totalAbandonoDispositivo[0] === "Hibrido"){
      registerTemp = dispositivosAbandono[index].count;    
      registerAbandonoDataTable = { "name": "Hibrido", "value": registerTemp};
      this.dataGraphicAbandonment.push(registerAbandonoDataTable);
      this.hibridAbandonment = dispositivosAbandono[index].count;
    }
    else if(dispositivosAbandono[index].totalAbandonoDispositivo[0] === "Computadora"){
      registerTemp = dispositivosAbandono[index].count;
      registerAbandonoDataTable = { "name": "Computadora", "value": registerTemp};
      this.dataGraphicAbandonment.push(registerAbandonoDataTable);
      this.pcAbandonment = dispositivosAbandono[index].count;
    }

  }
  this.dataGraphicAbandonmentFinal = this.dataGraphicAbandonment;

  }

  createArrayGraphicRegisters(registers){
    const estadoSolitudes =  registers
    var registerDataTable; 
    for (const index in estadoSolitudes){
      console.log("Valor de solitud", estadoSolitudes[index].count);

      if(estadoSolitudes[index].totalStatus[0] === "CONCLUIDO"){
        this.registersSuccesful = estadoSolitudes[index].count;
        registerDataTable = { "name": "Completado", "value": this.registersSuccesful};
        this.dataGraphicRegister.push(registerDataTable);
        
      }
      else if(estadoSolitudes[index].totalStatus[0] === "ABANDONO"){
        this.registersError = estadoSolitudes[index].count;
        registerDataTable = { "name": "Abandono", "value": this.registersError};
        this.dataGraphicRegister.push(registerDataTable);
      }

    }
    this.dataGraphicRegisterFinal = this.dataGraphicRegister;
  }

  isJson(text){
    try {
      var o = JSON.parse(text);
      if (o && typeof o === "object") {
      return true;
      }
      }
      catch (e) { }
      
       return false;
  }
  
  
  createDetailTable(response){
    this.exportDataRegisters = [];
      var table = $("#dtResultReport").DataTable();
      table.clear()
      table.destroy();
  
    for(const value in response){
      var dataResponse = response[value];
      if(this.isJson(JSON.stringify(response[value]))){
        for(const register in dataResponse){
          
          if(this.isJson(JSON.stringify(dataResponse[register]))){
            $('<tr>').append(
              $('<td>').text(dataResponse.trackId),
              $('<td>').text(dataResponse[register].dispositivo),
              $('<td>').text(register),
              $('<td>').text(dataResponse[register].intentos),
              $('<td>').text(dataResponse[register].fechaInicialLocale),
              $('<td>').text(dataResponse[register].fechaFinalLocale)).appendTo('#dtResultReport tbody');

              var fechaInicial;
              var fechaFinal;
              if(dataResponse[register].fechaInicialLocale){
                fechaInicial = this.changeFormatDate(dataResponse[register].fechaInicialLocale);
              }else{
                fechaInicial = dataResponse[register].fechaInicialLocale
              }
              
              if(dataResponse[register].fechaFinalLocale){
                fechaFinal = this.changeFormatDate(dataResponse[register].fechaFinalLocale);
              }else{
                fechaFinal = dataResponse[register].fechaFinalLocale;
              }
              
              

              const nodoDataTable = { trackid: `${dataResponse.trackId}`, dispositivo: `${dataResponse[register].dispositivo}`, nodo: `${register}`, intentos: `${dataResponse[register].intentos}`, ingreso: `${fechaInicial}`, salida: `${fechaFinal}`, cliente: `${dataResponse.nombreCliente}`, oferta: `${dataResponse.nombreOferta}` }; 
              this.exportDataRegisters.push(nodoDataTable);
          }

        }
        
      }
          
    }
    this.table.paginateTable("#dtResultReport")    
    $('.dataTables_length').addClass('bs-select');
  
  }

  exportTableAnalytics(){
    this.excelService.exportAsExcelFile(this.exportDataRegisters, "Desglose");
}

  initValues(){
    this.inputReadonly = true; 
    this.fechaInicial = "";
    this.fechaFinalMax = "";
    this.pcAbandonment = 0;
    this.movilAbandonment = 0;
    this.hibridAbandonment =0;
    this.registersSuccesful = 0;
    this.registersError = 0;
    this.disableOffers = true;
    //NEW INITS
    this.timeAverage = 0;
    this.registersTotal = 0;
    this.interactionsAverage = 0;
    this.showSpinnerOffer = false;
    this.nameNodeMayor = "";
    this.dataGraphicRegisterFinal = [];
    this.dataGraphicAbandonmentFinal = [];
    var table = $("#dtResultReport").DataTable();
    table.clear();
    table.destroy();
    
  }

  getDateStart(){
    var date = $('#date1Search').val();
    var formatDate = new Date(date.toString().replace(',','') );
    var day; 
    if(formatDate.getDate().toString().length > 1){
      day = formatDate.getDate();
    }else{
      day = "0" + formatDate.getDate();
    }
    var dateFinal = formatDate.getFullYear()+"-"+("0"+(formatDate.getMonth()+1)).slice(-2)+"-"+ day;
    this.fechaInicial = dateFinal
    this.inputReadonly = false; 
    return this.fechaInicial;
  }

  getDateEnd(){
    var date = $('#date2Search').val();
    var formatDate = new Date(date.toString().replace(',',''));
    var day; 
    if(formatDate.getDate().toString().length > 1){
      day = formatDate.getDate();
    }else{
      day = "0" + formatDate.getDate();
    }
    var dateFinal = formatDate.getFullYear()+"-"+("0"+(formatDate.getMonth()+1)).slice(-2)+"-"+ day;
    this.fechaFinal = dateFinal;
    return this.fechaFinal;
  }

  public loadJs() {
    const body = <HTMLDivElement>document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = '../../assets/js/exportFile.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);

  }


  changeFormatDate(dateResponse){
    var formattedDate;
    if(dateResponse != undefined){
      dateResponse = dateResponse.replace("GMT-6","");
      dateResponse = dateResponse.replace(",","");
        console.log("LA fecha a convertir es", dateResponse);
        var initialDate = dateResponse
  
      //Dividimos la fecha primero utilizando el espacio para obtener solo la fecha y el tiempo por separado
      var splitDate= initialDate.split(" ");
      var date=splitDate[0].split("/");
      var time=splitDate[1].split(":");
  
      // Obtenemos los campos individuales para todas las partes de la fecha
      var dd=date[0];
      var mm=date[1]-1;
      var yyyy =date[2];
      var hh=time[0];
      var min=time[1];
      var ss=time[2];
  
      // Creamos la fecha con Javascript
      var fecha = new Date(yyyy,mm,dd,hh,min,ss);
  
      // De esta manera se puede volver a convertir a un string
      formattedDate = ("0" + fecha.getDate()).slice(-2) + '/' + ("0" + (fecha.getMonth() + 1)).slice(-2) + '/' + fecha.getFullYear() + ' ' + ("0" + fecha.getHours()).slice(-2) + ':' + ("0" + fecha.getMinutes()).slice(-2) + ':' + ("0" + fecha.getSeconds()).slice(-2);
      console.log("La fecha con formato es:", formattedDate)
      
    }
    return formattedDate;
  }
  
  
  async buscarPorFecha(type: string, event: MatDatepickerInputEvent<Date>) {
    if (type === 'fechaInicial') {
      this.fechaInicial = event.value;
      let diasMax = this.compareDates();
      if( diasMax > 90 )  {
        diasMax = 90;
      }

      if(diasMax < 90){
        this.fechaFinal = new Date();
      }
      this.fechaFinalMax = new Date(this.fechaInicial.getFullYear(),
      this.fechaInicial.getMonth(), this.fechaInicial.getDate() + diasMax);
      console.log(this.fechaFinalMax);
    } else {
      this.spinner.show();
      console.log('si cambie de fecha');
      this.fechaFinal = event.value;
      
      this.spinner.hide();
      //console.log(this.result);
    }
  }

  generaFechaToSend(date: Date) {
    return `${date.getFullYear()}-${this.offsetIzquierda(date.getMonth() + 1)}-${this.offsetIzquierda(date.getDate())}`;
  }

  offsetIzquierda(numero: Number) {
    let nuevo = '' + numero;
    if (nuevo.length === 1) {
      nuevo = '0' + nuevo;
    }
    return nuevo;
  }

  compareDates() {
    let currentDate = new Date();
    console.log("La fecha actual es", currentDate);
    return Math.floor((Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()) - Date.UTC(this.fechaInicial.getFullYear(), this.fechaInicial.getMonth(), this.fechaInicial.getDate()) ) /(1000 * 60 * 60 * 24));
  }
    
 }


