import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerInputEvent } from '@angular/material';
import { MiddleMongoService } from '../../../services/http/middle-mongo.service';
import { MiddleReportsService } from '../../../services/http/middle-reports_service';
import { CognitoService } from '../../../services/aws/cognito.service';
import { ExcelService } from './../../../services/other/excel.service';

@Component({
  selector: 'app-reports',
  templateUrl: './analytics-specific.component.html',
  styleUrls: ['./analytics-specific.component.css']
})

export class AnalyticsSpecificComponent implements OnInit {

  //MARK: buttons
  showButtonReport: boolean;
  showEndDateInput: boolean;
  showSearchInput: boolean;

  //usuarios: any;
  selected: string;
  fechaInicial: Date;
  result: any;
  informacion: string
  userRole;

  //MARK ANALYTICS
  showDetailAnalytics;
  exportDataAnalytics = [];
  onlyRegister;


  constructor(private middleMongo: MiddleMongoService, public router: Router,
    private spinner: NgxSpinnerService, private cognito: CognitoService,
    private middleReport: MiddleReportsService, private excelService: ExcelService,) {

  }

  async ngOnInit() {
    await this.spinner.show();
    this.userRole = this.cognito.getRole();


    await this.spinner.hide();
  }

  offsetIzquierda(numero: Number) {
    let nuevo = '' + numero;
    if (nuevo.length === 1) {
      nuevo = '0' + nuevo;
    }
    return nuevo;
  }

  generaFechaToSend(date: Date) {
    return `${date.getFullYear()}-${this.offsetIzquierda(date.getMonth() + 1)}-${this.offsetIzquierda(date.getDate())}`;
  }

  compareDates() {
    let currentDate = new Date();
    return Math.floor((Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()) - Date.UTC(this.fechaInicial.getFullYear(), this.fechaInicial.getMonth(), this.fechaInicial.getDate())) / (1000 * 60 * 60 * 24));
  }

  async buscarPorTexto() {
    var tableA = $('#dtDetailAnalytics').DataTable();
    tableA.clear();
    tableA.destroy();
    this.showDetailAnalytics = false;
    sessionStorage.removeItem('exportButton');
    var informacionInput = $('#inputSearch').val().toString();
    this.informacion = informacionInput;
    this.spinner.show();
    let data = {};
    if (!informacionInput || informacionInput === '') {
      this.spinner.hide();
      return;
    }
    if (this.selected === 'correo') {
      console.log("data de correo");
      if (this.userRole !== "AdministradorMit") {
        var shop = sessionStorage.getItem('codigo_comercio');
        data = {
          correo: informacionInput,
          idCliente: shop
        };

      } else {
        data = {
          correo: informacionInput
        };
      };

      //data = {
      //  correo: informacionInput
      //};
    } else if (this.selected === 'trackId') {
      console.log("data de trackId");
      if (this.userRole !== "AdministradorMit") {
        var shop = sessionStorage.getItem('codigo_comercio');
        data = {
          trackId: informacionInput,
          idCliente: shop
        };

      } else {
        data = {
          trackId: informacionInput,
        };
      };
      // data = {
      //   trackId: informacionInput
      // };
    }
    console.log('voy a buscar : ', data);
    await this.middleMongo.getReportResult(data).then(response => {
      // $('#dtBasicExample').DataTable().ajax.reload();
      this.result = response;
    }, function (error) {
      console.log(error);
    });
    sessionStorage.setItem('resultSearch', JSON.stringify(this.result));
    if (this.result.length > 0) {
      this.showButtonReport = true;
      var table = $('#dtBasicExample').DataTable();
      table.clear();
      table.destroy();

      //crear tabla
      this.createTable(this.result);

    }

    this.spinner.hide();
    console.log("RESULTADO=", this.result);

  }


  async viewDetail(idAnalitics: string, oferta, comercio) {

    this.spinner.show();
    this.showDetailAnalytics = true;
    const result = await this.middleReport.createReportOfAnalytics(idAnalitics);
    //console.log("El resultado es:", result);
    //console.log("El registro unico es:", register[0]);

    await this.createDetailTable(result, oferta, comercio);

    this.spinner.hide();

  }

  select(type) {
    if (type == 'correo' || type == 'trackId') {
      this.showSearchInput = true;
      this.showEndDateInput = false;
      $("#inputSearch").val("");


    } else {
      this.showSearchInput = false;
    }
  }

  clearSessionSearch() {
    sessionStorage.removeItem('tipoDeBusqueda');
    sessionStorage.removeItem('tipoBusqueda');
    sessionStorage.removeItem('correoSearch');
    sessionStorage.removeItem('trackIDSearch');
    sessionStorage.removeItem('date1Search');
    sessionStorage.removeItem('date2Search');
  }

  createTable(response) {
    console.log("la respuesta es", response);
    $.each(response, function (i, item) {
      if (item.idAnalitics) {
        $('<tr>').append(
          $('<td>').text(item.nombreCliente),
          $('<td>').text(item.nombreOferta),
          $('<td>').text(item.date),
          $('<td>').text(item.dateUpdated),
          $('<td>').text(item.estatus),
          $('<td>').append('<button id="' + item.idAnalitics + '" value="' + item.trackId + '" oferta="' + item.nombreOferta + '" comercio="' + item.nombreCliente + '" class="btn btn-primary mr-4 mt-3 ButtonViewDetail"  mat-raised-button color="primary" data-toggle="tooltip" data-placement="top" title="Ver usuario"><i class="fa fa-eye" aria-hidden="true"></i>Ver Detalle</button>')).appendTo('#dtBasicExample tbody');

      }
      else {

      }


    });

    $('#dtBasicExample').DataTable({
      "lengthMenu": [5],
      "lengthChange": false,
      "pagingType": "full_numbers", // "simple" option for 'Previous' and 'Next' buttons only
      "language": {
        "lengthMenu": "Mostrar _MENU_ resultados por página",
        "zeroRecords": "No hay resultados",
        "info": "Resultados: _START_ de _END_ de _TOTAL_ de trámites",
        "infoEmpty": "No hay resultados disponibles",
        "search": "Buscar:",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "paginate": {
          "first": "Primero",
          "last": "Ultimo",
          "next": "Siguiente",
          "previous": "Anterior"
        },
      },
      "drawCallback": () => {
        $(".ButtonViewDetail").click((e) => {
          var id = e.target.id
          var oferta = (e.target.attributes.getNamedItem("oferta")).value;
          var comercio = (e.target.attributes.getNamedItem("comercio")).value;
          console.log("EL id seleccionado es", e.target.id);
          console.log("la oferta seleccionada es", oferta);
          console.log("el comercio seleccionada es", comercio);
          // this.getInfoTramite(track, id);
          this.viewDetail(id, oferta, comercio);
        });
      }

    });

    $('.dataTables_length').addClass('bs-select');

  }


  isJson(text) {
    try {
      var o = JSON.parse(text);
      if (o && typeof o === "object") {
        return true;
      }
    }
    catch (e) { }

    return false;
  }


  createDetailTable(response, oferta, comercio) {
    this.exportDataAnalytics = [];
    var table = $("#dtDetailAnalytics").DataTable();
    table.clear()
    table.destroy();

    for (const value in response) {

      if (this.isJson(JSON.stringify(response[value]))) {
        console.log(value);
        var estatus;
        if (response[value].abandono === false) {
          estatus = "Terminado"
        } else {
          estatus = "Iniciado"
        }
        let modoText =  !response[value].skip ? '<tr>' : '<tr style="background:orange">';
        // if (response[value].skip) {
        //   modoText = '<tr style="background:orange">';
        // }
        $(modoText).append(
          $('<td>').text(response.trackId),
          $('<td>').text(value),
          $('<td>').text(estatus),
          $('<td>').text(response[value].fechaFinalLocale),
          $('<td>').text(response[value].dispositivo)).appendTo('#dtDetailAnalytics tbody');

        let dateResponse;
        if (response[value].fechaFinalLocale) {
          dateResponse = this.changeFormatDate(response[value].fechaFinalLocale);
          console.log(dateResponse);
        }
        console.log(value, dateResponse);
        const nodoDataTable = { trackid: `${response._id}`, proceso: `${value}`, estatus: `${estatus}`, fecha: `${dateResponse}`, dispositivo: `${response[value].dispositivo}`, modo: `${response[value].skip ? 'SKIP' : ''}`, oferta: `${oferta}`, comercio: `${comercio}` };
        this.exportDataAnalytics.push(nodoDataTable);
        
      }

    }
    console.log(this.exportDataAnalytics);

  }

  exportTableAnalytics() {
    this.excelService.exportAsExcelFile(this.exportDataAnalytics, "Desglose de analitica");
  }

  changeFormatDate(dateResponse) {
    var formattedDate;
    if (dateResponse != undefined) {
      dateResponse = dateResponse.replace("GMT-6", "");
      console.log("LA fecha a convertir es", dateResponse);
      var initialDate = dateResponse

      //Dividimos la fecha primero utilizando el espacio para obtener solo la fecha y el tiempo por separado
      var splitDate = initialDate.split(" ");
      var date = splitDate[0].split("/");
      var time = splitDate[1].split(":");

      // Obtenemos los campos individuales para todas las partes de la fecha
      var dd = date[0];
      var mm = date[1] - 1;
      var yyyy = date[2];
      var hh = time[0];
      var min = time[1];
      var ss = time[2];

      // Creamos la fecha con Javascript
      var fecha = new Date(yyyy, mm, dd, hh, min, ss);

      // De esta manera se puede volver a convertir a un string
      formattedDate = ("0" + fecha.getDate()).slice(-2) + '/' + ("0" + (fecha.getMonth() + 1)).slice(-2) + '/' + fecha.getFullYear() + ' ' + ("0" + fecha.getHours()).slice(-2) + ':' + ("0" + fecha.getMinutes()).slice(-2) + ':' + ("0" + fecha.getSeconds()).slice(-2);
      console.log("La fecha con formato es:", formattedDate)

    }
    return formattedDate;
  }



}

