import { Component, OnInit, Input, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';

import { Oferta } from 'app/model/Clientes';
import { MiddleOfertaMongoService } from 'app/services/http/middle-oferta-mongo.service';
import { CognitoService } from 'app/services/aws/cognito.service'
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'environments/environment';
import { off } from 'process';
import { MatDialog, MAT_DIALOG_DATA, throwMatDialogContentAlreadyAttachedError } from '@angular/material/dialog';
import { NotificactionsService } from 'app/services/http/notifications.service';
import { MiddleMongoService } from './../../services/http/middle-mongo.service';
import { Ofertas, Clientes, Users } from 'app/model/Clientes';

const Swal = require('sweetalert2');


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  // Elementos de entrada
  @Input() idCliente = '';

  // MARK: Variables
  allUsers: {};
  typeMessage: string;
  typeAlert: string;
  showAlert: boolean;
  showAlertRegex: boolean;
  perfil: string;
  userRole;
  textoModal;
  messagePending;
  messageReview;
  messageRejected;
  showMessageError;
  id;
  responseClient;
  cliente = new Clientes();
  notifications;
  responseNotifications;
  filtersLoaded: Promise<boolean>;
  messagePendingInput;
  messageReviewInput;
  messageRejectedInput;


  constructor(private formBuilder: FormBuilder, private middleOferta: MiddleOfertaMongoService,
    public router: Router, private spinner: NgxSpinnerService,
    private cognito: CognitoService, private actRoute: ActivatedRoute, public dialog: MatDialog,
    private notification: NotificactionsService, private client: MiddleMongoService,) { }

  async ngOnInit() {
    this.spinner.show();
    this.userRole = this.cognito.getRole();
    console.log("El userRole es", this.userRole);
    this.cliente = new Clientes();
    this.actRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    const objectResponse = await this.client.getCustomer(this.id);
    console.log("El cliente es:", objectResponse);
    //this.responseClient = objectResponse['response'];
    Object.assign(this.cliente, objectResponse['response']);

    if(objectResponse['response'] && objectResponse['response']["notifications"]){
      console.log("Tengo que pintar valores");
      this.responseNotifications = objectResponse['response']["notifications"];
      console.log("valor",this.responseNotifications.pending.message);
      this.messagePendingInput = this.responseNotifications.pending.message;
      this.messageReviewInput = this.responseNotifications.review.message;
      this.messageRejectedInput = this.responseNotifications.rejected.message
      // this.setValues(objectResponse,this.responseNotifications.pending.message, this.responseNotifications.review.message,
      //   this.responseNotifications.rejected.message);

      
      
    }
    this.spinner.hide();

  }

  /**
   * Mostrar mensajes de confirmación o error
   */

  async showMessageAlert(typeAlert: string, typeMessage: string) {
    switch (typeAlert) {
      case "Add": {
        if (typeMessage == "OK") {
          var iconM = "success";
          var titleM = "Las notificaciones se han guardado con éxito";
          this.swalAlertConfig(typeMessage, iconM, titleM);
          break
        } else {
          var iconM = "error";
          var titleM = "Ocurrió un error, intentalo más tarde";
          this.swalAlertConfig(typeMessage, iconM, titleM);
          break
        }
      }
      default: {
        //statements; 
        break
      }
    }
  }

  async swalAlertConfig(type: string, iconM: string, titleM: string) {
    if (type == "OK") {
      Swal.fire({
        icon: iconM,
        title: titleM,
        // text: 'Something went wrong!',
      });
    } else {
      Swal.fire({
        icon: iconM,
        title: titleM,
        // text: 'Something went wrong!',
      });
    }
  }

  cleanInputs() {
    if (document.getElementById("inputEmail")) {
      var inputValue = document.getElementById("inputEmail") as HTMLInputElement;
      inputValue.value = "";
    }

    if (document.getElementById("inputName")) {
      var inputValue = document.getElementById("inputName") as HTMLInputElement;
      inputValue.value = "";
    }

  }

  disableInputUser() {
    $("#btn-add").prop("disabled", true);
  }

  abrirModal(id) {
    let idEmail = "#" + id;
    console.log(idEmail);
    let textEmail = $(idEmail).val();
    this.textoModal = textEmail;

    const modal = document.getElementById('modalTerminos');
    modal.style.display = 'block';
  }

  cerrarModal() {
    const modal = document.getElementById('modalTerminos');
    modal.style.display = 'none';
  }

  async saveNotifications() {
    this.spinner.show();
    console.log("guardar ");
    this.messagePending = $("#pass").val();
    this.messageReview = $("#review").val();
    this.messageRejected = $("#rejected").val();

    // if (this.messagePending == "" || this.messageRejected == "" || this.messageReview == "") {
    //   this.showMessageError = true;
    //   this.spinner.hide();
    // } else {
      this.showMessageError = false;
      this.notifications = {
        "pending": {
          "message": this.messagePending
        },
        "review": {
          "message": this.messageReview
        },
        "rejected": {
          "message": this.messageRejected
        }
      }
      console.log(this.notifications);
      //call to service 
      //TODO
      await this.updateCliente(this.cliente);

      this.spinner.hide();

    // }

  }

  async updateCliente(client) {
    console.log("el cliente es", client);
    this.cliente._id = this.id;
    this.cliente.nombre = client.nombre;
    this.cliente.correo = client.correo;
    this.cliente.telefono = client.telefono;
   // this.cliente.listasBlancasPost = client.listasBlancasPost;
   // this.cliente.listasBlancas = 
    this.cliente.hrefDaon = client.hrefDaon;
    this.cliente.iconFile = client.urlIcon;
    this.cliente.notifications = this.notifications;

    console.log("lo que voy a guardar es:", this.cliente);
    let response = await this.client.updateCustomer(this.cliente, this.id);
    console.log("response update client", response);
    if(response === "OK"){
      this.showMessageAlert("Add", "OK");
    }else{
      this.showMessageAlert("Add", "error");
    }
  }

  async setValues(objectResponse,pendingMessage, reviewMessage, rejectedMessage){

     setTimeout(function(){ 
      if(objectResponse['response']["notifications"]){
        if(document.getElementById("pass") && document.getElementById("review") && document.getElementById("rejected")){
          let pending = (document.getElementById("pass") as HTMLSelectElement);
          pending.value = pendingMessage;
          let review = (document.getElementById("review") as HTMLSelectElement);
          review.value = reviewMessage;
          let rejected = (document.getElementById("rejected") as HTMLSelectElement);
          rejected.value = rejectedMessage;
        }
      }
      
      
    },
       300);
  }

}


