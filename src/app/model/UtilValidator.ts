export function isEmpty(obj) {
  return Object.keys(obj).length === 0;
}

export function hexToB64Link(stringHex: string, nombreDeLaOferta: string) {
  let base64String = btoa(stringHex.match(/\w{2}/g).map(a => { return String.fromCharCode(parseInt(a, 16)); }).join(""));
  base64String = base64String.replace(/\+/g, '-').replace(/\//g, '_').replace(/\=+$/, '');
  base64String = btoa(`${base64String}:${nombreDeLaOferta}`).replace(/\+/g, '-').replace(/\//g, '_').replace(/\=+$/, '');
  return base64String;
}

export function returnNameOfService(nombre: string) {
  console.log('el nombre es: ' , nombre);
  const moreThanAWord = nombre.split(' ');
  nombre = nombre.replace(/\s/g, '');
  if (moreThanAWord && moreThanAWord.length > 1) {
    nombre = '';
    for (let i = 0; i < moreThanAWord.length; i++) {
      nombre += moreThanAWord[i].substring(0, 1) + moreThanAWord[i].substring(1, moreThanAWord[i].length).toLowerCase();
    }
    nombre =
      nombre.substring(0, 1).toLowerCase() + nombre.substring(1, nombre.length);
  } else {
    nombre = nombre.toLowerCase();
  }
  nombre = this.eliminarAcentos(nombre);
  return nombre;
}

export function eliminarAcentos(texto: string) {
  return texto.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
}

export function compareDates(fechaInicial: Date, diasMax: number) {
  let currentDate = new Date();
  let valoresDias = Math.floor((Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()) - Date.UTC(fechaInicial.getFullYear(), fechaInicial.getMonth(), fechaInicial.getDate()) ) /(1000 * 60 * 60 * 24));
  if(diasMax > 0 && valoresDias > diasMax) {
    valoresDias = diasMax;
  }
  return valoresDias;
}