export function serviciosCatalog() {
    let ofertasN = {};
    ofertasN['selfie'] = {
      nombre: 'selfie',
      tipo: 'Biometricos',
    }; 
    ofertasN['documento'] = {
      nombre: 'documento',
      tipo: 'Biometricos',
      propiedades: ['INE', 'PASSPORT'],
    };
    ofertasN['enroll'] = {
      nombre: 'Enroll',
      tipo: 'Biometricos'
    };
    // ofertasN['pruebaDeVida'] = {
    //   nombre: 'Prueba De Vida',
    //   tipo: 'Biometricos',
    // };
    ofertasN['comprobanteDomicilio'] = {
      nombre: 'Comprobante De Domicilio',
      tipo: 'Biometricos',
    };
    ofertasN['curp'] = {
      nombre: 'CURP',
      tipo: 'Validacion Información',
    };
    ofertasN['gps'] = {
      nombre: 'GPS',
      tipo: 'Validacion Información',
    };
    ofertasN['rfc'] = {
      nombre: 'RFC',
      tipo: 'Validacion Información',
      propiedades: ['vacio', 'Fisica', 'Moral', 'Calculo Persona Fisica' , 'Sin validar'],
    };
    ofertasN['cfdi'] = {
      nombre: 'CFDI',
      tipo: 'Validacion Información',
    };
    ofertasN['cuentaClabe'] = {
      nombre: 'Cuenta Clabe',
      tipo: 'Validacion Información',
      propiedades: ['Rechazo Score Global'],
    };
    ofertasN['tarjetaBancaria'] = {
      nombre: 'Tarjeta Bancaria',
      tipo: 'Validacion Información',
    };
    ofertasN['telefono'] = {
      nombre: 'Telefono',
      tipo: 'Validacion Información',
    };
    ofertasN['validarCurp'] = {
      nombre: 'Validar CURP',
      tipo: 'Validacion Información',
    };
    ofertasN['validarIne'] = {
      nombre: 'Validar INE',
      tipo: 'Validacion Información',
    };
    ofertasN['validarOfac'] = {
      nombre: 'Validar OFAC',
      tipo: 'Validacion Información',
    };
    ofertasN['direccion'] = {
      nombre: 'Dirección',
      tipo: 'Validacion Información',
    };
    ofertasN['seguroSocial'] = {
      nombre: 'Seguro Social ',
      tipo: 'Validacion Información',
    };
    ofertasN['cedulaProfesional'] = {
      nombre: 'Cedula Profesional ',
      tipo: 'Validacion Información',
    };
    ofertasN['69B'] = {
      nombre: '69B ',
      tipo: 'Validacion Información',
    };
    ofertasN['correoElectronico'] = {
      nombre: 'Correo Electrónico',
      tipo: 'Validacion Recepción',
    };
    ofertasN['notificaciones'] = {
      nombre: 'Notificar Usuario',
      tipo: 'Validacion Recepción',
    };
    ofertasN['datosAdicionales'] = {
        nombre: 'Datos Adicionales',
        tipo: 'Valores Adicionales',
        propiedades: ['Dato1', 'Dato2', 'Dato3', 'Dato4'],
      };
      ofertasN['ficoScore'] = {
        nombre: 'FICO Score',
        tipo: 'Análisis Crediticio',
        propiedades: ['Enviar Contrato']
      };
      ofertasN['reporteCrediticio'] = {
      nombre: 'Reporte Crediticio',
      tipo: 'Análisis Crediticio',
    };
    ofertasN['saltoDeNodo'] = {
      nombre: 'Skip',
      tipo: 'Skip',
    };
    ofertasN['firmaDigital'] = {
      nombre: 'NIP',
      tipo: 'Firma Digital',
    };
    ofertasN['archivosAdjuntos'] = {
      nombre: 'Archivos Adjuntos',
      tipo: 'Archivos Adjuntos',
      propiedades: ['Adjunto1', 'Adjunto2', 'Adjunto3', 'Adjunto4'],
    };
    ofertasN['documentoManual'] = {
      nombre: 'Documento Manual',
      tipo: 'Captura Manual',
      propiedades: ['INE', 'PASSPORT'],
    };
    ofertasN['resultadoManual'] = {
      nombre: 'Resultado',
      tipo: 'Captura Manual',
      propiedades: ['PASS', 'REVIEW'],
    };
    ofertasN['artificial'] = {
      nombre: 'Bot',
      tipo: 'Bot',
    };
    ofertasN['dispositivo'] = {
      nombre: 'Móvil',
      tipo: 'Dispositivo',
    };
    ofertasN['duplicidad'] = {
      nombre: 'Duplicidad',
      tipo: 'Validacion Información',
    };

    
    return ofertasN;
}
