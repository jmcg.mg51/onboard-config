export class Clientes {
    _id: string;
    nombre: string;
    correo: string;
    pass: string;
    ApiToken: string;
    telefono: string;
    listasBlancasPost: any[];
    listasBlancas: any[];
    hrefDaon: string;
    statusCliente: number;
    iconFile: string;
    iconHref: string;
    urlIcon: string;
    notifications;
    razon: string;
  }

  export class Ofertas {
    _id: string;
    idcliente: string;
    ofertas: Oferta[];
    ofertascliente: OfertasCliente;
  }

  export class Oferta {
    nombre: string;
    servicios: Servicio[];
  }

  // export class OfertaRefactor {
  //   nombre: string;
  //   servicios: Servicio[];
  // }

  export class Servicio {
    // nombre: string;
    // props: any[];
  }

  export class OfertasCliente {
    ofertas: Oferta[];
  }

  export class Users{
    _id: string;
    email: string;
    oferta: Oferta;
  }

