import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { CognitoService } from 'app/services/aws/cognito.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  titulo = 'Para poder ingresar, inicia sesión con el correo que registraste y tu contraseña';
  textUser: string;
  textPwd: string;
  newPass = false;
  newPassText: string;
  confirmNewPassText: string;
  error: string;
  codigo: string;
  forgotPasswordFlag = false;
  sendCodeFlag = false;
  showEstructuraCorreo: boolean;
  textoModal: string;


  constructor(public router: Router, private cognito: CognitoService, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  }

  async iniciar() {
    // this.router.navigate(['/dashboard/']);
    await this.spinner.show();
    this.showEstructuraCorreo = false;
    const resultado = await this.cognito.signIn(this.textUser, this.textPwd);
    this.error = '';
    if (resultado === '') {
      const jsonInicio = {
        usuario: this.textUser,
        horaInicio: new Date()
      };
      sessionStorage.setItem('jsonLogin', JSON.stringify(jsonInicio));
      this.router.navigate(['/dashboard/']);
    } else if (resultado === 'NEW') {
      this.titulo = 'Cambia tu contraseña';
      this.newPass = true;
      await this.spinner.hide();
    } else {
      this.textPwd = '';
      this.textUser = '';      
      this.error = 'Usuario o contraseña incorrectos';
      await this.spinner.hide();
    }
  }

  async cambioDePassword() {
    if (this.newPassText !== this.confirmNewPassText) {
      this.error = 'Tus contraseñas deben ser iguales';
      return;
    }
    await this.spinner.show();
    const resultado = await this.cognito.changePass(this.textUser, this.textPwd, this.newPassText);
    console.log(resultado);
    if (resultado === 'OK') {
      this.showEstructuraCorreo = false;
      const jsonInicio = {
        usuario: this.textUser,
        horaInicio: new Date()
      };
      sessionStorage.setItem('jsonLogin', JSON.stringify(jsonInicio));
      // this.router.navigate(['/dashboard/']);
      this.titulo = 'Inicia sesión con tu correo y tu nueva contraseña';
      this.newPass = false;
      this.forgotPasswordFlag = false;
      this.sendCodeFlag = false;
      this.textUser = '';
      this.textPwd = '';
    } else {
      this.showEstructuraCorreo = true;
      //this.resetValues('Ocurrio un error, favor de volver a intentar');
    }
    await this.spinner.hide();
  }

  forgotPass() {
    this.textUser = '';
    this.forgotPasswordFlag = true;
  }

  async forgotPassSendCode() {
    console.log(this.textUser);
    const result = await this.cognito.forgotPass(this.textUser);
    if (result === 'OK') {
      this.forgotPasswordFlag = false;
      this.sendCodeFlag = true;
    } else {
      this.resetValues('Ocurrio un error, favor de volver a intentar');
    }
  }

  async recoverPass() {
    console.log(this.codigo);
    console.log(this.textPwd);
    console.log(this.textUser);
    const result = await this.cognito.recoveryPass(this.textUser, '' + this.codigo, this.textPwd);
    if(result === 'OK') {
      this.iniciar();
    } else {
      this.error = 'Ocurrio un error, favor de volver a intentar';
      this.textPwd = '';
      this.codigo = '';
    }
  }

  resetValues(error: string) {
    this.titulo = 'Para poder ingresar, inicia sesión con el correo que registraste y tu contraseña';
    this.textUser = '';
    this.textPwd = '';
    this.newPass = false;
    this.newPassText = '';
    this.confirmNewPassText = '';
    this.forgotPasswordFlag = false;
    this.error = error;
    this.sendCodeFlag = false;
  }

  //TODO ENVIAR ID DEL INPUT
  showPass(typePass){
    if(typePass === "newPass"){
      var inputPass = $("#passNew1");
      $(this).toggleClass("fa-eye fa-eye-slash");
      //var input = $($(this).attr("toggle"));
      if (inputPass.attr("type") == "password") {
        inputPass.attr("type", "text");
      } else {
        inputPass.attr("type", "password");
      }
    }else{
      var inputPass = $("#passNew2");
      $(this).toggleClass("fa-eye fa-eye-slash");
      //var input = $($(this).attr("toggle"));
      if (inputPass.attr("type") == "password") {
        inputPass.attr("type", "text");
      } else {
        inputPass.attr("type", "password");
      }
    }
      

  }

  abrirModal(){
    //console.log(this.previewText);
    //this.showAlerPrivacity = false;
    this.setTerminos();
    const modal = document.getElementById('modalTerminos');
    modal.style.display = 'block';
  }

  cerrarModal(){
    const modal = document.getElementById('modalTerminos');
    modal.style.display = 'none';
  }


  setTerminos(){
    this.textoModal = `<p><strong>AVISO DE PRIVACIDAD</strong></p>
    <p><strong>&nbsp;</strong></p>
    <p>Mercadotecnia Ideas y Tecnolog&iacute;a, Sociedad An&oacute;nima de Capital Variable (&ldquo;MIT&rdquo;), valora su privacidad y est&aacute; comprometida en proteger la informaci&oacute;n personal (&ldquo;DATOS PERSONALES&rdquo;) que se recaben en el transcurso de nuestra relaci&oacute;n con nuestros usuarios,&nbsp;clientes, solicitantes, contratantes, beneficiarios, usuarios, proveedores o prestadores de bienes y/o servicios, empleados, candidatos, todos ellos personas f&iacute;sicas&nbsp;(&ldquo;TITULAR&rdquo;), por lo que&nbsp; en t&eacute;rminos de la Ley Federal de Protecci&oacute;n de Datos Personales en Posesi&oacute;n de los Particulares ("LEY DE DATOS PERSONALES"), pone a su disposici&oacute;n el presente Aviso de Privacidad (&ldquo;AVISO&rdquo;) a que se refiere la mencionada Ley, el cual tambi&eacute;n se encuentra a la vista en nuestras oficinas, ubicadas, en Corregidora 92, Col. Miguel Hidalgo, Delegaci&oacute;n Tlalpan, Ciudad de M&eacute;xico, C.P. 14260, M&eacute;xico, o bien para ser consultado en cualquier momento en la p&aacute;gina de internet:&nbsp;<a href="http://www.mitec.com.mx/">www.mitec.com.mx</a>.</p>
    <p>En consecuencia, este AVISO describe sus derechos respecto de la recopilaci&oacute;n, uso, almacenamiento, transferencia y protecci&oacute;n de sus DATOS PERSONALES por parte de MIT. Este AVISO aplica a cualquier sitio web propiedad de MIT, as&iacute; como a sitios relacionados, aplicaciones m&oacute;viles y herramientas cuyo Usuario pertenece a MIT, independientemente de la forma y lugar de acceso.</p>
    <p>Por lo anterior, consiente que sus DATOS PERSONALES, incluyendo aquellos de car&aacute;cter patrimonial o financiero y sensibles, en caso de que estos &uacute;ltimos se recaben, sean tratados conforme a los t&eacute;rminos y condiciones de este AVISO.</p>
    <p>&nbsp;</p>
    <ol>
    <li><strong>RESPONSABLE DE LOS DATOS PERSONALES.</strong></li>
    </ol>
    <p>El responsable de los DATOS PERSONALES (&ldquo;Responsable&rdquo;), en t&eacute;rminos de lo establecido en la LEY DE DATOS PERSONALES es MIT, cuyo domicilio ha quedado se&ntilde;alado en el p&aacute;rrafo primero de este AVISO.</p>
    <p>MIT mantiene medidas de seguridad administrativas, t&eacute;cnicas y f&iacute;sicas necesarias, entre ellas el acceso limitado a las bases de datos, uso de contrase&ntilde;as y hardware o archivos restringidos que permiten garantizar la privacidad y protecci&oacute;n de los DATOS PERSONALES contra da&ntilde;o, p&eacute;rdida, alteraci&oacute;n, destrucci&oacute;n o el uso, acceso o tratamiento no autorizado de los mismos.</p>
    <p>&nbsp;</p>
    <ol start="2">
    <li><strong>DATOS PERSONALES RECABADOS.</strong></li>
    </ol>
    <p>MIT solamente tendr&aacute; acceso a la informaci&oacute;n que el TITULAR proporcione voluntariamente a trav&eacute;s de correo electr&oacute;nico u otro medio de contacto directo, incluyendo a trav&eacute;s del sitio web y/o aplicaci&oacute;n m&oacute;vil de MIT. Por tanto, MIT podr&aacute; recabar y almacenar las siguientes DATOS PERSONALES del TITULAR:</p>
    <ul>
    <li><strong>Datos de identificaci&oacute;n Personales y Sensibles</strong>&nbsp;(<em>Nombre Completo, Correo Electr&oacute;nico Personal y/o Laboral, Domicilio, Tel&eacute;fono, Tel&eacute;fono Celular, Registro Federal de Contribuyentes &ldquo;RFC&rdquo;, Identificaci&oacute;n Oficial IFE/INE, Clave &Uacute;nica de Registro de Poblaci&oacute;n &ldquo;CURP&rdquo;, Acta de Nacimiento, Firma Aut&oacute;grafa, Antecedentes M&eacute;dicos)</em></li>
    <li><strong>Datos patrimoniales o financieros</strong>&nbsp;(<em>N&uacute;mero de Tarjeta de Cr&eacute;dito o D&eacute;bito, Numero CLABE de Cuenta de Cheques, Aviso de Retenci&oacute;n de INFONAVIT (&uacute;nicamente para fines de contrataci&oacute;n laboral)</em>)</li>
    <li><strong>Datos laborales y datos acad&eacute;micos</strong>&nbsp;(<em>Actividad o Giro, Curricula Vitae, Fotograf&iacute;as, Constancia de estudios de nivel m&aacute;ximo grado acad&eacute;mico alcanzado, Cartas de Recomendaci&oacute;n</em>)</li>
    </ul>
    <p><em>*Los n&uacute;meros de tarjeta de cr&eacute;dito tienen un tratamiento acorde a la normatividad de PCI, por lo que s&oacute;lo se almacenan los 6 primeros d&iacute;gitos y los 4 &uacute;ltimos.</em></p>
    <p>Asimismo, el TITULAR acepta que MIT podr&aacute; videograbar y almacenar su imagen o mientras permanezca en las instalaciones de MIT. Dicha videograbaci&oacute;n se har&aacute; con fines de identificaci&oacute;n y para preservar la seguridad de las instalaciones y personal de MIT. Por tanto, el TITULAR autoriza expresamente a que su imagen, incluyendo apariencia, semejanza, rasgos f&iacute;sicos y voz, sean videograbados y almacenados por MIT de conformidad con lo se&ntilde;alado en este p&aacute;rrafo.</p>
    <p>Los DATOS PERSONALES antes enunciados pueden ser obtenidos del TITULAR (i) personalmente; (ii) de manera directa, ya sea por v&iacute;a electr&oacute;nica, grabaci&oacute;n de conversaciones telef&oacute;nicas o cualquier otra tecnolog&iacute;a; a trav&eacute;s de solicitudes, cuestionarios o formatos (iii) de manera indirecta a trav&eacute;s de fuentes de acceso p&uacute;blico o de terceros autorizados por MIT, los cuales son recabados y generados de la relaci&oacute;n jur&iacute;dica que se tiene celebrada con el TITULAR.</p>
    <p>MIT podr&aacute; recabar y tratar los siguientes DATOS PERSONALES sensibles: el estado de salud, caracter&iacute;sticas f&iacute;sicas, &uacute;nicamente de sus candidatos y empleados dentro del proceso de selecci&oacute;n, reclutamiento y contrataci&oacute;n, por medio de evaluaciones t&eacute;cnicas y/o psicol&oacute;gicas, socioecon&oacute;micas y m&eacute;dicas. Los cuales se obtienen de manera personal del TITULAR.</p>
    <p>Todos los DATOS PERSONALES que sean recabados de los TITULARES, ser&aacute;n almacenados en la base de datos o base de almacenamiento y recopilaci&oacute;n de informaci&oacute;n de MIT (en lo sucesivo &ldquo;Base de Datos&rdquo;), misma que cuenta con altas medidas de seguridad para garantizar la privacidad y protecci&oacute;n de los DATOS PERSONALES contra da&ntilde;o, p&eacute;rdida, alteraci&oacute;n, destrucci&oacute;n o el uso, acceso o tratamiento no autorizado de los mismos. MIT almacenar&aacute; los DATOS PERSONALES que hayan sido recabados de los TITULARES durante el tiempo que sea necesario para cumplir con la finalidad para la cual fueron recabados, as&iacute; como durante el tiempo permitido por la legislaci&oacute;n aplicable. En consecuencia, los DATOS PERSONALES se encuentran protegidos tanto en l&iacute;nea, mediante c&oacute;digos de cifrado; como fuera de l&iacute;nea, ya que &uacute;nicamente el personal designado para realizar alguna tarea espec&iacute;fica con los DATOS PERSONALES puede acceder a la Base de Datos.</p>
    <p>MIT podr&aacute; comunicarse con el TITULAR por medio de correo electr&oacute;nico o alg&uacute;n otro medio autorizado por el TITULAR a fin de informarle sobre nuevos productos o servicios, as&iacute; como sobre alg&uacute;n cambio sobre las reglas operativas de los productos y/o al presente AVISO.</p>
    <p>&nbsp;</p>
    <ol start="3">
    <li><strong>FINALIDAD DE LOS DATOS PERSONALES RECABADOS.</strong></li>
    </ol>
    <p>Los DATOS PERSONALES que MIT trata, son para dos tipos de finalidades:</p>
    <ol>
    <li>Las que dan origen a la relaci&oacute;n jur&iacute;dica entre el MIT y el TITULAR, y son necesarias para la existencia, mantenimiento y cumplimiento de dicha relaci&oacute;n:</li>
    </ol>
    <ul>
    <li>Dar cumplimiento a las obligaciones contra&iacute;das con el TITULAR; derivadas de la prestaci&oacute;n de los servicios de validaci&oacute;n de identidad, recepci&oacute;n, y procesamiento de pagos con tarjeta de cr&eacute;dito en los t&eacute;rminos de la Ley de Instituciones de Cr&eacute;dito, en todas sus modalidades, de conformidad con las dem&aacute;s disposiciones legales y administrativas aplicables.</li>
    <li>En el caso de la relaci&oacute;n laboral, se utilizan los DATOS PERSONALES para todos los fines vinculados a la relaci&oacute;n. Particularmente, dentro del proceso de selecci&oacute;n, reclutamiento y contrataci&oacute;n por medio de evaluaciones t&eacute;cnicas y/o psicol&oacute;gicas, socioecon&oacute;micas y m&eacute;dicas, se utilizan Datos Sensibles. Tambi&eacute;n se emplean DATOS PERSONALES, en el caso de verificaci&oacute;n de referencias de empleos anteriores, integraci&oacute;n del expediente laboral cuando la persona sea contratada, mantener los datos de especialidades y competencias para la posible cobertura de vacantes; capacitaci&oacute;n, desarrollo, pago de prestaciones laborales y para el cumplimiento de obligaciones tributarias.</li>
    <li>En el caso de los proveedores o prestadores de bienes y/o servicios, para el cumplimiento de las obligaciones derivadas de la prestaci&oacute;n del servicio celebrado entre el TITULAR y el MIT, las cuales se establecen en los contratos respectivos;</li>
    <li>Identificar al TITULAR cuando acceda y permanezca en el domicilio de MIT.</li>
    <li>Para todos los fines vinculados con la relaci&oacute;n jur&iacute;dica contractual que se tiene celebrada con el TITULAR.</li>
    </ul>
    <ol start="2">
    <li>Las finalidades no necesarias, que no dan origen a la relaci&oacute;n jur&iacute;dica con MIT, como: prospecci&oacute;n comercial, mercadotecnia, publicidad de otros servicios y/ productos relacionados con MIT. En tal sentido, los TITULARES podr&aacute;n solicitar en cualquier momento la cancelaci&oacute;n del env&iacute;o de correos electr&oacute;nicos por parte de MIT relacionados con las finalidades precisadas en este inciso, mediante el env&iacute;o de un correo electr&oacute;nico a la siguiente direcci&oacute;n:&nbsp;<a href="mailto:privacidad@mitec.com.mx">privacidad@mitec.com.mx</a>.</li>
    </ol>
    <p>En caso de que los DATOS PERSONALES del TITULAR se recaben de manera indirecta (tal y como se defini&oacute; anteriormente), el TITULAR podr&aacute; manifestar su negativa para el tratamiento de sus DATOS PERSONALES, siguiendo para tal efecto el procedimiento previsto en este AVISO de Privacidad.</p>
    <p>&nbsp;</p>
    <ol start="4">
    <li><strong>TRANSFERENCIA DE LOS DATOS PERSONALES RECABADOS</strong></li>
    </ol>
    <p>En atenci&oacute;n a lo dispuesto en la LEY DE DATOS PERSONALES, MIT podr&aacute; transferir y/o compartir los DATOS PERSONALES que hayan sido recabados de los TITULARES, en los siguientes casos:</p>
    <ul>
    <li>Las excepciones previstas en el art&iacute;culo 37 de Ley de Datos Personales, as&iacute; como a realizar esta transferencia en los t&eacute;rminos que fija dicha Ley;</li>
    <li>Autoridades financieras, mexicanas y extranjeras, con la finalidad de dar cumplimiento a las obligaciones del Responsable derivadas de leyes o tratados internacionales, obligaciones tributarias, as&iacute; como para el cumplimiento de notificaciones o requerimientos oficiales;</li>
    <li>Autoridades judiciales, mexicanas y extranjeras, con la finalidad de dar cumplimiento a notificaciones, requerimientos u oficios de car&aacute;cter judicial;</li>
    <li>Cuando la transferencia sea necesaria en virtud de un contrato celebrado o por celebrar en inter&eacute;s del TITULAR, por MIT y un tercero.</li>
    <li>IMSS (Instituto Mexicano del Seguro Social), con la finalidad de dar cumplimiento a obligaciones contenidas en la legislaci&oacute;n de seguridad social;</li>
    <li>INFONAVIT (Instituto del Fondo Nacional de la Vivienda para los Trabajadores), con la finalidad de dar cumplimiento de obligaciones contenidas para el descuento y reporte de alg&uacute;n cr&eacute;dito o hipoteca.</li>
    </ul>
    <p>En caso de realizar alguna transferencia de los DATOS PERSONALES del TITULAR que requiera su consentimiento expreso, se le solicitar&aacute;.</p>
    <p>Para el caso en que MIT sea adquirida por un tercero, los DATOS PERSONALES ser&aacute;n transferidos al adquirente de MIT. El adquirente, como nuevo Responsable del tratamiento de los DATOS PERSONALES, seguir&aacute; manteniendo las medidas de seguridad administrativas, t&eacute;cnicas y f&iacute;sicas necesarias que permitan garantizar la privacidad y protecci&oacute;n de sus DATOS PERSONALES contra da&ntilde;o, p&eacute;rdida, alteraci&oacute;n, destrucci&oacute;n o el uso, acceso o tratamiento no autorizado de los mismos de conformidad con lo establecido en el presente AVISO.</p>
    <p>&nbsp;</p>
    <ol start="5">
    <li><strong> EJERCICIO DE DERECHOS SOBRE LOS DATOS PERSONALES</strong></li>
    </ol>
    <p>En cualquier momento, los TITULARES o sus representantes legales podr&aacute;n: revocar su consentimiento para el tratamiento o uso de sus DATOS PERSONALES por parte de MIT, limitar su uso o divulgaci&oacute;n, as&iacute; como acceder a ellos, rectificarlos en caso de que sean inexactos o incompletos, cancelarlos y oponerse a su tratamiento en su totalidad o para ciertos fines del presente AVISO de Privacidad, as&iacute; como para su uso con fines de mercadotecnia, publicidad y prospecci&oacute;n comercial.</p>
    <p>En tal caso, el TITULAR deber&aacute; contactar al &aacute;rea Protecci&oacute;n de DATOS PERSONALES de MIT al correo electr&oacute;nico:&nbsp;<a href="mailto:privacidad@mitec.com.mx">privacidad@mitec.com.mx</a>&nbsp;y siguiendo el procedimiento se&ntilde;alado en los p&aacute;rrafos que siguen.</p>
    <p>Los TITULARES deber&aacute;n se&ntilde;alar expresa y detalladamente: (i) que desean revocar su consentimiento para el tratamiento o uso de sus DATOS PERSONALES; (ii) se&ntilde;alar la manera mediante la cual desea limitar el uso o divulgaci&oacute;n de los DATOS PERSONALES; (iii) se&ntilde;alar la manera en que desean acceder o rectificar sus DATOS PERSONALES; (iv) se&ntilde;alar que desean cancelar sus DATOS PERSONALES; y/o (v) se&ntilde;alar que desean oponerse al tratamiento de sus DATOS PERSONALES.</p>
    <p>Para ejercer los derechos antes descritos, los TITULARES deber&aacute;n acompa&ntilde;ar a su solicitud lo siguiente:</p>
    <ol>
    <li>a)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El nombre del TITULAR, correo electr&oacute;nico, domicilio u otro medio para comunicarle la respuesta a su solicitud.</li>
    <li>b)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Una copia de los documentos que acrediten la identidad o, en su caso, la representaci&oacute;n legal del TITULAR.</li>
    <li>c)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La descripci&oacute;n clara y precisa de los DATOS PERSONALES respecto de los que se busca ejercer alguno de los derechos antes mencionados.</li>
    <li>d)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cualquier otro elemento o documento que facilite la localizaci&oacute;n de los DATOS PERSONALES.</li>
    </ol>
    <p>Para el caso del presente AVISO, la respuesta a la solicitud del TITULAR se realizar&aacute; primeramente en el correo electr&oacute;nico proporcionado y, salvo cuando as&iacute; lo especifique el TITULAR, se realizar&aacute; en el domicilio se&ntilde;alado.</p>
    <p>En el caso de solicitudes de rectificaci&oacute;n de DATOS PERSONALES, el TITULAR deber&aacute; indicar en la solicitud, adem&aacute;s de lo arriba se&ntilde;alado, las modificaciones a realizarse y aportar la documentaci&oacute;n que sustente su petici&oacute;n.</p>
    <p>MIT responder&aacute; toda solicitud en un plazo m&aacute;ximo de 20 (veinte) d&iacute;as h&aacute;biles contados desde la fecha de presentaci&oacute;n de la solicitud correspondiente, a efecto de que, si resulta procedente, se haga efectiva la misma dentro de los 15 (quince) d&iacute;as h&aacute;biles siguientes a la fecha en que se comunique la respuesta.</p>
    <p>Trat&aacute;ndose de solicitudes de acceso a DATOS PERSONALES proceder&aacute; la entrega, previa acreditaci&oacute;n de la identidad del solicitante o representante legal, seg&uacute;n corresponda.</p>
    <p>Los plazos antes referidos podr&aacute;n ser ampliados una sola vez por un periodo igual, siempre y cuando as&iacute; lo justifiquen las circunstancias del caso.</p>
    <p>&nbsp;</p>
    <ol start="5">
    <li><strong>USO DE COOKIES Y WEB BEACONS.</strong></li>
    </ol>
    <p>Las&nbsp;<em>cookies</em>&nbsp;son archivos de texto que son descargados autom&aacute;ticamente y almacenados en el disco duro del equipo de c&oacute;mputo del TITULAR al navegar en una p&aacute;gina de Internet espec&iacute;fica, que permiten recordar al servidor de Internet algunos datos sobre este TITULAR, entre ellos, sus preferencias para la visualizaci&oacute;n de las p&aacute;ginas de ese servidor, nombre y contrase&ntilde;a.</p>
    <p>Por su parte, las&nbsp;<em>web beacons</em>&nbsp;son im&aacute;genes insertadas en una p&aacute;gina de Internet o correo electr&oacute;nico, que puede ser utilizado para monitorear el comportamiento de un visitante, como almacenar informaci&oacute;n sobre la direcci&oacute;n IP del TITULAR, duraci&oacute;n del tiempo de interacci&oacute;n en dicha p&aacute;gina y el tipo de navegador utilizado, entre otros.</p>
    <p>Toda comunicaci&oacute;n por correo electr&oacute;nico no protegida realizada a trav&eacute;s de Internet puede ser objeto de intercepci&oacute;n o p&eacute;rdida, adem&aacute;s de posibles alteraciones. MIT no es responsable y ni usted ni otros podr&aacute;n exigirle responsabilidad por cualquier da&ntilde;o resultante de cualquier intercepci&oacute;n, p&eacute;rdida o alteraci&oacute;n relacionada con un mensaje de correo electr&oacute;nico enviado por el TITULAR a MIT o por MIT al TITULAR.&nbsp;MIT no obtiene DATOS PERSONALES a trav&eacute;s de dichas tecnolog&iacute;as u otras similares.</p>
    <p>&nbsp;</p>
    <p>El sitio web, aplicaci&oacute;n m&oacute;vil y otros sitios propiedad de MIT pueden llegar a contener enlaces a otros sitios que no son propiedad de MIT. Por tanto, MIT no es responsable de las pol&iacute;ticas de privacidad o el contenido de dichos sitios web.</p>
    <p>&nbsp;</p>
    <ol start="7">
    <li><strong>CAMBIOS AL AVISO Y JURISDICCI&Oacute;N.</strong></li>
    </ol>
    <p>MIT se reserva el derecho de efectuar en cualquier momento modificaciones o actualizaciones al presente AVISO de privacidad, de conformidad con las modificaciones que puedan llegar a existir en la Ley de DATOS PERSONALES, pol&iacute;ticas internas o nuevos requerimientos para la prestaci&oacute;n u ofrecimiento de sus servicios. Por tanto, el presente AVISO estar&aacute; vigente de manera indefinida, hasta en tanto no sea realizada alguna modificaci&oacute;n, cambio o actualizaci&oacute;n al mismo. El TITULAR acepta que cuando se notifique alguna modificaci&oacute;n en el AVISO y el TITULAR contin&uacute;e usando el sitio web de MIT, recibiendo sus servicios o continuando con su relaci&oacute;n contractual con MIT, &eacute;ste acepta expresamente y se somete a las modificaciones realizadas, las cuales surtir&aacute;n plenos efectos en la fecha de su notificaci&oacute;n al TITULAR.</p>
    <p>Cualquier cambio ser&aacute; informado mediante alguno de los siguientes medios:</p>
    <ul>
    <li>Nuestro sitio de internet:&nbsp;<a href="http://www.mitec.com.mx/">www.mitec.com.mx</a></li>
    <li>Al recabar DATOS PERSONALES, de manera personal.</li>
    </ul>
    <p>Para la interpretaci&oacute;n y cumplimiento del presente AVISO, el TITULAR y MIT se sujetan expresamente a la competencia de los Tribunales competentes en la Ciudad de M&eacute;xico, renunciando expresamente a cualquier jurisdicci&oacute;n que pudiera corresponderles por raz&oacute;n de su domicilio actual o futuro.</p>
    <p>&nbsp;</p>
    <ol start="8">
    <li><strong>CONSENTIMIENTO DEL TITULAR</strong></li>
    </ol>
    <p>En caso de que el TITULAR no est&eacute; de acuerdo de manera parcial o total sobre el contenido del presente AVISO, solicitamos notifique su inconformidad y desacuerdo al correo electr&oacute;nico:&nbsp;<a href="mailto:privacidad@mitec.com.mx">privacidad@mitec.com.mx</a>, con atenci&oacute;n al &aacute;rea de Protecci&oacute;n de DATOS PERSONALES.</p>
    <p>Para cualquier asunto relacionado con el presente AVISO y el tratamiento de sus DATOS PERSONALES puede contactar al Responsable a trav&eacute;s del &aacute;rea de Cumplimiento, ubicada en:&nbsp;Corregidora 92, Col. Miguel Hidalgo, Delegaci&oacute;n Tlalpan, Ciudad de M&eacute;xico, C.P. 14260, M&eacute;xico; o bien, a trav&eacute;s del correo electr&oacute;nico&nbsp;<a href="mailto:privacidad@mitec.com.mx">privacidad@mitec.com.mx</a>&nbsp; o comunicarse al tel&eacute;fono 1500-9020 ext. 9018.</p>
    <p>Asimismo, el TITULAR acepta y reconoce que: i) al hacer uso de los productos o servicios de MIT o continuar en el uso de cualquier sitio web, aplicaci&oacute;n m&oacute;vil o plataforma propiedad de MTI; ii) realizar alguna negociaci&oacute;n con MIT; iii) formalizar una relaci&oacute;n contractual con MIT o firmar f&iacute;sicamente este AVISO; iv) firmar cualquier contrato o anexo con MIT; v) aceptar electr&oacute;nicamente este AVISO por cualquier medio, o vi) aceptar de cualquier forma fehaciente este AVISO, otorga expresamente su consentimiento para el tratamiento de sus DATOS PERSONALES de conformidad con lo se&ntilde;alado el presente AVISO.</p>
    <p>Fecha de &uacute;ltima actualizaci&oacute;n: 30 de marzo de 2020</p>
    <p>&nbsp;</p>`;
  }


}
