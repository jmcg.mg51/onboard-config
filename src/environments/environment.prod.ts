
export const environment = {
  production: false,
  name: 'prod',
  // baseUrl: 'https://2u597e7kmf.execute-api.us-east-1.amazonaws.com/test/',
  baseUrl: 'https://api.mitidentity.com/',
  baseLinkServicios: 'https://obd.mitidentity.com/#/services/terminos/',
  tokenCognitoUrl: 'https://auth.mitidentity.com/oauth2/token',
  region: 'us-east-1',

  identityPoolId: 'us-east-1:9d4758cc-35af-419f-957f-485fcae29ab2',
  userPoolId: 'us-east-1_Q49DJB7ld',
  clientId: '47itsf3bpfum550nrbhm7nfofv',
  mandatorySignIn: 'true',
  logins: `cognito-idp.us-east-1.amazonaws.com/us-east-1_Q49DJB7ld`,


  rekognitionBucket: 'rekognition-pics',
  albumName: 'usercontent',
  bucketRegion: 'us-east-1',

  cognito_idp_endpoint: '',
  cognito_identity_endpoint: '',
  sts_endpoint: '',
  dynamodb_endpoint: '',
  s3_endpoint: ''

};
