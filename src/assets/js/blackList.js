var CLABE_WEIGHTS = [3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7];
var CLABE_LENGTH = 18;

var rfc_pattern_pm = '^(([A-Z�&]{3})([0-9]{2})([0][13578]|[1][02])(([0][1-9]|[12][\\d])|[3][01])([A-Z0-9]{3}))|' +
'(([A-Z�&]{3})([0-9]{2})([0][13456789]|[1][012])(([0][1-9]|[12][\\d])|[3][0])([A-Z0-9]{3}))|' +
'(([A-Z�&]{3})([02468][048]|[13579][26])[0][2]([0][1-9]|[12][\\d])([A-Z0-9]{3}))|' +
'(([A-Z�&]{3})([0-9]{2})[0][2]([0][1-9]|[1][0-9]|[2][0-8])([A-Z0-9]{3}))$';
var rfc_pattern_pf = '^(([A-Z�&]{4})([0-9]{2})([0][13578]|[1][02])(([0][1-9]|[12][\\d])|[3][01])([A-Z0-9]{3}))|' +
'(([A-Z�&]{4})([0-9]{2})([0][13456789]|[1][012])(([0][1-9]|[12][\\d])|[3][0])([A-Z0-9]{3}))|' +
'(([A-Z�&]{4})([02468][048]|[13579][26])[0][2]([0][1-9]|[12][\\d])([A-Z0-9]{3}))|' +
'(([A-Z�&]{4})([0-9]{2})[0][2]([0][1-9]|[1][0-9]|[2][0-8])([A-Z0-9]{3}))$';

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	
	//var actions = $("table td:last-child").html();
	// Append table with add row form on add new button click
    $(".add-new").click(function(){
		$(this).attr("disabled", "disabled");
		document.getElementById("btn-save").disabled = true; 

		var index = $("table tbody tr:last-child").index();
        var row = '<tr class="itemBlackList">' +
            '<td class="type"><select id="typeBlackList" name="typeBlackList" class="form-control form-control-lg custom-select">'+
			'<option value="0" selected>Selecciona el tipo de lista negra</option>'+
			'<option value="FINGER_PRINT">FINGER PRINT</option>'+
			'<option value="RFC">RFC</option>'+
			'<option value="CLABE">CLABE</option>'+
			'<option value="EMAIL">EMAIL</option>'+
		  '</select>'+
		  '<span id="spanError" style="color:red;"></span></td>' +
            '<td class="value"><input style="text-transform: uppercase;" autocomplete="off" id="valueBlackList" type="text" class="form-control"></td>' +
			'<td class="motive"><select id="motiveBlackList" name="motiveBlackList" class="form-control form-control-lg custom-select">'+
		  '<option value="0" selected>Selecciona un motivo</option>'+
		  '<option value="Fraude">Fraude</option>'+
		  '<option value="Reporte por contracargo">Reporte por contracargo</option>'+
		  '<option value="Determinación no Fraude">Determinación no Fraude</option>'+
		  '<option value="Vendedor">Vendedor</option>'+
		  '<option value="Otros">Otros</option>'+
		'</select>'+
		'<td class="commentInput"><textarea minlength="10" id="comment" name="comment" rows="2" cols="50" required maxlength="200"></textarea></td>' +
			'<td>'+
			'<a class="add" title="Agregar" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>'+
			'<a class="delete" title="Eliminar" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>'+
			'</td>' +
        '</tr>';
    	$("table").append(row);		
		//$("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
		$('[data-toggle="tooltip"]').tooltip();
		$("#spanError").hide();
    });
	// Add row on add button click
	$(document).on("click", ".add", function(){
		var empty = false;
		var emptySelect = false
		var input = $(this).parents("tr").find('input[type="text"]');
		var textArea = $(this).parents("tr").find('textarea');
		var select = $('#typeBlackList').val();
		var selectMotive = $('#motiveBlackList').val();
		var commentInput = $("#comment").val();
		var valueInput = $("#valueBlackList").val();
		var value = input['0'].value;
		var valueTextArea = textArea['0'].value;
		if(select == 0){
			$("#spanError").show();
			$("#spanError").text('Debe seleccionar un tipo de lista negra');
			console.log("no hay valor seleccionado");
			$('#btn-save').prop( "disabled", true );
 
			return;
		}else{
			$("#spanError").hide();
			$('#btn-save').prop( "disabled", false );
		}
		if(valueInput == ""){
			$("#spanError").show();
			$("#spanError").text('Debe ingresar un valor');
			console.log("no hay valor seleccionado");
			$('#btn-save').prop( "disabled", true );
			return;
		}else{
			$("#spanError").hide();
			$('#btn-save').prop( "disabled", false );
		}
		if(selectMotive == 0){
			$("#spanError").show();
			$("#spanError").text('Debe seleccionar un motivo');
			console.log("no hay valor seleccionado");
			$('#btn-save').prop( "disabled", true );
			return;
		}else{
			$("#spanError").hide();
			$('#btn-save').prop( "disabled", false );
		}
		if(commentInput == ""){
			$("#spanError").show();
			$("#spanError").text('Debe describir el motivo de enviar el registro a lista negra');
			console.log("no hay valor seleccionado");
			$('#btn-save').prop( "disabled", true );
			return;
		}else{
			$("#spanError").hide();
			$('#btn-save').prop( "disabled", false );
		}

		

        input.each(function() {
			if(!$(this).val()){
				$(this).addClass("error");
				empty = true;
			} else if(select === 'CLABE' && !validaClabe(value)) {
				console.log('no es una cuenta clabe válida');
				$("#spanError").show();
				$("#spanError").text('Cuenta Clabe Inválida');
				$('#btn-save').prop( "disabled", true );
				empty = true;
			} else if(select === 'RFC') {
				var rfcTemp = value.toUpperCase();
				if (rfcTemp.length == 12 && !rfcTemp.match(rfc_pattern_pm)){
					console.log('no es un RFC válido');
					$("#spanError").show();
					$("#spanError").text('RFC Inválido');
					$('#btn-save').prop( "disabled", true );
					empty = true;
				} else if (rfcTemp.length == 13 && !rfcTemp.match(rfc_pattern_pf)){
					console.log('no es un RFC válido');
					$("#spanError").show();
					$("#spanError").text('RFC Inválido');
					$('#btn-save').prop( "disabled", true );
					empty = true;
					//
				}else if (rfcTemp.length !== 13 && rfcTemp.length !== 12 && !rfcTemp.match(rfc_pattern_pf)){
					console.log('no es un RFC válido');
					$("#spanError").show();
					$("#spanError").text('RFC Inválido');
					$('#btn-save').prop( "disabled", true );
					empty = true;
					//
				}else{
					$("#valueBlackList").val(rfcTemp);
				}
				
			} else if (select === 'EMAIL' && !value.match('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')) {
				console.log('no es un correo válido');
				$("#spanError").show();
				$("#spanError").text('Email Inválido');
				$('#btn-save').prop( "disabled", true );
				empty = true;
			} else if(valueTextArea.length < 10) {
				console.log('descripcion invaida');
				$("#spanError").show();
				$("#spanError").text('La descripción debe contener entre 10 y 200 carácteres.');
				$('#btn-save').prop( "disabled", true );
				empty = true;
			} else {
				$("#spanError").hide();
				$(this).removeClass("error");
				$('#btn-save').prop( "disabled", false );
            }
		});
		$(this).parents("tr").find(".error").first().focus();
		if(!empty){
			input.each(function(){
				$(this).parent("td").html($(this).val());
			});			
			$(this).parents("tr").find(".add, .edit").toggle();
			$(".add-new").removeAttr("disabled");
			if(select != 0){
				$('#typeBlackList').parent().html($('#typeBlackList').val());
			}

			if(selectMotive != 0){
				$("#comment").parent("td").html($("#comment").val());
				$('#motiveBlackList').parent().html($('#motiveBlackList').val());
			}

			
		}

		
    });
	// Edit row on edit button click
	$(document).on("click", ".edit", function(){		
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
			$(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
		});		
		$(this).parents("tr").find(".add, .edit").toggle();
		$(".add-new").attr("disabled", "disabled");
    });
	// Delete row on delete button click
	$(document).on("click", ".delete", function(){
        $(this).parents("tr").remove();
		$(".add-new").removeAttr("disabled");
		document.getElementById("btn-save").disabled = false;
    });
});

function validaClabe(clabe) {
    return isANumber(clabe) &&
      clabe.length === CLABE_LENGTH &&
      clabe.substring(CLABE_LENGTH - 1) === validacionDigitoVerificador(clabe);
  }
function isANumber(str) {
    return !/\D/.test(str);
  }
  function validacionDigitoVerificador(clabe) {
    const clabeList = clabe.split('');
    const clabeInt = clabeList.map((i) => Number(i));
    const weighted = [];
    for (let i = 0; i < CLABE_LENGTH - 1; i++) {
      weighted.push(clabeInt[i] * CLABE_WEIGHTS[i] % 10);
    }
    const summed = weighted.reduce((curr, next) => curr + next) % 10;
    const controlDigit = (10 - summed) % 10;
    return controlDigit.toString();
  }